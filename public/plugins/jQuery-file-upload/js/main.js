$(function () {
    'use strict';

    loading();
    deleteHightLight();

    // Initialize the jQuery File Upload widget:
    $(':file').click(function() {
      $('#optionform').fileupload({
        filesContainer: '.'+$(this).data('image_cate')
      });
    });

    $(':file').change(function() {
      $('#optionform').fileupload('option', {
          url: admin_url+'filemanager/index/POST/'+$(this).data('image_cate')+'/'+menu_link,
          disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
          autoUpload: true,
          filesContainer: '.'+$(this).data('image_cate')
      }).bind('fileuploaddone', function(e, data) {

        if(data.result.files[0].status) {
          loading(true);
        }
      });
    });

    $( ".sortable" ).sortable({
        appendTo: "body",
        helper: "clone",
        update: function (event, ui) {
          $.ajax({
              data: $("#optionform").serialize(),
              type: 'POST',
              url: admin_url+'filemanager/index/PUT/'+$(this).data('image_cate')+'/'+menu_link
          }).done(function() {
            loading(true);
          });
        }
    }).disableSelection();
});

function loading(option=null)
{
  var main_id = $("#optionform").find('#main_id').val();
  var lang_id = $("#optionform").find('#lang_id').val();
  var option  = (option ? option : ( main_id ? true : $("#optionform").find('.alert-error').length ));

  if($('.sortable').length > 0) {
    $.each( $('.sortable'), function( key, value ) {
      $.ajax({
        type: "GET",
        url: admin_url+'filemanager/index/GET/'+$(value).data('image_cate')+'/'+menu_link,
        dataType: "json",
        data: {
          option: option,
          lang_id: lang_id,
          main_id: main_id
        },
        success: function(dataJSON) {

          $(value).html(tmpl("template-download", dataJSON));
          $(value).find('.fade').addClass('in');

          $('.'+$(value).data('image_cate')+' .delete').click(function() {
            $('#optionform').fileupload({
              filesContainer: '.'+$(value).data('image_cate')
            });
          });

          $('.set_highlight').click(function() {

            var default_main_id = $(this).data('default_main_id');
            var attachment_id   = $(this).data('attachment_id');

            setHighLight(menu_link,default_main_id,attachment_id,lang_id);
          });
        }
      });
    });
  }
}

function setHighLight(menu_link,default_main_id,attachment_id,lang_id)
{
  $.ajax({
    type: "POST",
    url: admin_url+'filemanager/highlight/'+menu_link+'/'+lang_id,
    dataType: "json",
    data: {
      default_main_id: default_main_id,
      attachment_id: attachment_id
    },
    success: function(dataJSON) {

      var optionform = $("#optionform");
      optionform.find('.thumbnail img').attr({ "src": dataJSON.file_path });
      optionform.find('.image_tools_select_mb').removeClass('hidden');

      loading(true);
      deleteHightLight();
    }
  });
}

function deleteHightLight()
{
  $('.delete_hihglight').click(function() {
    $.ajax({
      type: "POST",
      url: admin_url+'filemanager/highlight/'+menu_link+'/'+$("#optionform").find('#lang_id').val(),
      dataType: "json",
      data: {
        default_main_id: $(this).data('main_id'),
        attachment_id: null
      },
      success: function(dataJSON) {

        $('.delete_hihglight').addClass('hidden');
        $('.delete_hihglight').parents('.thumbnail').find('img').attr({ "src": base_url+"public/images/thumbnail-default.jpg" });
        loading(true);
      }
    });
  });
}