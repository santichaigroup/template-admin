
/*
 * Facebook JavaSDK Login Respon
 */


  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    /*console.log('statusChangeCallback');
    console.log(response);*/
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
	   FB.login(function(response) {
			/*console.log('statusChangeCallback');
   			console.log(response);*/
		  if (response.status === 'connected') {
			// Logged into your app and Facebook..
			testAPI(response);
		  } else if (response.status === 'not_authorized') {
			// The person is logged into Facebook, but not your app.
			document.getElementById('status').innerHTML = 'Please log '+'into this app.';
		  } else {
			// The person is not logged into Facebook, so we're not sure if
			// they are logged into this app or not.
			/*document.getElementById('status').innerHTML = 'Please log ' +'into Facebook.';*/
		  }
		},{scope: 'public_profile,email'});
  }

	
  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() { 
  FB.init({
    appId      : '121097351633793',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.3' // use version 
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.
	
	//Check Login Page Auto
  /*FB.getLoginStatus(function(response) {
   	statusChangeCallback(response);
  });*/

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI(response_status) {
    // console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=id,name,email,first_name,last_name,gender', function(response) {
		//console.log(response);
      	//console.log('Successful login for: ' + response.name);//Show Console log Connect Fb 
		getDataFB(response,response_status);	
		
    }); 
  }
	
	// Get Data Insert Database
  function getDataFB(response,response_status)
  {
	// console.log(response);
		if(response_status.status=='connected')
		{
			var linkurl = site_url+'member/LoginFB';

          $.ajax({
            type: "GET",
            url: linkurl,
            data: {
                id      : response.id,
                name    : response.name,
                first_name  : response.first_name,
                last_name   : response.last_name,
                gender    : response.gender,
                email   : response.email,
                accessToken : response_status.authResponse.accessToken,
                status    : response_status.status
            },
            success: function(result) {

              if(result=="no") { 

                location.href=site_url; 
                
              } else if(result=="yes") {
                
                // location.href=site_url+'cart/EN'; 
                location.href=current_url;
              }
            }
          });

		}else{
			document.getElementById('status').innerHTML = 'Please log ' +'into Facebook.';	
		}
	  
  }