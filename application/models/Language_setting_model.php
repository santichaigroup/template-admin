<?php
class Language_setting_model extends CI_Model {

	public function dataTable($lang_id="TH", $limit=null, $start=null, $is_order = array(), $is_search = array())
	{
		$this->db->select('*');
		$this->db->from('system_seo');

		// Search from Datatable
			if(!empty($is_search)) {
				foreach ($is_search as $key => $search) {

					switch ($key) {
						case 'post_date_format':
							$dateFillter = explode(' - ', $search);

							$this->db->where("system_seo.post_date >=", $dateFillter[0]);
							$this->db->where("system_seo.post_date <=", $dateFillter[1]);
							break;

						default:
							// Check Column name
							if($this->db->field_exists($key, 'system_seo')) {

								$this->db->where("system_seo.".$key, $search);
							}
							break;
					}
				}

				$this->db->where("system_seo.seo_status <>", "deleted");
			} else {

				$this->db->where("system_seo.seo_status <>", "deleted");
			}

		// Sorting from Datatable
			if(!empty($is_order)) {
				foreach ($is_order as $key => $order) {
					// Check Column name
						if($this->db->field_exists($key, 'system_seo')) {

							$this->db->order_by("system_seo.".$key, $order);
						}
				}
			} else {
				$this->db->order_by("system_seo.sequence", "ASC");
			}

		// Limit Start Filtered page
			if($limit || $start) {
				$this->db->limit($limit, $start);

				return $this->db->get();
			} else {

				return $this->db->count_all_results();
			}
	}

	public function insertContent(array $data = array())
	{
		$this->db->set('post_by',$this->admin_library->userdata('user_id'));
		$this->db->set('post_date','NOW()',false);
		$this->db->set('post_ip',$this->input->ip_address());

		$this->db->insert('system_seo', $data);
		return $this->db->insert_id();
	}

	public function updateContent(array $data = array())
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());

		$this->db->where('id', $data['id']);
		return $this->db->update('system_seo', $data);
	}

	public function getDetail($main_id)
	{
		$this->db->select('*');
		$this->db->where('id', $main_id);

		return $this->db->get('system_seo');
	}

	public function setStatusContent($main_id,$status="pending")
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());
		$this->db->set('seo_status', $status);
		$this->db->where('id', $main_id);

		return $this->db->update('system_seo');
	}

	public function action_language($action, array $data = array())
	{
		switch ($action) {
			case 'insert':

				$this->db->insert('ml_languages', $data);

				$r = $this->db->insert_id();
				break;
			case 'update':

				$r = $this->db->update('ml_languages', $data);
				break;
			case 'delete':

				$this->db->set('is_deleted', 1);
				$r = $this->db->update('ml_languages', $data);
				break;
			default:

				$this->db->select('id, name, language_code, display_order, is_default');

				if(!empty($data['id'])) {
					$this->db->where('id', $data['id']);
				}

				if(!empty($data['language_code'])) {
					$this->db->where('language_code', $data['language_code']);
				}

				$this->db->where('is_deleted', 0);
				$this->db->order_by('display_order, name', 'ASC');

				$r = $this->db->get('ml_languages');
				break;
		}

		return $r;
	}

	public function action_translate($action, array $data = array())
	{
		// ************** Example ************** //

			// List
				// $data = ['label'=>'home', 'language_id'=>'2'];
				// dd($this->language_setting_model->action_translate('list', $data)->result_array());

			// Insert
				// $data = [
				// 			'label'			=> 'home_test_insert',
				// 			'language_id'	=> [ 1, 2 ],
				// 			'value'			=> [ 'Test Insert', 'ทดสอบเพิ่มข้อมูล' ]
				// 		];
				// dd($this->language_setting_model->action_translate('insert', $data));

			// Update
				// $data = [ 
				// 			1 =>	[
				// 						'label'			=> 'home_test_update',
				// 						'value'			=> [ 'Test Update', 'ทดสอบแก้ไขข้อมูล' ]
				// 					]
				// 		];
				// dd($this->language_setting_model->action_translate('update', $data));

			// Delete
				// $data = [
				// 			'id' => '15'
				// 		];
				// dd($this->language_setting_model->action_translate('delete', $data));
		// ************** Example ************** //

		switch ($action) {
			case 'insert':

				$this->db->set('label', $data['label']);
				$this->db->set('is_deleted', 0);
				$this->db->insert('ml_translation_keys');
				$ml_id = $this->db->insert_id();

				if(!empty($data['language_id'])) {

					foreach ($data['language_id'] as $key => $value) {
					
						$this->db->set('translation_key_id', $ml_id);
						$this->db->set('language_id', $value);

						if(!empty($data['value'][$key])) {
							$this->db->set('value', $data['value'][$key]);
						}

						$this->db->set('is_deleted', 0);

						$r = $this->db->insert('ml_translation_values');
					}
				}
				break;
			case 'update':

				if(!empty($data)) {

					foreach ($data as $k => $d) {
						
						$this->db->set('label', $d['label']);
						$this->db->where('id', $k);
						$this->db->update('ml_translation_keys');

						if(!empty($d['value'])) {

							foreach ($d['value'] as $key => $value) {

								$this->db->set('value', $value);
								$this->db->where('id', $key);

								$r = $this->db->update('ml_translation_values');
							}
						}
					}
				}
				break;
			case 'delete':

				$this->db->set('is_deleted', 1);
				$this->db->where('id', $data['id']);
				$r = $this->db->update('ml_translation_keys');

				$this->db->set('is_deleted', 1);
				$this->db->where('translation_key_id', $data['id']);
				$this->db->update('ml_translation_values');
				break;
			default:

				$this->db->select('t1.*');
				$this->db->from('ml_translation_keys as t1');

				if(!empty($data['label'])) {

					$this->db->like('t1.label', $data['label']."_", 'match', 'after'); //before, both
				}

				$this->db->where('t1.is_deleted', 0);
				$this->db->order_by('t1.id', 'asc');
				$r['translation_keys'] = $this->db->get()->result_array();

				if(!empty($r['translation_keys'])) {

					foreach ($r['translation_keys'] as $k => $v) {
						
						$this->db->select('ml_translation_values.*');
						$this->db->join('ml_languages', 'ml_translation_values.language_id=ml_languages.id', 'left');
						$this->db->where('translation_key_id', $v['id']);
						$this->db->where('ml_languages.is_deleted', '0');

						if(!empty($data['language_code'])) {

							$this->db->where('ml_languages.language_code', $data['language_code']);
						}

						$this->db->order_by('language_id', 'asc');
						$r['translation_keys'][$k]['values'] = $this->db->get('ml_translation_values')->result_array();

						if(!empty($r['translation_keys'][$k]['values'])) {

							foreach ($r['translation_keys'][$k]['values'] as $key => $value) {
								
								$language = $this->action_language('list', ['id' => $value['language_id']])->row_array();

								$r['translation_keys'][$k]['values'][$key] = $value;
								$r['translation_keys'][$k]['values'][$key]['name'] = $language['name'];
								$r['translation_keys'][$k]['values'][$key]['language_code'] = $language['language_code'];
							}
						}
					}
				}
				break;
		}

		return $r;
	}
}