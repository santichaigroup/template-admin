<?php
class Setting_email_model extends CI_Model{
	public function dataTable($lang_id)
	{
		$this->db->select('*'); 
		$this->db->from('setting_email_id', 'setting_email_content');
		
		$this->db->join("setting_email_content","setting_email_content.email_id = setting_email_id.email_id");

		$this->db->where("setting_email_content.lang_id",$lang_id);	
		$this->db->where("setting_email_id.email_status <>","deleted");	
		$this->db->order_by("setting_email_id.email_id","desc");
		// $this->db->limit(2);
		return $this->db->get();
	}
	public function checkExistst($email_id)
	{
		$this->db->where("email_id",$email_id);
		$this->db->where("setting_email_id.email_status <>","deleted");
		return $this->db->count_all_results("setting_email_id");
	}

	public function getDetail($email_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('setting_email_id', 'setting_email_content');
		
		if($lang_id){
			$this->db->join("setting_email_content","setting_email_content.email_id = setting_email_id.email_id AND setting_email_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("setting_email_content","setting_email_content.email_id = setting_email_id.email_id AND setting_email_content.content_id = setting_email_id.default_content_id");
		}
		
		$this->db->where("setting_email_id.email_id",$email_id);
		$this->db->where("setting_email_id.email_status <>",'deleted');
		$this->db->order_by("setting_email_id.email_id","desc");
		$this->db->limit(1);
		return  $this->db->get()->row_array();
						
	}
	public function addData()
	{
		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->insert("setting_email_id");
		$email_id = $this->db->insert_id();
		if(!$email_id){
			show_error("Cannot create  email id");	
		}
		return $email_id;
	}
	public function setDate($email_id,$email_date)
	{
		$email_date = date("Y-m-d",strtotime($email_date));
		$this->db->set("email_date",$email_date);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		return $this->db->update("setting_email_id");	
	}
	public function setStatus($email_id,$status)
	{
		$this->db->set("email_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		return $this->db->update("setting_email_id");	
	}
	public function setDefaultContent($email_id,$content_id)
	{
		$this->db->set("default_content_id",$content_id);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		return $this->db->update("setting_email_id");
	}
	public function addLanguage($email_id,$lang_id)
	{//
		$this->db->where("email_id",$email_id);
		$this->db->where("lang_id",$lang_id);
		$has = $this->db->get("setting_email_content");
		
		if($has->num_rows() == 0){
			$this->db->set("email_id",$email_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("setting_email_content");
			return $this->db->insert_id();
		}else{
			$r= $has->row_array();
			return 	@$r['content_id'];
		}
		
	}
	function updateContent($email_id,$lang_id,$content_detail,$subject,$email,$email_cc)
	{
		$this->db->set("content_detail",$content_detail);
		$this->db->set("content_subject",$subject);
		$this->db->set("content_email",$email);
		$this->db->set("content_email_cc",$email_cc);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("setting_email_content");
	}
	function updateContentStatus($email_id,$lang_id,$status)
	{
		$this->db->set("content_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("setting_email_content");
	}
}