<?php
class Module_model extends CI_Model {

	public function list_all_menu($menu_id=null)
	{
		$this->db->select("*");

		if($menu_id) {
			$this->db->where('menu_id', $menu_id);
		}

		$this->db->where("menu_status !=","deleted");
		$this->db->order_by("menu_sequent","asc");

		$result = $this->db->get("system_menu");
		$entry 	= array();

		foreach($result->result_array() AS $row ) {

			$sub_entry 					= array();
			$sub_entry['submenu_entry']	= $this->list_all_submenu($row);
			$sub_entry['id'] 			= $row['menu_id'];
			$sub_entry['label'] 		= $row['menu_label'];
			$sub_entry['sequent'] 		= $row['menu_sequent'];
			$sub_entry['title'] 		= $row['menu_title'];
			$sub_entry['link'] 			= $row['menu_link'];
			$sub_entry['seo'] 			= $row['menu_seo'];
			$entry[] 					= $sub_entry;
			unset($sub_entry);
		}

		return $entry;
	}

	public function get_detail_menu($menu_id)
	{
		$this->db->select("*");
		$this->db->where('menu_id', $menu_id);
		$this->db->order_by("menu_sequent","asc");
		$result = $this->db->get("system_menu")->row_array();
		$entry 	= array();

		if($result) {

			$sub_entry 					= array();
			$sub_entry['submenu_entry']	= ($this->list_all_submenu($result) ? $this->list_all_submenu($result) : '');
			$sub_entry['id'] 			= $result['menu_id'];
			$sub_entry['label'] 		= $result['menu_label'];
			$sub_entry['icon'] 			= $result['menu_icon'];
			$sub_entry['sequent'] 		= $result['menu_sequent'];
			$sub_entry['title'] 		= $result['menu_title'];
			$sub_entry['link'] 			= $result['menu_link'];
			$sub_entry['seo'] 			= $result['menu_seo'];
			$entry[] 					= $sub_entry;
			unset($sub_entry);
		}

		return $entry;
	}

	public function add_menu($data)
	{
		$this->db->set("menu_label", $data['menu_label']);
		$this->db->set("menu_title", $data['menu_title']);
		$this->db->set("menu_icon", $data['menu_icon']);
		$this->db->set("menu_link", $data['menu_link']);
		$this->db->set("menu_seo", $data['menu_seo']."/index/th");
		$this->db->set("menu_sequent", $data['menu_sequent']);
		$this->db->set("menu_status", 'active');
		
		$this->db->insert("system_menu");
		$type_id = $this->db->insert_id();

		if(!$type_id) {
			show_error("Cannot create new Menu");	
		}

		return $type_id;
	}

	public function update_menu($data)
	{
		$this->db->where("menu_id", $data['menu_id']);
		return $this->db->update("system_menu", $data);
	}

	public function list_all_submenu($menu)
	{
		$this->db->select("*");
		$this->db->where("menu_id",$menu['menu_id']);
		$this->db->where("menu_status !=","deleted");
		$this->db->order_by("menu_sequent","asc");
		$submenu 	= $this->db->get("system_submenu");
		$entry 		= array();

		foreach($submenu->result_array() as $row) {
			
			$sub_entry 					= array();
			$sub_entry['submenu_entry']	= array();
			$sub_entry['id'] 			= $row['submenu_id'];
			$sub_entry['label'] 		= $row['menu_label'];
			$sub_entry['icon'] 			= $row['menu_icon'];
			$sub_entry['sequent'] 		= $row['menu_sequent'];
			$sub_entry['title'] 		= $row['menu_title'];
			$sub_entry['link'] 			= $row['menu_link'];
			$sub_entry['seo'] 			= $row['menu_seo'];
			$entry[] 					= $sub_entry;
			unset($sub_entry);
		}

		return $entry;
	}

	public function list_all_layout()
	{
		$this->db->select("*");
		$this->db->order_by("layout_id","asc");
		$query = $this->db->get("system_layout");

		return $query;
	}

	public function add_sub_menu($data)
	{
		$this->db->insert("system_submenu", $data);
		$type_id = $this->db->insert_id();

		if(!$type_id) {
			show_error("Cannot create new Sub Menu");	
		}

		return $type_id;
	}

	public function update_sub_menu($data)
	{
		$this->db->where("submenu_id", $data['submenu_id']);
		return $this->db->update("system_submenu", $data);
	}

	public function add_system_image($data)
	{
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		return $this->db->insert("system_image", $data);
	}
}
?>