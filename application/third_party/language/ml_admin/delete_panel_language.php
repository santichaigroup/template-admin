<?php
require_once('config/panel.php');

session_start();
if (!isset($_SESSION['script_user'])) {
    header('Location:index.php');
    exit();
}

require_once('Connections/conn.php');

if (isset($_GET['id'])
    && isset($_POST['csrf_d'])
    && isset($_SESSION['csrf_d'])
    && $_POST['csrf_d'] == $_SESSION['csrf_d']
) {
    $id = intval($_GET['id']);

    mysqli_query(
        $conn,
        "UPDATE {$panel_strings_table_prefix}languages SET is_deleted = 1 WHERE id='{$id}' AND is_default = 0"
    );

    unset($_SESSION['csrf_d']);

    header("Location: panel_languages.php");

    exit();
} else {
    header("Location: panel_languages.php");
    exit();
}