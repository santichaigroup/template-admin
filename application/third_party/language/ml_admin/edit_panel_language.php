<?php
require_once('config/panel.php');

session_start();
if (!isset($_SESSION['script_user'])) {
    header('Location:index.php');
    exit();
}

require_once('Connections/conn.php');

require_once('site/php/translated_panel_strings.php');

if (isset($_GET['id'])) {
    $id = intval($_GET['id']);

    $sql = <<<EOF
SELECT
t1.name,
t1.language_code,
t1.display_order,
t1.is_default
FROM {$panel_strings_table_prefix}languages t1
WHERE
t1.id='{$id}'
AND t1.is_deleted = 0
EOF;

    $Recordset_LanguageDetails = mysqli_query($conn, $sql) or die(mysqli_error($conn));
    $row_language_details = mysqli_fetch_assoc($Recordset_LanguageDetails);

    if (mysqli_num_rows($Recordset_LanguageDetails) == 0) {
        header("Location: panel_languages.php");
        exit();
    }
} else {
    header("Location: panel_languages.php");
    exit();
}

if (isset($_POST['name'])
    && trim($_POST['name']) != ''
    && isset($_POST['language_code'])
    && trim($_POST['language_code']) != ''
    && isset($_POST['csrf_e'])
    && isset($_SESSION['csrf_e'])
    && $_POST['csrf_e'] == $_SESSION['csrf_e']
) {
    $name          = prepare_for_db($_POST['name']);
    $language_code = prepare_for_db($_POST['language_code']);
    $display_order = (intval($_POST['display_order']) <= 255 ? intval($_POST['display_order']) : 0);
    $is_default    = (isset($_POST['is_default']) ? 1 : 0);

    if ($is_default) {
        $sql = <<<EOF
UPDATE {$panel_strings_table_prefix}languages
SET
name = '{$name}',
language_code = '{$language_code}',
display_order = '{$display_order}',
is_default = '{$is_default}'
WHERE
id = '{$id}'
EOF;
    } else {
        $sql = <<<EOF
UPDATE {$panel_strings_table_prefix}languages
SET
name = '{$name}',
language_code = '{$language_code}',
display_order = '{$display_order}'
WHERE
id = '{$id}'
EOF;
    }

    //update record
    mysqli_query($conn, $sql) or die(mysqli_error($conn));

    //if this is marked as "default", update other records
    if ($is_default) {
        $sql = <<<EOF
UPDATE {$panel_strings_table_prefix}languages
SET
is_default = 0
WHERE
NOT id = '{$id}'
EOF;
        mysqli_query($conn, $sql) or die(mysqli_error($conn));
    }

    unset($_SESSION['csrf_e']);

    header("Location: panel_languages.php");

    exit();
}

$_SESSION['csrf_e'] = md5(uniqid());
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $translated_panel_strings['EditLanguage']; ?></title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<?php include('menu_section.php'); ?>
<div class="container">
    <div class="row" style="margin-top: 10px;">
        <p><strong><?php echo $translated_panel_strings['EditLanguage']; ?></strong></p>

        <form action="" method="post" enctype="multipart/form-data" role="form">
            <table class="table table-bordered" style="width: 600px;">
                <tr>
                    <td>
                        <?php echo $translated_panel_strings['Language']; ?>
                    </td>
                    <td>
                        <input type="text" name="name"
                               value="<?php echo htmlspecialchars($row_language_details['name']); ?>"
                               style="width:160px;"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $translated_panel_strings['LanguageCode']; ?>
                    </td>
                    <td>
                        <input type="text" name="language_code"
                               value="<?php echo htmlspecialchars($row_language_details['language_code']); ?>"
                               style="width:160px;"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $translated_panel_strings['DisplayOrder']; ?>
                    </td>
                    <td>
                        <input type="text" name="display_order"
                               value="<?php echo htmlspecialchars($row_language_details['display_order']); ?>"
                               style="width:160px;"/>
                    </td>
                </tr>
                <?php
                if ($row_language_details['is_default'] == false) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $translated_panel_strings['IsDefault']; ?>
                        </td>
                        <td>
                            <label>
                                <input type="checkbox" name="is_default" id="is_default"/>
                                <?php echo $translated_panel_strings['Yes']; ?></label>
                        </td>
                    </tr>
                <?php
                }
                ?>
                <tr>
                    <td><input type="hidden" name="csrf_e" value="<?php echo $_SESSION['csrf_e']; ?>"/></td>
                    <td>
                        <input type="submit" name="Submit" value="<?php echo $translated_panel_strings['Save']; ?>"/>
                    </td>
                </tr>
            </table>
        </form>
        <br/>
    </div>
</div>
</body>
</html>