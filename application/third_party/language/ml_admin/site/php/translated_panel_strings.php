<?php
$translated_panel_strings = array();

//panel - translation keys
$sql = <<<EOF
SELECT
t1.id,
t1.label
FROM {$panel_strings_table_prefix}translation_keys t1
WHERE
t1.is_deleted = 0
ORDER BY t1.label ASC
EOF;

$Recordset_PanelTranslationKeys = mysqli_query($conn, $sql) or die(mysqli_error($conn));

$panel_translation_keys = array();

while ($row_key = mysqli_fetch_assoc($Recordset_PanelTranslationKeys)) {
    $panel_translation_keys[] = $row_key;
}

//panel - translation values
$sql = <<<EOF
SELECT
t1.label,
t2.value,
t2.translation_key_id
FROM {$panel_strings_table_prefix}translation_keys t1
INNER JOIN {$panel_strings_table_prefix}translation_values t2 ON t2.translation_key_id = t1.id
INNER JOIN {$panel_strings_table_prefix}languages t3 ON t2.language_id = t3.id
WHERE
t1.is_deleted = 0
AND t2.is_deleted = 0
AND t3.is_deleted = 0
AND t3.is_default = 1
EOF;

$Recordset_TranslatedStrings = mysqli_query($conn, $sql) or die(mysqli_error($conn));

$translated_panel_strings = array();

while ($row_value = mysqli_fetch_assoc($Recordset_TranslatedStrings)) {
    $translated_panel_strings[$row_value['label']] = $row_value['value'];
}

foreach ($panel_translation_keys as $key) {
    if (!array_key_exists($key['label'], $translated_panel_strings)) {
        $translated_panel_strings[$key['label']] = '';
    }
}