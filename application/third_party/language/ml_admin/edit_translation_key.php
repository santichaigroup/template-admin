<?php
require_once('config/panel.php');

session_start();
if (!isset($_SESSION['script_user'])) {
    header('Location:index.php');
    exit();
}

require_once('Connections/conn.php');

require_once('site/php/translated_panel_strings.php');

if (isset($_GET['id'])) {
    $id = intval($_GET['id']);

    $sql = <<<EOF
SELECT
t1.label
FROM {$strings_table_prefix}translation_keys t1
WHERE
t1.id='{$id}'
AND t1.is_deleted = 0
EOF;

    $Recordset_TranslationKeyDetails = mysqli_query($conn, $sql) or die(mysqli_error($conn));
    $row_translation_key_details = mysqli_fetch_assoc($Recordset_TranslationKeyDetails);

    if (mysqli_num_rows($Recordset_TranslationKeyDetails) == 0) {
        header("Location: translations.php");
        exit();
    }
} else {
    header("Location: translations.php");
    exit();
}

if (isset($_POST['label'])
    && trim($_POST['label']) != ''
    && isset($_POST['csrf_e'])
    && isset($_SESSION['csrf_e'])
    && $_POST['csrf_e'] == $_SESSION['csrf_e']
) {
    $label = prepare_for_db($_POST['label']);

    $sql = <<<EOF
UPDATE {$strings_table_prefix}translation_keys
SET
label = '{$label}'
WHERE
id = '{$id}'
EOF;

    //update record
    mysqli_query($conn, $sql) or die(mysqli_error($conn));

    unset($_SESSION['csrf_e']);

    header("Location: translations.php");

    exit();
}

$_SESSION['csrf_e'] = md5(uniqid());
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $translated_panel_strings['EditTranslationKey']; ?></title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<?php include('menu_section.php'); ?>
<div class="container">
    <div class="row" style="margin-top: 10px;">
        <p><strong><?php echo $translated_panel_strings['EditTranslationKey']; ?></strong></p>

        <form action="" method="post" enctype="multipart/form-data" role="form">
            <table class="table table-bordered" style="width: 600px;">
                <tr>
                    <td>
                        <?php echo $translated_panel_strings['Key']; ?>
                    </td>
                    <td>
                        <input type="text" name="label"
                               value="<?php echo htmlspecialchars($row_translation_key_details['label']); ?>"
                               style="width:160px;"/>
                    </td>
                </tr>
                <tr>
                    <td><input type="hidden" name="csrf_e" value="<?php echo $_SESSION['csrf_e']; ?>"/></td>
                    <td>
                        <input type="submit" name="Submit" value="<?php echo $translated_panel_strings['Save']; ?>"/>
                    </td>
                </tr>
            </table>
        </form>
        <br/>
    </div>
</div>
</body>
</html>