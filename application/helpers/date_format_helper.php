<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Project Name : CI Master V3
* Create Date : 2/25/2558 BE
*/
if ( ! function_exists('date_format_shortmonth'))
{
	function date_format_shortmonth(&$datestring, $lang="TH")
	{
		if(!is_numeric($datestring)){
			$month = (int) date("m",strtotime($datestring));
			$month = (int) ($month-1);
		}else{
			$month = (int) $datestring-1;
		}
		
		if($month < 0 ){ return '(ไม่รู้จัก)'; }

		if($lang=="TH") {
			$month_array = array('ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');
		} else {
			$month_array = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		}

		$monthstring = (isset($month_array[$month]))?$month_array[$month]:'(ไม่รู้จัก '.$month.')';
		return $monthstring;
	}
}
if ( ! function_exists('date_format_fullmonth'))
{
	function date_format_fullmonth(&$datestring, $lang="TH")
	{
		if(!is_numeric($datestring)){
			$month = (int) date("m",strtotime($datestring));
			$month = (int) ($month-1);
		}else{
			$month = (int) $datestring-1;
		}
		
		if($month <0 ){ return '(ไม่รู้จัก.)'; }
		if($lang=="TH") {
			$month_array = array('มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฏาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
		} else {
			$month_array = array("January","February","March","April","May","June","July","August","September","October","November","December");
		}
		$monthstring = (isset($month_array[$month]))?$month_array[$month]:'(ไม่รู้จัก)';
		return $monthstring;
	}
}
if ( ! function_exists('date_format_shortyear'))
{
	function date_format_shortyear(&$datestring, $lang="TH")
	{
		if(!is_numeric($datestring)){
			$globalyear = (int) date("Y",strtotime($datestring));
		}else{
			$globalyear=$datestring;
		}
		$thaiyear = (int) ($globalyear+543);
		$thaiyear = substr($thaiyear, -2,2);
		return $thaiyear;
	}
}
if ( ! function_exists('date_format_fullyear'))
{
	function date_format_fullyear(&$datestring, $lang="TH")
	{
		if(!is_numeric($datestring)){
			$globalyear = (int) date("Y",strtotime($datestring));
		}else{
			$globalyear=$datestring;
		}
		$thaiyear = (int) ($globalyear+543);
		return $thaiyear;
	}
}
if ( ! function_exists('date_format_convert_shortdate'))
{
	function date_format_convert_shortdate(&$datestring, $lang="TH")
	{
		list($date,$month,$year) = explode('-', date("j-m-Y",strtotime($datestring)));
		$strdate = $date . " " . date_format_shortmonth($month, $lang) . " " . date_format_shortyear($year);
		return $strdate;
	}
}
if ( ! function_exists('date_format_convert_shortdatetime'))
{
	function date_format_convert_shortdatetime(&$datestring, $lang="TH")
	{
		list($date,$month,$year,$hour,$minute,$second) = explode('-', date("j-m-Y-H-i-s",strtotime($datestring)));
		$strdate = $date . " " . date_format_shortmonth($month, $lang) . " " . date_format_shortyear($year) . " " . $hour . ":".$minute . ":". $second;
		return $strdate;
	}
}
if ( ! function_exists('sdateth'))
{
	function sdateth($datestring)
	{
		return date_format_convert_shortdate($datestring);
	}
}
if ( ! function_exists('sdatetimeth'))
{
	function sdatetimeth($datestring)
	{
		return date_format_convert_shortdatetime($datestring) ;
	}
}
if ( ! function_exists('date_format_convert_fulldate'))
{
	function date_format_convert_fulldate(&$datestring, $lang="TH")
	{
		list($date,$month,$year) = explode('-', date("j-m-Y",strtotime($datestring)));
		$strdate = $date . " " . date_format_fullmonth($month, $lang) . " " . date_format_fullyear($year);
		return $strdate;
	}
}
if ( ! function_exists('ldateth'))
{
	function ldateth($datestring)
	{
		return date_format_convert_fulldate($datestring);
	}
}

