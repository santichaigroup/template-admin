<?php
class Syssetting extends CI_Controller {

	var $_data = array();
	var $_menu_name = '';
	var $menu;
	var $submenu;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();

		$this->path 	= 	$this->uri->ruri_string();
		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(1));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(1), $this->uri->segment(2));
	}
	public function index()
	{
		admin_redirect();
	}

	// ################### User list. ######################

	public function userlist()
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
		$this->admin_library->setDetail($this->submenu['menu_title']);
		$this->admin_library->view("syssetting/users/listview",$this->_data); 
		$this->admin_library->output($this->path);
	}

	public function load_datatable()
	{
		$result_data 	= $this->admin_library->getAllUser();

		$output = array(
			"iTotalRecords" => $result_data->num_rows(),
			"iTotalDisplayRecords" => "25",
 	           "aData" => $result_data->result()
 	       );

		echo json_encode($output);
	}

	public function useradd()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules("user_fullname","Full Name","trim|required|max_length[100]");	
		$this->form_validation->set_rules("user_email","Email","trim|required|max_length[100]|valid_email");
		$this->form_validation->set_rules("user_mobileno","Mobile Phone","trim|max_length[10]|numeric");	
		$this->form_validation->set_rules("user_group","Group","trim|required|numeric");	
		$this->form_validation->set_rules("username","Username","trim|required|max_length[30]");
		$this->form_validation->set_rules("password","Password","trim|required");

		// เช็คอัพโหลดไฟล์
		$this->form_validation->set_rules("image_thumb[]","รูปภาพ","callback_fileupload_images");

		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata('error_message');
			$this->useradd_view();

		} else {

			$user_fullname 		= $this->input->post("user_fullname");
			$user_email 		= $this->input->post("user_email");
			$user_mobileno 		= $this->input->post("user_mobileno");
			$user_group 		= $this->input->post("user_group");
			$username 			= $this->input->post("username");
			$newpassword 		= trim($this->input->post("password"));
			$user_status 		= $this->input->post("user_status");

			$img_thumbnail 		= @$this->_data['img_thumbnail'];
			if($img_thumbnail) {
				foreach($img_thumbnail as $thumbnail )
				{
					$user_thumbnail = $thumbnail;
				}
			}

			$data = array(
						'username'				=> $username,
						'password'				=> md5($newpassword),
						'user_fullname' 		=> $user_fullname,
						'user_email'			=> $user_email,
						'user_mobileno'			=> $user_mobileno,
						'user_group'			=> $user_group,
						'user_thumbnail'		=> @$user_thumbnail,
						'user_status' 			=> $user_status
					);

			$adduser = $this->admin_library->adduser($data);
			
			if(!$adduser) {

				$this->session->set_flashdata("success_message","User can't created profile.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "User can't created profile",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

				admin_redirect("syssetting/useradd");

			} else {

				$this->session->set_flashdata("success_message","User profile is created.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "User profile is created",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

				admin_redirect("syssetting/userlist");
			}
		}
	}
	
	private function useradd_view()
	{
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->admin_library->setTitle("Create user",'glyphicon-plus-sign');
		$this->admin_library->setDetail("เพิ่มข้อมูลผู้ใช้งาน");
		$this->admin_library->view("syssetting/users/add",$this->_data); 
		$this->admin_library->output($this->path);
	}

	public function useredit($user_id)
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules("user_fullname","Full Name","trim|required|max_length[100]");	
		$this->form_validation->set_rules("user_email","Email","trim|required|max_length[100]|valid_email");
		$this->form_validation->set_rules("user_mobileno","Mobile Phone","trim|max_length[10]|numeric");	
		// $this->form_validation->set_rules("user_group","Group","trim|numeric");	
		$this->form_validation->set_rules("username","Username","trim|required|max_length[30]");

		// เช็คอัพโหลดไฟล์
		$this->form_validation->set_rules("image_thumb[]","รูปภาพ","callback_fileupload_images");

		if($this->form_validation->run()===false){
			$this->_data['error_message'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->useredit_view($user_id);
		}else{
			$user_fullname = $this->input->post("user_fullname");
			$user_email = $this->input->post("user_email");
			$user_mobileno = $this->input->post("user_mobileno");
			// $user_group = $this->input->post("user_group");
			$username = $this->input->post("username");
			$old_username = $this->input->post("old_username");
			$newpassword = trim($this->input->post("password"));
			$user_status = $this->input->post("user_status");

			$img_thumbnail = $this->_data['img_thumbnail'];
			if($img_thumbnail) {
				foreach($img_thumbnail as $thumbnail )
				{
					$user_thumbnail = $thumbnail;
				}

			}

			if($newpassword != "") {

				$this->admin_library->updatePassword($user_id,$newpassword);

				// Send Mail
			}

			$data = array(
						'user_id'				=> $user_id,
						'username'				=> $username,
						'user_fullname' 		=> $user_fullname,
						'user_email'			=> $user_email,
						'user_mobileno'			=> $user_mobileno,
						// 'user_group'			=> $user_group,
						'user_thumbnail'		=> @$user_thumbnail,
						'user_status' 			=> $user_status
					);

			$userupdate = $this->admin_library->updateuserinfo($data);
			
			if(!$userupdate) {

				$this->session->set_flashdata("success_message","Can't update user profile.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Can't update user profile",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			} else {

				$this->session->set_flashdata("success_message","User profile is updated.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "User profile is updated",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

				admin_redirect("syssetting/useredit/".$user_id);
			}
		}
	}

	private function useredit_view($user_id)
	{
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['rs'] = $this->admin_library->getuserinfo($user_id);
		$this->admin_library->setTitle("Edit user","glyphicon-pencil");
		$this->admin_library->setDetail("เปลี่ยนแปลงแก้ไขข้อมูลผู้ใช้งาน");
		$this->admin_library->view("syssetting/users/edit",$this->_data); 
		$this->admin_library->output($this->path);
	}

	public function userdelete_user($user_id)
	{
		$data = array(
					'user_id' 		=> $user_id,
					'user_status' 	=> 'deleted'
				);

		$deleteuser = $this->admin_library->update_status_account($data);

		if(!$deleteuser) {

			$this->session->set_flashdata("success_message","Can't delete user.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'delete',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Can't delete user",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/userlist");

		} else {

			$this->session->set_flashdata("success_message","Delete user success.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'delete',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Delete user success",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/userlist");
		}
	}

	public function userhandle_delete()
	{
		$data = array(
					'user_id' 		=> $this->input->post("user_id"),
					'user_status' 	=> 'deleted'
				);

		$deleteuser = $this->admin_library->update_status_account($data);

		if(!$deleteuser) {

			$this->session->set_flashdata("success_message","Can't delete user.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'delete',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Can't delete user",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/userlist");

		} else {

			$this->session->set_flashdata("success_message","Delete user success.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'delete',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Delete user success",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/userlist");
		}
	}
	public function userhandle_suspend()
	{
		$data = array(
					'user_id' 		=> $this->input->post("user_id"),
					'user_status' 	=> 'pending'
				);

		$deleteuser = $this->admin_library->update_status_account($data);

		if(!$deleteuser) {

			$this->session->set_flashdata("success_message","Can't update user.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'Update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Can't Update user",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/userlist");

		} else {

			$this->session->set_flashdata("success_message","Update user success.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'Update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Update user success",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/userlist");
		}
	}
	public function userhandle_unsuspend()
	{
		$data = array(
					'user_id' 		=> $this->input->post("user_id"),
					'user_status' 	=> 'active'
				);

		$deleteuser = $this->admin_library->update_status_account($data);

		if(!$deleteuser) {

			$this->session->set_flashdata("success_message","Can't update user.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'Update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Can't Update user",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/userlist");

		} else {

			$this->session->set_flashdata("success_message","Update user success.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'Update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Update user success",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/userlist");
		}
	}

	public function check_email_user()
	{
		$user_email = $this->input->post('user_email');
		if(!empty($user_email)) {

			$checking = $this->admin_library->check_email($user_email);

			if($checking) {
				echo "false";
			} else {
				echo "true";
			}
		} else {

			admin_redirect('syssetting/useradd');
		}
	}

	public function check_username_user()
	{
		$username = $this->input->post('username');
		if(!empty($username)) {

			$checking = $this->admin_library->check_username($username);

			if($checking) {
				echo "false";
			} else {
				echo "true";
			}
		} else {

			admin_redirect('syssetting/useradd');
		}
	}

	// ################### Check uploads Images. ######################

	public function fileupload_images()
	{
	    $number_of_files = sizeof($_FILES['image_thumb']['tmp_name']);
	 
	    $files = $_FILES['image_thumb'];

		if($files['tmp_name']) {

			if($files['tmp_name'][0]==NULL) {

				return TRUE;
			}
		}

		$config['upload_path'] 		= FCPATH.'./public/uploads/system_users/images';
		$config['encrypt_name']		= true;
		$config['allowed_types'] 	= 'gif|jpg|jpeg|png';
		$config['max_size']			= '5024';
		$config['max_width']  		= '640';
		$config['max_height']  		= '360';

	    for ($i = 0; $i < $number_of_files; $i++)
	    {
	      $_FILES['image_thumb']['name'] 		= $files['name'][$i];
	      $_FILES['image_thumb']['type'] 		= $files['type'][$i];
	      $_FILES['image_thumb']['tmp_name'] 	= $files['tmp_name'][$i];
	      $_FILES['image_thumb']['error'] 		= $files['error'][$i];
	      $_FILES['image_thumb']['size'] 		= $files['size'][$i];
	      
	      $this->upload->initialize($config);
	      if ($this->upload->do_upload('image_thumb'))
	      {
			$data  								= $this->upload->data();
			$this->_data['img_thumbnail'][] 	= $data['file_name'];
	      }
	      else
	      {
			$this->form_validation->set_message('fileupload_images', $this->upload->display_errors());
	        return FALSE;
	      }
	    }

	    return TRUE;
	}
	
	// ################### User group. ######################

	public function usergroup()
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
		$this->admin_library->setDetail($this->submenu['menu_title']);
		$this->admin_library->view("syssetting/usergroup/listview",$this->_data); 
		$this->admin_library->output($this->path);
	}

	public function load_datatable_group()
	{
		$result_data 	= $this->admin_library->getAllGroup(true);

		$output = array(
			"iTotalRecords" => $result_data->num_rows(),
			"iTotalDisplayRecords" => "25",
 	           "aData" => $result_data->result()
 	       );

		echo json_encode($output);
	}
	
	public function usergroupadd()
	{
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->load->library('form_validation');
		$this->form_validation->set_rules("group_name","Group Name","trim|required");
		$this->form_validation->set_rules("company_id","Company","trim|required");
		$this->form_validation->set_rules("group_superadmin","Superadmin Group","trim|required");

		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata('error_message');
			
			$this->admin_library->setTitle('Users Group','glyphicon-plus-sign');
			$this->admin_library->setDetail("เพิ่มกลุ่มผู้ใช้งานและจัดการสิทธิการเข้าถึงเมนู");
			$this->admin_library->view("syssetting/usergroup/add",$this->_data); 
			$this->admin_library->output($this->path);
		} else {

			$data = array(
						'group_name' 		=> $this->input->post("group_name"),
						'group_superadmin'	=> $this->input->post("group_superadmin"),
						'company_id'		=> $this->input->post("company_id"),
						'group_status'		=> $this->input->post("group_status")
					);

			$group_id = $this->admin_library->addGroup($data);

			if(!$group_id) {
					
				$this->session->set_flashdata("success_message","User group \"".$this->input->post("group_name")."\" can't created.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "User group can't created",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					
			} else {

				$this->setPermision($group_id, 'add');
				$this->session->set_flashdata("success_message","User group \"".$this->input->post("group_name")."\" is created.");

				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "User group is created",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->users_log($logs);
				// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

				admin_redirect("syssetting/usergroup");
			}
		}
	}
	
	public function usergroupedit($id)
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules("group_id","Group Id","trim|required");
		$this->form_validation->set_rules("group_name","Group Name","trim|required");
		$this->form_validation->set_rules("company_id","Company","trim|required");
		$this->form_validation->set_rules("group_superadmin","Superadmin Group","trim|required");

		if($this->form_validation->run()===false){

			$this->_data['_menu_link'] = $this->menu['menu_link'];
			$this->_data['row']  = $this->admin_library->getGroupDetail($id);

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata('error_message');

			$this->admin_library->setTitle('Users Group','glyphicon-pencil');
			$this->admin_library->setDetail("แก้ไขกลุ่มผู้ใช้งานและจัดการสิทธิการเข้าถึงเมนู");

			$this->admin_library->view("syssetting/usergroup/edit",$this->_data); 
			$this->admin_library->output($this->path);
		}else{

			$data = array(
						'group_name' 		=> $this->input->post("group_name"),
						'group_superadmin'	=> $this->input->post("group_superadmin"),
						'company_id'		=> $this->input->post("company_id"),
						'group_id'			=> $this->input->post("group_id"),
						'group_status'		=> $this->input->post("group_status"),
					);

			$save = $this->admin_library->updateGroup($data);

			if(!$save) {

					$this->session->set_flashdata("success_message","User group \"".$this->input->post("group_name")."\" can't updated.");

					// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
						$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
						$logs = array(
									'menu_id' 		=> $this->menu['menu_id'],
									'submenu_id' 	=> __Function__,
									'action'		=> 'update',
									'username' 		=> $user['username'],
									'session' 		=> $this->session->session_id,
									'messages' 		=> "User group is updated",
									'data'			=> serialize($data),
									'status' 		=> 'failure'
								);

						$this->logs_library->users_log($logs);
					// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			} else {
					
					$this->setPermision($this->input->post("group_id"), 'update');
					$this->session->set_flashdata("success_message","User group \"".$this->input->post("group_name")."\" is updated.");

					// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
						$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
						$logs = array(
									'menu_id' 		=> $this->menu['menu_id'],
									'submenu_id' 	=> __Function__,
									'action'		=> 'update',
									'username' 		=> $user['username'],
									'session' 		=> $this->session->session_id,
									'messages' 		=> "User group is updated",
									'data'			=> serialize($data),
									'status' 		=> 'success'
								);

						$this->logs_library->users_log($logs);
					// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

					admin_redirect("syssetting/usergroup");								
			}
		}
	}
	
	private function setPermision($group_id, $action)
	{
		$menu = @$_POST['menu'];
		$submenu = @$_POST['submenu'];
		$this->admin_library->clearPermision($group_id);
		if(is_array($menu)){
			foreach($menu as $row){
				$this->admin_library->addPermision($group_id,$row,0);
			}
		}
		if(is_array($menu)){
			foreach($submenu as $menu_id=>$rs){
				foreach($rs as $row){
					$this->admin_library->addPermision($group_id,$menu_id,$row);
				}
			}
		}

		$data = array(
					'group_id' 		=> $group_id,
					'menu'			=> $menu,
					'submenu'		=> $submenu
				);

		// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> $action,
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "User group is created",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->users_log($logs);
		// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
	}
	public function usergroupdelete_user($group_id)
	{
		$data = array(
					'group_status'		=> 'deleted',
					'group_id'			=> $group_id
				);

		$check = $this->admin_library->update_status_group($data);

		if(!$check) {

			$this->session->set_flashdata("success_message","Can't delete user group.");
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Can't delete user group",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->users_log($logs);
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

		} else {

			$this->session->set_flashdata("success_message","Delete user success.");
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "User group is deleted",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->users_log($logs);
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/usergroup");
		}
	}
	public function usergrouphandle_delete()
	{
		$data = array(
					'group_status'		=> 'deleted',
					'group_id'			=> $this->input->post("group_id")
				);

		$check = $this->admin_library->update_status_group($data);

		if(!$check) {

			$this->session->set_flashdata("success_message","Can't delete user group.");
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Can't delete user group",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->users_log($logs);
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

		} else {

			$this->session->set_flashdata("success_message","Delete user success.");
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "User group is deleted",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->users_log($logs);
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/usergroup");
		}
	}
	public function usergrouphandle_suspend()
	{
		$data = array(
					'group_status'		=> 'pending',
					'group_id'			=> $this->input->post("group_id")
				);

		$check = $this->admin_library->update_status_group($data);

		if(!$check) {

			$this->session->set_flashdata("success_message","Can't update user group.");
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'update',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Can't update user group",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->users_log($logs);
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

		} else {

			$this->session->set_flashdata("success_message","Update user success.");
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'update',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "User group is updated",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->users_log($logs);
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/usergroup");
		}
	}
	public function usergrouphandle_unsuspend()
	{
		$data = array(
					'group_status'		=> 'active',
					'group_id'			=> $this->input->post("group_id")
				);

		$check = $this->admin_library->update_status_group($data);

		if(!$check) {

			$this->session->set_flashdata("success_message","Can't update user group.");
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'update',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Can't update user group",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->users_log($logs);
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

		} else {

			$this->session->set_flashdata("success_message","Update user success.");
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'update',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "User group is updated",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->users_log($logs);
			// IIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIII //

			admin_redirect("syssetting/usergroup");
		}
	}

	// ################### Company name. ######################

	// public function usercompany()
	// {
	// 	$this->_data['success_message'] = $this->session->flashdata('success_message');
	// 	$this->admin_library->setTitle('User Company','icon-leaf');
	// 	$this->admin_library->setDetail("จัดการบริษัท/หน่วยงาน");
	// 	$this->admin_library->view("syssetting/company/listview",$this->_data); 
	// 	$this->admin_library->output($this->path);
	// }

	// public function gganalytics()
	// {
	// 	(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
	// 	$this->_data['lang_id'] 		= $lang_id;
	// 	$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
	// 	$this->_data['_menu_link']  	= $this->menu['menu_link'];

	// 	if($_POST){
	// 		$this->settings->set("analytic_tracking_id",$this->input->post("analytic_tracking_id"));
	// 		$this->settings->set("analytic_tracking_add_code",$this->input->post("analytic_tracking_add_code", FALSE));
	// 		$this->settings->set("analytic_tracking_event",$this->input->post("analytic_tracking_event"));
	// 		$this->_data['success_message'] = "Update successfully.";
	// 	}

	// 	$this->_data['analytic_tracking_id'] 	= $this->settings_model->item('analytic_tracking_id');
	// 	$this->_data['analytic_tracking_event'] = $this->settings_model->item('analytic_tracking_event');
	// 	$this->_data['analytic_tracking_add_code']	= $this->settings_model->item('analytic_tracking_add_code');

	// 	$this->admin_library->setTitle('Google Analytics','glyphicon-globe');
	// 	$this->admin_library->setDetail("ติดตั้งตัวติดตามเว็บไซต์ Google Analytics");
	// 	$this->admin_library->view("syssetting/gganalytics/edit",$this->_data); 
	// 	$this->admin_library->output($this->path);
	// }

	// public function seotools()
	// {
	// 	(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
	// 	$this->_data['lang_id'] 		= $lang_id;
	// 	$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
	// 	$this->_data['_menu_link']  	= $this->menu['menu_link'];

	// 	if($_POST){
	// 		$this->settings->set("web_title",$this->input->post("web_title"));
	// 		$this->settings->set("web_keyword",$this->input->post("web_keyword"));
	// 		$this->settings->set("web_description",$this->input->post("web_description"));
	// 		$this->settings->set("web_title_en",$this->input->post("web_title_en"));
	// 		$this->settings->set("web_keyword_en",$this->input->post("web_keyword_en"));
	// 		$this->settings->set("web_description_en",$this->input->post("web_description_en"));
	// 		$this->settings->set("web_head_add_code",$this->input->post("web_head_add_code"));
	// 		$this->_data['success_message'] = "Update successfully.";
	// 	}
		
	// 	$this->admin_library->setTitle('SEO Settings','glyphicon-globe');
	// 	$this->admin_library->setDetail("ตั้งค่า SEO");
	// 	$this->admin_library->view("syssetting/seotools/edit",$this->_data); 
	// 	$this->admin_library->output($this->path);
	// }

}