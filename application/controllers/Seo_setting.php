<?php
class Seo_setting extends CI_Controller {

	var $_data = array(),
		$_menu_name,
		$menu,
		$submenu;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('admin_library');
		$this->admin_library->forceLogin();
		$this->load->library('form_validation');

		$this->path 	= 	$this->uri->ruri_string();
		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(1));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(1), $this->uri->segment(2));

		$this->setLanguage			= "th";
		$this->_data['language'] 	= "multiple";
	}

	public function index($lang_id=NULL)
	{
		$this->_data['lang_id'] 		= $this->language_setting($lang_id);
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');

		// ---------- Tools Sequent ---------- //
			$seq 		= $this->input->post('seq');
			$content_id = $this->input->post('content_id');
			if($content_id) {

				foreach ($content_id as $key => $value) {

					$this->language_setting_model->setSequence($key, $value);
				}
			}

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->menu['menu_icon']);
		$this->admin_library->setDetail($this->menu['menu_title']);
		$this->admin_library->view("seosetting/listview",$this->_data);
		$this->admin_library->output($this->path, 5);
	}

	public function load_datatable($lang_id=NULL)
	{
		$this->form_validation->set_rules("length","Length","trim|required|integer");
		$this->form_validation->set_rules("start","Start","trim|required|integer");

		if($this->form_validation->run()===false) {

			echo json_encode([]);
		} else {

			$limit 			= $this->input->post('length');
			$start 			= $this->input->post('start');
			$is_order 		= array();
			$is_search 		= array();
			$result_array 	= array();

			// ####### Column all ####### //
				if(!empty($this->input->post('columns'))) {

					foreach ($this->input->post('columns') as $key => $column) {

						$is_order[$key] = $column['data'];

						if($column['search']['value']) {

							$is_search[$column['data']] 	= $column['search']['value'];
						}
					}
				}

			// ####### Orderable ####### //
				if(!empty($this->input->post('order')[0]['column'])) {

					$is_order = array($is_order[$this->input->post('order')[0]['column']] => $this->input->post('order')[0]['dir']);
				} else {

					$is_order = array();
				}

			// ####### Searchable ####### //
				if(empty($is_search)) {

					$all_data 		= $this->language_setting_model->dataTable($lang_id);
		            $query_data 	= $this->language_setting_model->dataTable($lang_id, $limit, $start, $is_order)
		            									->result_array();
		        } else {

					$all_data 		= $this->language_setting_model->dataTable($lang_id, null, null, null, $is_search);
					$query_data 	= $this->language_setting_model->dataTable($lang_id, $limit, $start, $is_order, $is_search)
														->result_array();
				}

			if(!empty($query_data)) {

				foreach ($query_data as $key => $value) {
					$result_array[$key] = $value;
					$result_array[$key]['no'] = ($key+1)+$start;
					$result_array[$key]['post_date_format'] = date_format_convert_shortdatetime($value['post_date']);
				}
			}

			$output = array(
							"draw"            	=> intval($this->input->post('draw')),
							"recordsFiltered"   => intval($all_data),
							"recordsTotal" 		=> count($result_array),
							"data"            	=> $result_array
						);

			echo json_encode($output);
		}
	}

	public function add($lang_id=NULL)
	{
		$this->_data['lang_id'] = $this->language_setting($lang_id);
		$lang_id 				= $this->_data['lang_id'];

		$this->form_validation->set_rules("seo_name","Menu name","trim|required|max_length[255]");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']			= "Add ".$this->menu['menu_label'];
			$this->_data['_menu_icon']			= "glyphicon-plus-sign";
			$this->_data['_menu_title']			= $this->menu['menu_title'];
			$this->_data['_menu_link']  		= $this->menu['menu_link'];

			$this->_data['validation_errors'] 	= validation_errors();
			$this->_data['error_message'] 		= $this->session->flashdata('error_message');

			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("seosetting/add",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$data = array(
						'seo_name' 			=> $this->input->post('seo_name'),
						'seo_tracking_id' 	=> $this->input->post('seo_tracking_id'),
						'seo_tracking_code' => $this->input->post('seo_tracking_code'),
						'seo_code' 			=> $this->input->post('seo_code'),
						'seo_status' 		=> $this->input->post('seo_status')
					);

			$main_id = $this->language_setting_model->insertContent($data);

			// ---------- Redirect Page ---------- //
				if($this->_data['language']=="single") {
					admin_redirect($this->menu['menu_link']."/edit/".$main_id);
				} else {
					admin_redirect($this->menu['menu_link']."/edit/".$main_id."/".$lang_id);
				}
		}
	}

	public function edit($main_id,$lang_id=NULL)
	{
		$this->_data['lang_id'] = $this->language_setting($lang_id);
		$lang_id 				= $this->_data['lang_id'];
		$this->_data['main_id'] = $main_id;

		$this->_data['row'] = $this->language_setting_model->getDetail($main_id)->row_array();

		if(!$this->_data['row']) {
			show_error("ไม่พบข้อมูลในระบบ");
		}

		if($this->_data['language']=='single') {
			$search 		= ['language_code' => $lang_id];
		} else {
			$search 		= [];
		}
		$list_language 	= $this->language_setting_model->action_language('list', $search);

		if(!empty($list_language)) {
			$this->_data['row']['language'] = $list_language->result_array();
		} else {
			$this->_data['row']['language'] = null;
		}

		if($this->_data['language']=='single') {
			$data = ['label' => strtolower($this->_data['row']['seo_name']), 'language_code' => $lang_id];
		} else {
			$data = ['label' => strtolower($this->_data['row']['seo_name'])];
		}
		$list_translate = $this->language_setting_model->action_translate('list', $data);

		if(!empty($list_translate)) {
			$this->_data['row']['translate'] = $list_translate['translation_keys'];
		} else {
			$this->_data['row']['translate'] = null;
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules("seo_name","Menu name","trim|required|max_length[255]");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']	= "Edit ".$this->menu['menu_label'];
			$this->_data['_menu_icon']	= "glyphicon-plus-sign";
			$this->_data['_menu_title']	= $this->menu['menu_title'];
			$this->_data['_menu_link']  = $this->menu['menu_link'];

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("seosetting/edit",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			// Update SEO content
				$data = [
							'id' 				=> $main_id,
							'seo_name' 			=> $this->input->post('seo_name'),
							'seo_tracking_id' 	=> $this->input->post('seo_tracking_id'),
							'seo_tracking_code' => $this->input->post('seo_tracking_code'),
							'seo_code' 			=> $this->input->post('seo_code'),
							'seo_status' 		=> $this->input->post('seo_status')
						];

				$this->language_setting_model->updateContent($data);

			// Setting Key language
				if($this->input->post('label')) {

					$data_translate = [
								'label'			=> strtolower($this->input->post('seo_name'))."_".$this->input->post('label'),
								'language_id'	=> $this->input->post('language_id'),
								'value'			=> $this->input->post('value')
							];

					$this->language_setting_model->action_translate('insert', $data_translate);
				}

			// Update Value language
				if(!empty($this->input->post('label_update'))) {

					foreach ($this->input->post('label_update') as $key => $value) {
						
						$data_update[$key] = [
									'label' 		=> $value,
									'value' 		=> $this->input->post('value_update')
						];
					}

					$this->language_setting_model->action_translate('update', $data_update);
				}

			// ---------- Redirect Page ---------- //
				if($this->_data['language']=="single") {
					admin_redirect($this->menu['menu_link']."/edit/".$main_id);
				} else {
					admin_redirect($this->menu['menu_link']."/edit/".$main_id."/".$lang_id);
				}
		}
	}

	public function delete_key($main_id=null, $key_id=null, $lang_id=NULL)
	{
		$this->_data['lang_id'] = $this->language_setting($lang_id);
		$lang_id 				= $this->_data['lang_id'];

		if($main_id && $key_id) {

			$data = [ 'id' => $key_id ];
			$this->language_setting_model->action_translate('delete', $data);

			$this->session->set_flashdata("success_message","Delete centent success.");

			// ---------- Redirect Page ---------- //
				if($this->_data['language']=="single") {
					admin_redirect($this->menu['menu_link']."/edit/".$main_id);
				} else {
					admin_redirect($this->menu['menu_link']."/edit/".$main_id.'/'.$lang_id);
				}
		}
	}

	public function delete($main_id, $lang_id=NULL)
	{
		$this->_data['lang_id'] = $this->language_setting($lang_id);
		$lang_id 				= $this->_data['lang_id'];

		$this->language_setting_model->setStatusContent($main_id, 'deleted');

		$this->session->set_flashdata("success_message","Delete centent success.");

		// ---------- Redirect Page ---------- //
			if($this->_data['language']=="single") {
				admin_redirect($this->menu['menu_link']."/index");
			} else {
				admin_redirect($this->menu['menu_link']."/index/".$lang_id);
			}
	}

	public function handle_delete($lang_id=NULL)
	{
		$this->_data['lang_id'] = $this->language_setting($lang_id);
		$lang_id 				= $this->_data['lang_id'];

		$data = array(
					'main_id' 	=> $this->input->post("main_id")
				);

		foreach($data['main_id'] as $id) {

			$this->language_setting_model->setStatusContent($id, 'deleted');
		}

		$this->session->set_flashdata("success_message","Delete centent success.");

		// ---------- Redirect Page ---------- //
			if($this->_data['language']=="single") {
				admin_redirect($this->menu['menu_link']."/index");
			} else {
				admin_redirect($this->menu['menu_link']."/index/".$lang_id);
			}
	}

	public function handle_suspend($lang_id=NULL)
	{
		$this->_data['lang_id'] = $this->language_setting($lang_id);
		$lang_id 				= $this->_data['lang_id'];

		$data = array(
					'main_id' 	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);

		foreach($data['main_id'] as $id) {
			$this->language_setting_model->setStatusContent($id, 'pending');
		}

		$this->session->set_flashdata("success_message","Update status success.");

		// ---------- Redirect Page ---------- //
			if($this->_data['language']=="single") {
				admin_redirect($this->menu['menu_link']."/index");
			} else {
				admin_redirect($this->menu['menu_link']."/index/".$lang_id);
			}
	}

	public function handle_unsuspend($lang_id=NULL)
	{
		$this->_data['lang_id'] = $this->language_setting($lang_id);
		$lang_id 				= $this->_data['lang_id'];

		$data = array(
					'main_id'	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);

		foreach($data['main_id'] as $id) {
			$this->language_setting_model->setStatusContent($id, 'active');
		}

		$this->session->set_flashdata("success_message","Update status success.");

		// ---------- Redirect Page ---------- //
			if($this->_data['language']=="single") {
				admin_redirect($this->menu['menu_link']."/index");
			} else {
				admin_redirect($this->menu['menu_link']."/index/".$lang_id);
			}
	}

	// ################### Language Setting ######################

	private function language_setting($lang_id)
	{
		return $lang_id = (empty($lang_id)) ? $this->setLanguage : $lang_id;
	}
}