<?php
class Module extends CI_Controller {

	var $_data = array(),
		$_menu_name,
		$menu,
		$submenu;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('admin_library');
		$this->load->library('template_library');
		$this->admin_library->forceLogin();
		$this->load->model('module_model');

		$this->path 	= 	$this->uri->ruri_string();
		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(1));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(1), $this->uri->segment(2));
	}

	public function index()
	{
		admin_redirect('module/list_menu');
	}

	public function list_menu()
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['result']			= $this->module_model->list_all_menu();
		$this->_data['success_message'] = $this->session->flashdata('success_message');
		
		$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
		$this->admin_library->setDetail($this->submenu['menu_title']);
		$this->admin_library->view("module/listview",$this->_data);
		$this->admin_library->output($this->path);
	}

	public function add_menu()
	{
		$this->_data['_menu_name'] 		= "Add New Menu";
		$this->_data['_menu_icon']		= "glyphicon-plus-sign";
		$this->_data['_menu_title']		= "เพิ่มเมนู";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("menu_label","Menu Label","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_title","Menu Detail","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_icon","Menu Icon","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_link","Menu Link","trim|required|max_length[255]");
		// $this->form_validation->set_rules("menu_seo","SEO","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_sequent","Menu Sequent","trim|required|max_length[255]");

		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata('error_message');

			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("module/addview",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$menu_label 	= $this->input->post("menu_label");
			$menu_title 	= $this->input->post("menu_title");
			$menu_icon 		= $this->input->post("menu_icon");
			$menu_link 		= $this->input->post("menu_link");
			$menu_seo 		= $this->input->post("menu_link");
			$menu_sequent 	= $this->input->post("menu_sequent");
			$menu_language 	= $this->input->post("menu_language");

			$array_data		= 	array(
									'menu_label'	=>	ucfirst(strtolower($menu_label)),
									'menu_title'	=>	$menu_title,
									'menu_icon'		=>	$menu_icon,
									'menu_link'		=>	strtolower($menu_link),
									'menu_seo'		=>	$menu_seo,
									'menu_sequent'	=>	$menu_sequent,
									'menu_language'	=>	$menu_language
								);

			// Add Data to Database
			$add_menu 	=	$this->module_model->add_menu($array_data);

			$data_image 	= [
								'menu_id'		=> $add_menu,
								'menu_link'		=> strtolower($menu_link),
								'image_title'	=> 'Upload Images',
								'image_name'	=> 'image_thumb',
								'image_cate'	=> 'pc',
								'image_lang'	=> $menu_language,
								'image_limit'	=> '6',
								'image_size'	=> '5000',
								'image_width'	=> '640',
								'image_height'	=> '360',
								'image_allowed'	=> 'gif|jpg|jpeg|png',
								'image_desc'	=> '*ขนาดรูป 640x360 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 6 รูป',
								'image_status'	=> 'pending'
							];
			$this->module_model->add_system_image($data_image);

			// Create file controller
			$checked 	= 	$this->template_library->create_menu($array_data);
			// Check process
			if( $checked && $add_menu ) {

				$this->session->set_flashdata("success_message","Menu has been create.");
				admin_redirect($this->_data['_menu_link']."/list_menu");

			} else {

				$this->session->set_flashdata("error_message","Menu don't create.");
				admin_redirect($this->_data['_menu_link']."/list_menu");

			}
		}
	}

	public function edit_menu($menu_id)
	{
		$this->_data['_menu_name'] 		= "Edit Menu";
		$this->_data['_menu_icon']		= "glyphicon-plus-sign";
		$this->_data['_menu_title']		= "แก้ไขเมนู";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['result']			= $this->module_model->get_detail_menu($menu_id);

		if(!$this->_data['result']) {
			admin_redirect($this->_data['_menu_link']."/list_menu");
		}

		$this->_data['result'] 			= $this->_data['result'][0];
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("menu_label","Menu Label","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_title","Menu Detail","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_icon","Menu Icon","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_link","Menu Link","trim|required|max_length[255]");
		// $this->form_validation->set_rules("menu_seo","SEO","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_sequent","Menu Sequent","trim|required|max_length[255]");

		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] 	= validation_errors();
			$this->_data['error_message'] 		= $this->session->flashdata('error_message');

			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("module/editview",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$menu_id 		= $this->input->post("menu_id");
			$menu_label 	= $this->input->post("menu_label");
			$menu_title 	= $this->input->post("menu_title");
			$menu_icon 		= $this->input->post("menu_icon");
			$menu_link 		= $this->input->post("menu_link");
			$menu_seo 		= $this->input->post("menu_seo");
			$menu_sequent 	= $this->input->post("menu_sequent");

			$array_data		= 	array(
									'menu_id'		=>	$menu_id,
									'menu_label'	=>	$menu_label,
									'menu_title'	=>	$menu_title,
									'menu_icon'		=>	$menu_icon,
									'menu_link'		=>	$menu_link,
									'menu_seo'		=>	$menu_seo,
									'menu_sequent'	=>	$menu_sequent
								);

			$update_menu 	=	$this->module_model->update_menu($array_data);

			if($update_menu) {

				$submenu_label 		= $this->input->post("submenu_label");
				$submenu_title 		= $this->input->post("submenu_title");
				$submenu_icon 		= $this->input->post("submenu_icon");
				$submenu_link 		= $this->input->post("submenu_link");
				$submenu_seo 		= $this->input->post("submenu_seo");
				$submenu_sequent 	= $this->input->post("submenu_sequent");

				foreach ($submenu_label as $key => $value) {
					
					$array_data		= 	array(
											'submenu_id'	=>	$key,
											'menu_label'	=>	$submenu_label[$key],
											'menu_title'	=>	$submenu_title[$key],
											'menu_icon'		=>	$submenu_icon[$key],
											'menu_link'		=>	$submenu_link[$key],
											'menu_seo'		=>	$submenu_seo[$key],
											'menu_sequent'	=>	$submenu_sequent[$key]
										);

					$this->module_model->update_sub_menu($array_data);
				}
			}
			

			// Add Data to Database
			// $add_menu 	=	$this->module_model->add_menu($array_data);
			// Create file controller
			// $checked 	= 	$this->template_library->create_menu($array_data);
			// Check process
			// if( $checked && $add_menu ) {

			// 	$this->session->set_flashdata("success_message","Menu has been update.");
			// 	admin_redirect($this->_data['_menu_link']."/edit_menu/".$menu_id);
			// } else {

			// 	$this->session->set_flashdata("error_message","Menu don't update.");
			// 	admin_redirect($this->_data['_menu_link']."/edit_menu/".$menu_id);
			// }
			$this->session->set_flashdata("success_message","Menu has been update.");
			admin_redirect($this->_data['_menu_link']."/edit_menu/".$menu_id);
		}
	}

	public function delete($menu_id)
	{
		$this->db->set('menu_status', 'deleted');

		$this->db->where('menu_id', $menu_id);
		$this->db->update('system_menu');

		$this->session->set_flashdata("success_message","Menu has been delete.");
		admin_redirect("module/list_menu");
	}

	public function add_sub_menu($menu_id)
	{
		$this->_data['_menu_name'] 		= "Edit Menu";
		$this->_data['_menu_icon']		= "glyphicon-plus-sign";
		$this->_data['_menu_title']		= "เพิ่มเมนูย่อย";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['result']			= $this->module_model->get_detail_menu($menu_id);

		if(!$this->_data['result']) {
			admin_redirect($this->_data['_menu_link']."/list_menu");
		}

		$this->_data['result'] 			= $this->_data['result'][0];
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("submenu_label","Sub Menu Label","trim|required|max_length[255]");
		$this->form_validation->set_rules("submenu_title","Sub Menu Detail","trim|required|max_length[255]");
		$this->form_validation->set_rules("submenu_icon","Sub Menu Icon","trim|required|max_length[255]");
		$this->form_validation->set_rules("submenu_link","Sub Menu Link","trim|required|max_length[255]");
		$this->form_validation->set_rules("submenu_seo","SEO","trim|required|max_length[255]");
		$this->form_validation->set_rules("submenu_sequent","Sub Menu Sequent","trim|required|max_length[255]");

		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] 	= validation_errors();
			$this->_data['error_message'] 		= $this->session->flashdata('error_message');

			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("module/addsubview",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$menu_label 	= $this->input->post("submenu_label");
			$menu_title 	= $this->input->post("submenu_title");
			$menu_icon 		= $this->input->post("submenu_icon");
			$menu_link 		= $this->input->post("submenu_link");
			$menu_seo 		= $this->input->post("submenu_seo");
			$menu_sequent 	= $this->input->post("submenu_sequent");

			$array_data		= 	array(
									'menu_id' 		=> 	$menu_id,
									'menu_label'	=>	$menu_label,
									'menu_title'	=>	$menu_title,
									'menu_icon'		=>	$menu_icon,
									'menu_link'		=>	$menu_link,
									'menu_seo'		=>	$menu_seo,
									'menu_sequent'	=>	$menu_sequent
								);

			$add_menu 	=	$this->module_model->add_sub_menu($array_data);

			// Add Data to Database
			// $add_menu 	=	$this->module_model->add_menu($array_data);
			// Create file controller
			// $checked 	= 	$this->template_library->create_menu($array_data);
			// Check process
			// if( $checked && $add_menu ) {

			// 	$this->session->set_flashdata("success_message","Menu has been update.");
			// 	admin_redirect($this->_data['_menu_link']."/edit_menu/".$menu_id);
			// } else {

			// 	$this->session->set_flashdata("error_message","Menu don't update.");
			// 	admin_redirect($this->_data['_menu_link']."/edit_menu/".$menu_id);
			// }
			$this->session->set_flashdata("success_message","Menu has been update.");
			admin_redirect($this->_data['_menu_link']."/edit_menu/".$menu_id);
		}
	}

	public function edit_sub_menu()
	{
		$this->_data['_menu_name'] 		= "Edit Sub Menu";
		$this->_data['_menu_icon']		= "glyphicon-plus-sign";
		$this->_data['_menu_title']		= "แก้ไขเมนูย่อย";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['result']			= $this->module_model->list_all_menu();



		$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
		$this->admin_library->setDetail($this->_data['_menu_title']);
		$this->admin_library->view("module/editsubview",$this->_data);
		$this->admin_library->output($this->path);
	}

	public function delete_submenu($submenu_id)
	{
		$this->db->set('menu_status', 'deleted');

		$this->db->where('submenu_id', $submenu_id);
		$this->db->update('system_submenu');

		$this->session->set_flashdata("success_message","Menu has been delete.");
		admin_redirect("module/list_menu");
	}

	public function cms_menu()
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];
		
		$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
		$this->admin_library->setDetail($this->submenu['menu_title']);
		$this->admin_library->view("module/cmsview",$this->_data);
		$this->admin_library->output($this->path);
	}

	public function load_layout()
	{
		$result_data 	= $this->module_model->list_all_layout();

		$output = array(
			"iTotalRecords" => $result_data->num_rows(),
			"iTotalDisplayRecords" => "10",
				"aData" => $result_data->result()
			);

		echo json_encode($output);
	}

	public function edit_cms_menu()
	{
		$this->_data['_menu_name'] 		= "Edit Sub Menu";
		$this->_data['_menu_icon']		= "glyphicon-plus-sign";
		$this->_data['_menu_title']		= "แก้ไขเมนูย่อย";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['result']			= $this->module_model->list_all_menu();
		


		$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
		$this->admin_library->setDetail($this->_data['_menu_title']);
		$this->admin_library->view("module/editsubview",$this->_data);
		$this->admin_library->output($this->path);
	}

	public function cms_template()
	{
		redirect(base_url().'public/core/');
	}

}
?>