<?php
class Member extends CI_Controller{
	
	var $_data = array();
	var $_menu_name = "";
	var $menu;
	var $submenu;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('admin_library');	
		$this->load->library('upload');
		$this->admin_library->forceLogin();
		$this->load->model('member_admin_model');

		$this->path 	= 	$this->uri->ruri_string();
		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(1));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(1), $this->uri->segment(2));
	}
	

	public function index()
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');
		
		$this->admin_library->setTitle($this->_data['_menu_name'],$this->menu['menu_icon']);
		$this->admin_library->setDetail($this->menu['menu_title']);
		$this->admin_library->view("member/listview",$this->_data); 
		$this->admin_library->output($this->path);
	}

	public function load_datatable()
	{
		$result_data 	= $this->member_admin_model->dataTable();

		$output = array(
			"iTotalRecords" => $result_data->num_rows(),
			"iTotalDisplayRecords" => "25",
 	           "aData" => $result_data->result()
 	       );

		echo json_encode($output);
	}

	public function add($lang_id="TH"){
		
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		// $this->admin_library->add_breadcrumb($this->admin_library->getLanguagename($this->_data['lang_id']),"member/add/".$this->_data['lang_id'],"icon-globe");
		
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules("content_subject","ห้วข้อ","trim|required|max_length[255]");

		if($this->form_validation->run()===false){
			
			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata('error_message');
			$this->admin_library->setTitle($this->_menu_name,'glyphicon-list-alt');
			$this->admin_library->setDetail("สมัครสมาชิก Professional".$this->_menu_name."");
			$this->admin_library->view("member/add",$this->_data); 
			$this->admin_library->output($this->path);
		}else{
			
			
			$member_class 			= $this->input->post('member_class');
			$email 					= $this->input->post('email');
			$telephone 				= $this->input->post('telephone');
			$firstname 				= $this->input->post('firstname');
			$lastname 				= $this->input->post('lastname');
			$gender 				= $this->input->post('gender');
			$birthdate 				= $this->input->post('year')."-".$this->input->post('month')."-".$this->input->post('day');
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("member/edit/".$member_id.'/'.$lang_id);
		}
	}

	public function edit($member_id,$lang_id="TH")
	{

		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];
		
		$lang_id = strtoupper($lang_id);

		if($lang_id==NULL)
		{
			$lang_id = $this->member_admin_model->checkDefaultLang($member_id);
		}

		$this->member_admin_model->checkExistst($member_id);
		$this->_data['row'] = $this->member_admin_model->getDetail($member_id,$lang_id);

		if(!$this->_data['row']){
			$this->member_admin_model->addLanguage($member_id,$lang_id);
			$this->_data['row'] = $this->member_admin_model->getDetail($member_id,$lang_id);
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");	
		}

		$this->_data['address'] = $this->member_admin_model->get_address_member($member_id);
		
		$this->load->library('form_validation');
	
		if($this->form_validation->run()===false){

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle('Member','glyphicon-list-alt');
			$this->admin_library->setDetail("รายชื่อสมาชิก");
			$this->admin_library->view("member/edit",$this->_data); 
			$this->admin_library->output($this->path);
		}else{
			
			$this->member_admin_model->updateContentStatus(
				$this->input->post("member_id"),
				$this->input->post("main_status")
			);
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("member/edit/".$member_id."/".$lang_id);
		}
	}

	public function list_class($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');
		
		$this->admin_library->setTitle($this->_data['_menu_name'],$this->menu['menu_icon']);
		$this->admin_library->setDetail($this->menu['menu_title']);
		$this->admin_library->view("member/listview_class",$this->_data); 
		$this->admin_library->output($this->path);	
	}

	public function load_datatable_class()
	{
		$result_data 	= $this->member_admin_model->dataTable_class();

		$output = array(
			"iTotalRecords" => $result_data->num_rows(),
			"iTotalDisplayRecords" => "25",
 	           "aData" => $result_data->result()
 	       );

		echo json_encode($output);
	}

	public function add_class($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];
		
		$lang_id = strtoupper($lang_id);
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("class_name"	,"Class Name","trim|required");
		$this->form_validation->set_rules("member_class_adjust_value"	,"ยอดสั่งซื้อรวมขั้นต่ำ","trim|required|numeric");
	
		if($this->form_validation->run()===false){

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle('Class Member','glyphicon-user');
			$this->admin_library->setDetail("ระดับสมาชิก");
			$this->admin_library->view("member/add_class",$this->_data); 
			$this->admin_library->output($this->path);
		}else{
			
			$_class_id = $this->member_admin_model->addClass(
				$this->input->post('class_name'),
				$this->input->post('class_detail'),
				$this->input->post("class_status")
			);

			$this->member_admin_model->addClassAdjustmentValue(
				$_class_id,
				$this->input->post('member_class_adjust_value')
			);			
			
			$this->session->set_flashdata("success_message","Content has been update.");
			admin_redirect("member/list_class/".$lang_id);
		}
	}

	public function edit_class($member_id,$lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];
		
		$lang_id = strtoupper($lang_id);

		$this->member_admin_model->checkExiststClass($member_id);
		$this->_data['row'] = $this->member_admin_model->getDetailClass($member_id,$lang_id);

		$_member_class_adjustment_value = $this->member_admin_model->getClassAdjustmentValue($this->_data['row']['id']);
		$this->_data['row']['member_class_adjust_value'] = $_member_class_adjustment_value['adjust_value'];
		//echo "<pre>"; print_r($this->_data['row']); exit;

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");	
		}
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("class_name"	,"Class Name","trim|required");
		$this->form_validation->set_rules("member_class_adjust_value"	,"ยอดสั่งซื้อรวมขั้นต่ำ","trim|required|numeric");
	
		if($this->form_validation->run()===false){

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle('Class Member','glyphicon-user');
			$this->admin_library->setDetail("ระดับสมาชิก");
			$this->admin_library->view("member/edit_class",$this->_data); 
			$this->admin_library->output($this->path);
		}else{
			
			$this->member_admin_model->updateClass(
				$member_id,
				$this->input->post('class_name'),
				$this->input->post('class_detail'),
				$this->input->post("class_status")
			);

			$this->member_admin_model->updateClassAdjustmentValue(
				$member_id,
				$this->input->post('member_class_adjust_value')
			);
			
			$this->session->set_flashdata("success_message","Content has been update.");
			admin_redirect("member/edit_class/".$member_id."/".$lang_id);
		}
	}

	public function history($member_id,$lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		// if($lang_id==NULL)
		// {
		// 	$lang_id = $this->member_admin_model->checkDefaultLang($member_id);
		// }

		$this->member_admin_model->checkExistst($member_id);
		// $this->_data['row'] = $this->member_admin_model->getDetail($member_id,$lang_id);
		$this->_data['row'] = $this->member_admin_model->get_order_member_id($member_id);

		// if(!$this->_data['row']){
		// 	$this->member_admin_model->addLanguage($member_id,$lang_id);
		// 	$this->_data['row'] = $this->member_admin_model->getDetail($member_id,$lang_id);
		// }

		// if(!$this->_data['row']){
		// 	show_error("ไม่พบข้อมูลในระบบ");	
		// }
		
		$this->admin_library->setTitle('Member','glyphicon-list-alt');
		$this->admin_library->setDetail("ประวัติสั่งซื้อ");
		$this->admin_library->view("member/history",$this->_data); 
		$this->admin_library->output($this->path);
	}

	function delete($member_id)
	{
		$this->member_admin_model->delData($member_id);
		admin_redirect("member/index/");
	}
	function delete_class($member_id)
	{
		$this->member_admin_model->delDataClass($member_id);
		admin_redirect("member/list_class/");
	}
	public function handle_delete()
	{
		$member_id = $this->input->post("main_id");	
	
		foreach($member_id as $id){
			$this->member_admin_model->delData($id);
		}
		admin_redirect("member/index/");
	}
	public function handle_suspend()
	{
		$member_id = $this->input->post("main_id");
		
		foreach($member_id as $id){
			$this->member_admin_model->setStatus($id,"pending");
		}
		admin_redirect("member/index/");
	}
	public function handle_unsuspend()
	{
		$member_id = $this->input->post("main_id");
		
		foreach($member_id as $id){
			$this->member_admin_model->setStatus($id,"active");
		}
		admin_redirect("member/index/");
	}
	public function handle_suspend_member()
	{
		$member_id = $this->input->post("member_id");
		
		$this->member_admin_model->setStatus($member_id,"pending");
		admin_redirect("member/edit/".$member_id);
	}
	public function handle_unsuspend_member()
	{
		$member_id = $this->input->post("member_id");
		
		$this->member_admin_model->setStatus($member_id,"active");
		admin_redirect("member/edit/".$member_id);
	}
	public function set_sequence()
	{
		$member_id = $this->input->post("hid_member_id");
		$new_sequence = $this->input->post("new_sequence");	
		$old_sequence = $this->input->post("old_sequence");	
		$this->member_admin_model->set_sequence($member_id,$old_sequence,$new_sequence);
		admin_redirect("member/index");
	}
	function delete_img_member($img_id,$member_id,$img_name,$lang)
	{
		$path = './public/uploads/member/'.$img_name;
		$rs = $this->member_admin_model->deleteImage($img_id);
		if($rs)
		{
			if(unlink($path))
			{
				$this->session->set_flashdata("success_message","Content has been create image.");
			}else{
				$this->session->set_flashdata("error_message",'Error! Can not Delete Image');
			}
		}
		admin_redirect("member/edit/".$member_id."/".$lang);
	}
	
	function export_member()
	{
		require_once 'application/libraries/PHPExcel/Classes/PHPExcel.php';

        $query                  = ""
                . " SELECT          member.*, member_class.class_name AS class_name"
                . " FROM            member AS member"
                . " LEFT JOIN  		member_class ON member.member_class=member_class.id"
                . "";
        $rs             = $this->db->query($query)->result_array();
        $len            = count($rs);


        
        $TOTAL_PRICE    = 0;
        
        
        $objPHPExcel    = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setTitle('Tipfood Register Member');
        $worksheet      = $objPHPExcel->setActiveSheetIndex(0);
        $rowIndex       = 1;
        $page           = 1;
        $worksheet->setTitle('Page '.$page);
        $worksheet  ->setCellValueByColumnAndRow(0, 1, 'Member ID')
                    ->setCellValueByColumnAndRow(1, 1, 'Member Class')
                    ->setCellValueByColumnAndRow(2, 1, 'Email')
                    ->setCellValueByColumnAndRow(3, 1, 'Telephone')
                    ->setCellValueByColumnAndRow(4, 1, 'Fullname')
                    ->setCellValueByColumnAndRow(5, 1, 'Gender')
                    ->setCellValueByColumnAndRow(6, 1, 'Birthdate')
                    // ->setCellValueByColumnAndRow(26, 1, 'E-New')
                    ->setCellValueByColumnAndRow(7, 1, 'Create By')
                    ->setCellValueByColumnAndRow(8, 1, 'Create Date')
                    ->setCellValueByColumnAndRow(9, 1, 'Status');

		for( $j=0;  $j<$len;  $j++ ) {

	        $query_address 			= ""
	        		. " SELECT 			* "
	        		. " FROM 			member_address "
	        		. " WHERE 			member_id = '" .$rs[$j]['member_id']."'"
	        		. " AND 			address_status != 'deleted'"
	        		. "";

	        $rs_address		= $this->db->query($query_address)->result_array();
			$len_address 	= count($rs_address);

				for( $k=0; $k<$len_address; $k++ ) {

					$worksheet->setCellValueByColumnAndRow(10+($k), 1, 'Address '.($k+1));
				}
	    }


        $worksheet->getColumnDimension('A')->setWidth(5);
        $worksheet->getColumnDimension('B')->setWidth(5);
        $worksheet->getColumnDimension('C')->setWidth(20);
        $worksheet->getColumnDimension('D')->setWidth(15);
        $worksheet->getColumnDimension('E')->setWidth(25);
        $worksheet->getColumnDimension('F')->setWidth(20);
        $worksheet->getColumnDimension('G')->setWidth(15);
        $worksheet->getColumnDimension('H')->setWidth(15);

        for( $i=0;  $i<$len;  $i++ ) {
            $rowIndex++;

            $worksheet->setCellValueByColumnAndRow(0, $rowIndex, $rs[$i]['member_id']);
            $worksheet->setCellValueByColumnAndRow(1, $rowIndex, $rs[$i]['class_name']);
            $worksheet->setCellValueByColumnAndRow(2, $rowIndex, $rs[$i]['email']);
            $worksheet->setCellValueByColumnAndRow(3, $rowIndex, $rs[$i]['telephone']);
            $worksheet->setCellValueByColumnAndRow(4, $rowIndex, $rs[$i]['firstname']." ".$rs[$i]['lastname']);
            $worksheet->setCellValueByColumnAndRow(5, $rowIndex, $rs[$i]['gender']);
            $worksheet->setCellValueByColumnAndRow(6, $rowIndex, $rs[$i]['birthdate']);
            // $worksheet->setCellValueByColumnAndRow(7, $rowIndex, $rs[$i]['is_enews']);
            $worksheet->setCellValueByColumnAndRow(7, $rowIndex, ( $rs[$i]['post_by'] == "0" ? "Member" : $this->admin_library->getUser($rs[$i]['post_by'])) );
            $worksheet->setCellValueByColumnAndRow(8, $rowIndex, $rs[$i]['post_date']);
            $worksheet->setCellValueByColumnAndRow(9, $rowIndex, $rs[$i]['status']);

		        $query_address 			= ""
		        		. " SELECT 			member_address.*, system_province.province_name_th AS province_name, system_district.district_name_th AS district_name, system_sub_district.sub_district_name_th AS sub_district_name"
		        		. " FROM 			member_address "
		        		. " Left Join 		system_province ON member_address.province_id=system_province.province_id"
		        		. " LEft Join 		system_district ON member_address.district_id=system_district.district_id"
		        		. " LEFT Join 		system_sub_district ON member_address.sub_district_id=system_sub_district.sub_district_id"
		        		. " WHERE 			member_address.member_id = '" .$rs[$i]['member_id']."'"
		        		. " AND 			member_address.address_status != 'deleted'"
		        		. "";

		        $rs_address		= $this->db->query($query_address)->result_array();
				$len_address 	= count($rs_address);

			 	for( $k=0; $k<$len_address; $k++ ) {

			 		$address = $rs_address[$k]['address_no'].", ".$rs_address[$k]['address_soi']." ".$rs_address[$k]['address_road']." ".$rs_address[$k]['sub_district_name']." ".$rs_address[$k]['district_name']." ".$rs_address[$k]['province_name'].", ".$rs_address[$k]['postal_code'];

			 		$worksheet->setCellValueByColumnAndRow(10+($k), $rowIndex, $address);
			 	}


        }
        $rowIndex++;
        
        /*
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment;filename="01simple.xlsx"');
         header('Cache-Control: max-age=0');
        */
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Member_Profile_' . date('Y-m-d') . '.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}
	
}