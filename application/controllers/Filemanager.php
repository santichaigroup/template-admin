<?php
class Filemanager extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('admin_library');	
		$this->load->library('image_library');
		$this->admin_library->forceLogin();
	}

	function index($REQUEST_METHOD="GET",$image_cate=null,$menu_link=null,$attachment_id=null,$attachment_name=null)
	{
		$CUI = $this->image_library->checkUploadImage($menu_link, $image_cate)->row_array();

		if($_SERVER['REQUEST_METHOD']=="POST") {

			if($REQUEST_METHOD=="PUT") {

				return $this->update($CUI);
			} else if($REQUEST_METHOD=="DELETE") {

				return $this->delete($attachment_id,$attachment_name,$CUI);
			} else {

				return $this->upload($CUI);
			}
		} else {

			return $this->loading($CUI);
		}
	}
	function loading(array $CUI = array())
	{
		if($CUI) {

			$menu_link 		= $CUI['menu_link'];
			$image_cate 	= $CUI['image_cate'];
			$image_lang 	= $CUI['image_lang'];
			$lang_id 		= $this->input->get('lang_id');
			$option 		= $this->input->get('option');
			$main_id 		= ( null !== $this->input->get('main_id') ? $this->input->get('main_id') : $this->security->get_csrf_hash() );

			$image_array 					= [];
			$image_array[0]['status'] 		= false;
			$image_array[0]['messages'] 	= "";

			if(validation_errors()==NULL && !$option) {

				$this->image_library->refreshHash($menu_link, $this->security->get_csrf_hash());
			} else {

				if($image_lang=="single") {
					$image_lists 	= $this->image_library->getAttachmentsList($menu_link,$image_cate,$main_id)->result_array();
				} else {
					$image_lists 	= $this->image_library->getAttachmentsList($menu_link,$image_cate,$main_id,$lang_id)->result_array();
				}

				if(!empty($image_lists)) {

					foreach ($image_lists as $i => $image) {

	                  	$check_attachment_name  = explode("_", $image['attachment_name']);
	                  	$attachment_name        = ( count($check_attachment_name) > 1 ? $image['attachment_name'] : null );

						$image_array[$i] 						= $image;
						$image_array[$i]['file_name']			= $image['attachment_name'];
						$image_array[$i]['attachment_id']		= $image['attachment_id'];
						$image_array[$i]['attachment_detail']	= $image['attachment_detail'];
						$image_array[$i]['sequence']			= $image['sequence'];
						$image_array[$i]['default_main_id']		= $main_id;
						$image_array[$i]['attachment_name_old']	= $attachment_name;
						$image_array[$i]['file_path']			= base_url("public/uploads/".$menu_link."/images/".$image['attachment_name']);
						$image_array[$i]['deleteType'] 			= 'POST';
						$image_array[$i]['deleteUrl'] 			= admin_url('filemanager/index/DELETE/'.$image_cate.'/'.$menu_link.'/'.$image['attachment_id'].'/'.$image['attachment_name']);
						$image_array[$i]['set_highlight'] 		= (!empty($this->input->get('main_id')) && $image_cate == "pc" ? true : false);
						$image_array[$i]['status'] 				= true;
						$image_array[$i]['messages'] 			= '';
					}
				}
			}

			echo json_encode(["files" => $image_array]);
		}
	}
	function upload(array $CUI = array())
	{
		if($CUI) {

			$this->load->library('upload');

			$menu_link 		= $CUI['menu_link'];
			$image_cate 	= $CUI['image_cate'];
			$lang_id 		= $this->input->post('lang_id');
			$main_id 		= ( null !== $this->input->post('main_id') ? $this->input->post('main_id') : $this->security->get_csrf_hash() );

			$image_array 	= [];
			$image_name 	= $CUI['image_name'];
			$image_lang 	= $CUI['image_lang'];
			$image_limit 	= $CUI['image_limit'];
			$image_allowed 	= $CUI['image_allowed'];
			$image_size		= $CUI['image_size'];
			$image_width 	= $CUI['image_width'];
			$image_height 	= $CUI['image_height'];

			$image_file 	= $_FILES[$image_name];
			$image_all_file = sizeof($image_file['tmp_name']);

			if($image_lang=="single") {

				$image_count 	= $this->image_library->checkAttachments($menu_link, $image_cate, $main_id);
			} else {

				$image_count 	= $this->image_library->checkAttachments($menu_link, $image_cate, $main_id, $lang_id);
			}

			$config['upload_path'] 		= 'public/uploads/'.$menu_link.'/images';
			$config['encrypt_name']		= true;
			$config['allowed_types'] 	= $image_allowed;
			$config['max_size']			= $image_size;
			$config['max_width']  		= $image_width;
			$config['max_height']  		= $image_height;

			for ($i = 0; $i < $image_all_file; $i++) {

				$_FILES[$image_name]['name'] 		= $image_file['name'][$i];
				$_FILES[$image_name]['type'] 		= $image_file['type'][$i];
				$_FILES[$image_name]['tmp_name'] 	= $image_file['tmp_name'][$i];
				$_FILES[$image_name]['error'] 		= $image_file['error'][$i];
				$_FILES[$image_name]['size'] 		= $image_file['size'][$i];

				$this->upload->initialize($config);
				if ($this->upload->do_upload($image_name)) {

					$image_array[$i] = $this->upload->data();

					// เช็คจำนวนอัพโหลดมากสุด
						if((count($image_array)+$image_count) > $image_limit) {

							$image_array[$i]['status'] 		= false;
							$image_array[$i]['messages'] 	= 'The uploaded file exceeds the maximum size allowed in '.$image_limit.' files';
						} else {

							$image_array[$i]['status'] 		= true;
							$image_array[$i]['messages'] 	= '';

							// อัพโหลดครั้งเดียวทุกภาษา
								if($image_lang=="single") {

									$attachment_id = $this->image_library->insertContent(
										$menu_link,
										$main_id,
										$image_array[$i]['file_name'],
										$image_cate
									);
								}
							// อัพโหลดภาษาละครั้ง
								else {

									$attachment_id = $this->image_library->insertContent(
										$menu_link,
										$main_id,
										$image_array[$i]['file_name'],
										$image_cate,
										$lang_id
									);
								}

								$image_array[$i]['attachment_id']	= $attachment_id;
								$image_array[$i]['default_main_id']	= $main_id;
								$image_array[$i]['attachment_detail']	= "";
								$image_array[$i]['attachment_name_old']	= "";
								$image_array[$i]['sequence']		= ( $image_count + 1 );
								$image_array[$i]['file_path']		= base_url("public/uploads/".$menu_link."/images/".$image_array[$i]['file_name']);
								$image_array[$i]['deleteType'] 		= 'POST';
								$image_array[$i]['deleteUrl'] 		= admin_url('filemanager/index/DELETE/'.$image_cate.'/'.$menu_link.'/'.$attachment_id.'/'.$image_array[$i]['file_name']);
								$image_array[$i]['set_highlight'] 	= (!empty($this->input->post('main_id')) && $image_cate == "pc" ? true : false);
						}

				} else {

					$image_array[$i] 				= $_FILES[$image_name];
					$image_array[$i]['status'] 		= false;
					$image_array[$i]['messages'] 	= strip_tags($this->upload->display_errors());
				}
			}

			echo json_encode(["files" => $image_array]);

		} else {

			$callback 					= $this->input->get('CKEditorFuncNum');
			$config['upload_path'] 		= 'public/uploads/editor/';
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['max_size']			= '2048';
			$config['max_width']  		= '2048';
			$config['max_height']  		= '2048';
			$config['encrypt_name']  	= true;
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('upload'))
			{
				$error = $this->upload->display_errors();
				echo '<script type="text/javascript">alert("'.$error.'"); window.parent.CKEDITOR.tools.callFunction('.$callback.', "","");</script>';
			}
			else
			{
				$data = $this->upload->data();
				$image_url = base_url('public/uploads/editor/'.$data ["file_name"]);
				echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction('.$callback.', "'.$image_url .'","");</script>';
				
				// $config['image_library'] = 'gd2';
				// $config['source_image']	= $data['full_path'];
				// $config['maintain_ratio'] = TRUE;
				// $config['width']	 = 800;
				// $config['height']	= 800;
				// $this->load->library('image_lib', $config); 
				// if($this->image_lib->resize()){
				// 	$image_url = "/public/uploads/editor/".$data ['file_name'];
				// 	echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction('.$callback.', "'.$image_url .'","");</script>';
				// }else{
				// 	$error = $this->image_lib->display_errors();
				// 	echo '<script type="text/javascript">alert("'.$error.'"); window.parent.CKEDITOR.tools.callFunction('.$callback.', "","");</script>';
				// }
			}
		}
	}
	function update(array $CUI = array())
	{
		$main_id 	= ( null !== $this->input->input_stream('main_id') ? $this->input->input_stream('main_id') : $this->security->get_csrf_hash() );
		$data 		= [
						'menu_link'				=> $CUI['menu_link'],
						'main_id'				=> $main_id,
						'attachment_id'			=> $this->input->input_stream('attachment_id'),
						'default_main_id'		=> $this->input->input_stream('default_main_id'),
						'attachment_name'		=> $this->input->input_stream('attachment_name'),
						'attachment_name_old'	=> $this->input->input_stream('attachment_name_old'),
						'attachment_name_main'	=> $this->input->input_stream('attachment_name_main'),
						'attachment_detail'		=> $this->input->input_stream('attachment_detail')
					];
		$this->image_library->updateContent($data);
	}
	function delete($attachment_id,$attachment_name,array $CUI = array())
	{
		$menu_link 	= $CUI['menu_link'];
		$menu_id 	= $CUI['menu_id'];
		$data 		= [
							'attachment_id'		=> $attachment_id,
							'attachment_name'	=> $attachment_name,
							'type_name'			=> 'images',
							'menu_name'			=> $menu_link
						];

		if($data['type_name'] == "images") {
			$path = './public/uploads/'.$menu_link.'/images/'.$attachment_name;
		} else {
			$path = './public/uploads/'.$menu_link.'/files/'.$attachment_name;
		}

		$checkImage = $this->image_library->deleteImage($menu_link,$attachment_id);

		if(!$checkImage) {

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = [
							'menu_link'		=> $menu_link,
							'menu_id' 		=> $menu_id,
							'submenu_id' 	=> 'delete_img',
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Can't delete image",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						];

				$this->load->library('logs_library');
				$this->logs_library->menu_backend_log($logs);

		} else {

			if(unlink($path)) {
				$this->session->set_flashdata("success_message","Delete file in Server success.");
			} else {
				$this->session->set_flashdata("error_message","Error! Can't delete file in Server.");
			}

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = [
							'menu_link'		=> $menu_link,
							'menu_id' 		=> $menu_id,
							'submenu_id' 	=> 'delete_img',
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Delete image success",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						];

				$this->load->library('logs_library');
				$this->logs_library->menu_backend_log($logs);

			echo true;
		}
	}
	function highlight($menu_link=null,$lang_id=null)
	{
		$default_main_id 	= $this->input->post('default_main_id');
		$attachment_id 		= ( $this->input->post('attachment_id') ? $this->input->post('attachment_id') : null );

		if($default_main_id) {

			$this->image_library->setDefaultImage($menu_link,$default_main_id,$attachment_id,$lang_id);

			$image = $this->image_library->getThumbnailImage($menu_link,$attachment_id)->row_array();

			if($image) {
				echo json_encode(
						[
							'file_path' => base_url("public/uploads/".$menu_link."/images/".$image['attachment_name'])
						]
					);
			} else {
				echo true;
			}
		}
		echo false;
	}
}  