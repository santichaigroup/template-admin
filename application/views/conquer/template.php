<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $asset_url; ?>" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <!-- DatePicker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="plugins/bootstrap-daterangepicker/daterangepicker.css">
    <!-- JsTree -->
    <link rel="stylesheet" href="plugins/jstree/dist/themes/default/style.min.css">
    <!-- Multiselect -->
    <link rel="stylesheet" href="plugins/multiselect/css/ui.multiselect.css">
    <!-- <link rel="stylesheet" href="plugins/jQueryUI/themes/flick/jquery-ui.min.css" /> -->
    <!-- Tag It -->
    <link rel="stylesheet" href="plugins/tag-it/css/jquery.tagit.css" />
    <!-- Color Picker -->
    <link rel="stylesheet" href="plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
    <!-- Chart -->
    <script src="plugins/chartjs/Chart.min.js"></script>

    <!-- Theme style -->
    <link rel="stylesheet" href="css/adminLTE.min.css">
    <!-- adminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="plugins/jQueryUI/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
      var admin_url = "<?php echo $admin_url; ?>";
      var base_url = "<?php echo $base_url; ?>";
      var site_url = "<?php echo $site_url; ?>";
      var asset_url = "<?php echo $asset_url; ?>";
      var menu_link = "<?php echo $_menu_link; ?>";
      $(function(){
        $.ajaxSetup({
            data: {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            }
        });
      });
    </script> 
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
    <script src="plugins/ckeditor/ckeditor.js"></script>
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- date-range-picker -->
    <script src="plugins/moment/moment.min.js"></script>
    <script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Fancy Box -->
    <script type="text/javascript" src="plugins/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <script type="text/javascript" src="plugins/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>

    <link rel="stylesheet" type="text/css" href="plugins/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" type="text/css" href="plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />

    <link rel="stylesheet" type="text/css" href="plugins/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="plugins/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    <script type="text/javascript" src="plugins/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

    <script type="text/javascript" src="js/moment.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">

    <script type="text/javascript" src="plugins/jstree/dist/jstree.min.js"></script>

    <script type="text/javascript" src="plugins/multiselect/plugins/localisation/jquery.localisation-min.js"></script>
    <script type="text/javascript" src="plugins/multiselect/plugins/ui.multiselect.js"></script>

    <script type="text/javascript" src="plugins/tag-it/js/tag-it.min.js"></script>
    
    <script type="text/javascript" src="plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <script type="text/javascript" src="plugins/input-mask/v3/inputmask.js"></script>
    <script type="text/javascript" src="plugins/input-mask/v3/jquery.inputmask.js"></script>
    <script type="text/javascript" src="plugins/input-mask/v3/inputmask.numeric.extensions.js"></script>

    <link rel="stylesheet" href="plugins/jQuery-file-upload/css/jquery.fileupload.css">
    <link rel="stylesheet" href="plugins/jQuery-file-upload/css/jquery.fileupload-ui.css">

    <script type="text/javascript" src="js/chosen.jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/chosen.css" />
    <link rel="stylesheet" type="text/css" href="css/styleAdmin.css" />

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

<?php
    ##############################################   Header Menu   #############################################
?>

      <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo admin_url(); ?>" class="logo">
          <span class="logo-mini"><b>A</b>min</span>

          <span class="logo-lg"><b><?php echo $this->admin_library->getCompanyName(); ?></b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <?php echo $header_bar; ?>

      </header>

<?php
    ##############################################   Left Menu   #############################################
?>

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <section class="sidebar">
          
          <div class="user-panel">
            <span class="user-company">
              
                <?php $group_name = $this->admin_library->getGroupDetail($user_info['user_group']); echo $group_name['group_name']; ?>
              
            </span>
          </div>

          <!-- sidebar menu: -->

          <?php echo $left_menu; ?>

          <!-- sidebar menu: -->
          
        </section>
      </aside>

<?php
    ##############################################   Main Menu   #############################################
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

          <?php echo $page_navi; ?>

          <!-- Main content -->
          <section class="content">

              <!-- Main Body -->
              
              <?php echo $body_entry; ?>

              <!-- Main Body -->

          </section>
          <!-- Main Body -->

      </div>
      <!-- Content Wrapper. Contains page content -->

<?php
    ##############################################   Footer Menu   #############################################
?>

<!--       <footer class="main-footer">
        <div class="pull-right hidden-xs">

        </div>
      </footer> -->

<?php
    ##############################################   Tools Menu   #############################################
?>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs"></ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <!-- Themes -->
          </div>
          <!-- Home tab content -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

<?php
    ##############################################   JS   #############################################
?>

    <!-- adminLTE App -->
    <script src="js/app.min.js"></script>
    <!-- adminLTE for demo purposes -->
    <script src="js/themes.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- CKEDITOR -->
    <script type="text/javascript">
      $(function () {
        
        //Initialize Select2 Elements
        $(".select2").select2();
        // Date Picker
        $('.date-picker').daterangepicker({
          "autoApply": true,
          "timePicker": true,
          "timePicker24Hour": true,
          "singleDatePicker": true,
          "locale":{
              "format": 'YYYY-MM-DD H:mm:ss'
          }
        });
        // Date Rang Picker
        $('.dateRangePicker').daterangepicker({
          autoUpdateInput: false,
          "drops": "down",
          "autoApply": false,
          timePicker: false,
          "timePicker24Hour": false,
          locale: {
            format: 'YYYY-MM-DD HH:mm:ss'
          }
        });
        // Fancy box
        $('.fancybox-buttons').fancybox({
          openEffect  : 'none',
          closeEffect : 'none',

          prevEffect : 'none',
          nextEffect : 'none',

          closeBtn  : true,

          helpers : {
            title : {
              type : 'inside'
            },
            buttons : {}
          },

          afterLoad : function() {
            this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
          }
        });
        // ui move
        $( "#sortable" ).sortable({
            appendTo: "body",
            helper: "clone"
        }).disableSelection();
      });
    </script>

    <?php if(!empty($system_image)) : ?>
      <!-- The Templates plugin is included to render the upload/download listings -->
      <script src="plugins/jQuery-file-upload/blueimp/tmpl.min.js"></script>
      <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
      <script src="plugins/jQuery-file-upload/blueimp/load-image.all.min.js"></script>
      <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
      <script src="plugins/jQuery-file-upload/js/jquery.iframe-transport.js"></script>
      <!-- The basic File Upload plugin -->
      <script src="plugins/jQuery-file-upload/js/jquery.fileupload.js"></script>
      <!-- The File Upload processing plugin -->
      <script src="plugins/jQuery-file-upload/js/jquery.fileupload-process.js"></script>
      <!-- The File Upload image preview & resize plugin -->
      <script src="plugins/jQuery-file-upload/js/jquery.fileupload-image.js"></script>
      <!-- The File Upload validation plugin -->
      <script src="plugins/jQuery-file-upload/js/jquery.fileupload-validate.js"></script>
      <!-- The File Upload user interface plugin -->
      <script src="plugins/jQuery-file-upload/js/jquery.fileupload-ui.js"></script>
      <!-- The main application script -->
      <script src="plugins/jQuery-file-upload/js/main.js"></script>
      <!-- NOT USE!!! because SET Upload AUTO !!! The template to display files available for upload -->
      <script id="template-upload" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <div class="col-md-3 col-sm-3 col-xs-3 image_select template-upload fade">
              <div class="thumbnail image_box text-center">
                <a href="javascript:;"><span class="preview"></span></a>

                {% if (window.innerWidth > 480 || !o.options.loadImageFileTypes.test(file.type)) { %}
                    <p>File name: <span class="name">{%=file.name%}</span><br>File size: <span class="size">Processing...</span></p>
                {% } %}
                <strong class="error text-danger"></strong>

                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>

                {% if (!i && !o.options.autoUpload) { %}
                  <button class="btn btn-primary start hidden" disabled><i class="glyphicon glyphicon-upload"></i><span>Start</span></button>
                {% } %}
                {% if (!i) { %}
                  <div class="image_tools flex-center hover-opacity cancel"><i class="fa fa-remove text-red"></i></div>
                {% } %}
              </div>
            </div>
        {% } %}
      </script>
      <!-- The template to display files available for download -->
      <script id="template-download" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
          {% if (file.status) { %}
            <div class="col-md-3 col-sm-3 col-xs-3 image_select template-download fade">
                <div class="thumbnail image_box text-center">
                  <span class="preview">
                    {% if (file.file_path) { %}
                      <a class="fancybox-buttons" data-fancybox-group="button" href="{%=file.file_path%}" title="{%=file.file_name%}" data-gallery>
                        <img class="img-responsive" data-holder-rendered="true" src="{%=file.file_path%}">
                      </a>
                    {% } %}
                  </span>

                  {% if (file.deleteUrl) { %}
                    <div class="image_tools_select flex-center hover-opacity delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}">
                      <i class="fa fa-remove text-red"></i>&nbsp;&nbsp;&nbsp;
                      <i class="glyphicon glyphicon-move text-light-blue" style="font-size: 45px;"></i>
                    </div>
                    <input type="checkbox" name="delete" value="1" class="toggle hidden">
                  {% } else { %}
                    <div class="image_tools_select flex-center hover-opacity cancel">
                      <i class="fa fa-remove text-red"></i>&nbsp;&nbsp;&nbsp;
                      <i class="glyphicon glyphicon-move text-light-blue" style="font-size: 45px;"></i>
                    </div>
                  {% } %}

                  <input type="hidden" name="attachment_id[]" value="{%=file.attachment_id%}">
                  <input type="hidden" name="default_main_id" value="{%=file.default_main_id%}">
                  <input type="hidden" name="sequence" value="{%=file.sequence%}">
                </div>

                <div style="margin-top: 0px; margin-bottom: 20px;">
                  <div class="form-group">
                    <label for="attachment_name" class="control-label">Image Name: </label>
                    <input type="text" name="attachment_name[{%=file.attachment_id%}]" class="form-control" value="{%=file.attachment_name_old%}">
                    {% if (!file.attachment_name_old) { %}
                    _{%=file.file_name%}
                    {% } %}
                    <input type="hidden" name="attachment_name_old[{%=file.attachment_id%}]" class="form-control" value="{%=file.attachment_name_old%}">
                    <input type="hidden" name="attachment_name_main[{%=file.attachment_id%}]" class="form-control" value="{%=file.file_name%}">
                  </div>

                  <div class="form-group">
                    <label for="attachment_detail" class="control-label">Image Alt: </label>
                    <textarea class="form-control" name="attachment_detail[{%=file.attachment_id%}]" rows="2">{%=file.attachment_detail%}</textarea>
                  </div>

                  {% if (file.set_highlight) { %}
                    <a href="javascript:;" class="btn btn-success btn-block text-center set_highlight" data-default_main_id="{%=file.default_main_id%}" data-attachment_id="{%=file.attachment_id%}">
                      <i class="glyphicon glyphicon-pushpin"></i>&nbsp;&nbsp;&nbsp;Set Image highlight
                    </a>
                  {% } %}
                </div>
            </div>
          {% } else if(!file.status && file.messages) { %}
            <div class="col-md-3 col-sm-3 col-xs-3 image_select template-download fade">
                <div class="thumbnail image_box text-center">
                  <p class="preview">
                    <span class="label label-danger" style="font-size: 12px;"><i class="glyphicon glyphicon-alert"></i>   Error</span>
                  </p>

                  {% if (file.messages) { %}
                      <p>{%=file.messages%}</p>
                  {% } %}

                  <div class="image_tools_select flex-center hover-opacity cancel">
                    <i class="fa fa-remove text-red"></i>&nbsp;&nbsp;&nbsp;
                  </div>
                </div>
            </div>
          {% } %}
        {% } %}
      </script>
    <?php endif; ?>

  </body>
</html>