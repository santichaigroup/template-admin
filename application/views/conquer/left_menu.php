<ul class="sidebar-menu">

<?php $html_main = ""; $html_setting = ""; ?>

<?php foreach($menu_entry as $rs) { ?>

	<?php if($rs['sequent'] >= 900) {

			if(count($rs['submenu_entry'])!=0) {

			$html_setting .= "<li class='".$rs['active']."'>

				  <a href='javascript:;''>
				    	<i class='glyphicon ".$rs['icon']."'></i> 
				    	<span></i> ".$rs['label']."</span> 
				    	<i class='fa fa-angle-left pull-right'></i>
				  </a>";

			$html_setting .= "<ul class='treeview-menu'>";

				  		foreach($rs['submenu_entry'] as $submenu) {

			$html_setting .= "<li class='".$submenu['active']."'>
				    		<a href='".$submenu['link']."'>
				    			<i class='fa fa-angle-right'></i> 
				    			".$submenu['label']."
				    		</a></li>";

				    	}

			$html_setting .= "</ul></li>";

			} else {

			$html_setting .= "<li class='".$rs['active']."'>
				<a href='".$rs['link']."'>
					<i class='glyphicon ".$rs['icon']."'></i> 
					<span>".$rs['label']."</span>
				</a></li>";

			}
	
		} else { ?>

		<?php if(count($rs['submenu_entry'])!=0) {

			$html_main .= "<li class='".$rs['active']."'>

				  <a href='javascript:;'>
				    	<i class='glyphicon ".$rs['icon']."'></i> 
				    	<span></i> ".$rs['label']."</span> 
				    	<i class='fa fa-angle-left pull-right'></i>
				  </a>";

			$html_main .= "<ul class='treeview-menu'>";

			foreach($rs['submenu_entry'] as $submenu) {

			$html_main .= "<li class='".$submenu['active']."'>
				    		<a href='".$submenu['link']."'>
				    			<i class='fa fa-angle-right'></i> 
				    			".$submenu['label'];

					########################## Add Alert Number #########################

						if($submenu['noti'] != "no") {

							if(!$this->db->simple_query('SELECT * FROM '.$submenu['database'])) {

								show_error("Check database column 'menu_database' in system_submenu.");
							} else {

								$countData 	= $this->db->where($submenu['column'], $submenu['noti'])->count_all_results($submenu['database']);

								$html_main .= '<small class="label pull-right bg-yellow">'.$countData.'</small>';
							}
						}

					########################## Add Alert Number #########################

			$html_main .= "</a></li>";

				    	}

			$html_main .= "</ul></li>";

			} else {

			$html_main .= "<li class='".$rs['active']."'>
				<a href='".$rs['link']."'>
					<i class='glyphicon ".$rs['icon']."'></i> 
					<span>".$rs['label']."</span>";
			$html_main .= "</a></li>";

			} } } ?>


<li class="header">MAIN MENU</li>

<?php echo $html_main; ?>

<?php if($html_setting) : ?>
<li class="header">SETTING MENU</li>

<?php echo $html_setting; ?>
<?php endif; ?>
</ul>