class <?php echo $class_name; ?> extends CI_Controller {

	var $_data = array(),
		$_menu_name,
		$menu,
		$submenu;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('admin_library');
		$this->load->library('image_library');
		$this->admin_library->forceLogin();
		$this->load->model('<?php echo $class_name; ?>_model');
		$this->load->library('form_validation');

		$this->path 	= 	$this->uri->ruri_string();
		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(1));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(1), $this->uri->segment(2));

		$this->setLanguage			= "th";
		$this->_data['language'] 	= "<?php echo $language; ?>";
	}

	public function index($lang_id=NULL)
	{
		$this->_data['lang_id'] 		= $this->language_setting($lang_id);
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');

		// ---------- Tools Sequent ---------- //
			$seq 		= $this->input->post('seq');
			$content_id = $this->input->post('content_id');
			if($content_id) {

				foreach ($content_id as $key => $value) {

					$this-><?php echo $class_name; ?>_model->setSequence($key, $value);
				}
			}

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->menu['menu_icon']);
		$this->admin_library->setDetail($this->menu['menu_title']);
		$this->admin_library->view("<?php echo strtolower($class_name); ?>/listview",$this->_data);
		$this->admin_library->output($this->path);
	}

	public function load_datatable($lang_id=NULL)
	{
		$this->form_validation->set_rules("length","Length","trim|required|integer");
		$this->form_validation->set_rules("start","Start","trim|required|integer");

		if($this->form_validation->run()===false) {

			echo json_encode([]);
		} else {
		
			$limit 			= $this->input->post('length');
			$start 			= $this->input->post('start');
			$is_order 		= array();
			$is_search 		= array();
			$result_array 	= array();

			// ---------- Column all ---------- //
				if(!empty($this->input->post('columns'))) {

					foreach ($this->input->post('columns') as $key => $column) {

						$is_order[$key] = $column['data'];

						if($column['search']['value']) {

							$is_search[$column['data']] 	= $column['search']['value'];
						}
					}
				}

			// ---------- Orderable ---------- //
				if(!empty($this->input->post('order')[0]['column'])) {

					$is_order = array($is_order[$this->input->post('order')[0]['column']] => $this->input->post('order')[0]['dir']);
				} else {

					$is_order = array();
				}

			// ---------- Searchable ---------- //
				if(empty($is_search)) {

					$all_data 		= $this-><?php echo $class_name; ?>_model->dataTable($lang_id);
		            $query_data 	= $this-><?php echo $class_name; ?>_model->dataTable($lang_id, $limit, $start, $is_order)
		            															->result_array();
		        } else {

					$all_data 		= $this-><?php echo $class_name; ?>_model->dataTable($lang_id, null, null, null, $is_search);
					$query_data 	= $this-><?php echo $class_name; ?>_model->dataTable($lang_id, $limit, $start, $is_order, $is_search)
																				->result_array();
				}

			if(!empty($query_data)) {

				foreach ($query_data as $key => $value) {
					$result_array[$key] = $value;
					$result_array[$key]['no'] = ($key+1)+$start;
					$result_array[$key]['post_date_format'] = date_format_convert_shortdatetime($value['post_date']);
				}
			}

			$output = array(
							"draw"            	=> intval($this->input->post('draw')),
							"recordsFiltered"   => intval($all_data),
							"recordsTotal" 		=> count($result_array),
							"data"            	=> $result_array
						);

			echo json_encode($output);
		}
	}

	public function add($lang_id=NULL)
	{
		$this->_data['lang_id'] = $this->language_setting($lang_id);
		$lang_id 				= $this->_data['lang_id'];

		$this->form_validation->set_rules("content_subject","Title Name","trim|required|max_length[255]");
		// $this->form_validation->set_rules("content_detail","Description","trim|required");

		// เช็คอัพโหลดไฟล์
		// $this->form_validation->set_rules("file_thumb[]","Files Attachment","callback_fileupload_files");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']			= "Add ".$this->menu['menu_label'];
			$this->_data['_menu_icon']			= "glyphicon-plus-sign";
			$this->_data['_menu_title']			= $this->menu['menu_title'];
			$this->_data['_menu_link']  		= $this->menu['menu_link'];
			$this->_data['system_image'] 		= $this->image_library->checkUploadImage($this->menu['menu_link'])->result_array();

			$this->_data['validation_errors'] 	= validation_errors();
			$this->_data['error_message'] 		= $this->session->flashdata('error_message');

			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("<?php echo strtolower($class_name); ?>/add",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$main_id 		= $this-><?php echo $class_name; ?>_model->addData();
			$content_id 	= $this-><?php echo $class_name; ?>_model->addLanguage($main_id,$lang_id);

			$this-><?php echo $class_name; ?>_model->setDefaultContent($main_id,$lang_id,$content_id);
			$this-><?php echo $class_name; ?>_model->setDate($main_id, $this->input->post("post_date"));

			$data = array(
						'main_id'				=> $main_id,
						'lang_id'				=> $lang_id,
						'content_subject' 		=> $this->input->post("content_subject"),
						'content_detail'		=> $this->input->post("content_detail"),
						'content_title'			=> ($this->input->post("content_title") ? 
														$this->input->post("content_title") : 
															$this->input->post("content_subject")),
						'content_keyword'		=> $this->input->post("content_keyword"),
						'content_description'	=> $this->input->post("content_description"),
						'content_status'		=> $this->input->post("content_status"),
						'content_seo'			=> $this->input->post("content_subject"),
						'content_rewrite_id'	=> md5($this->menu['menu_id'].$main_id.time())
					);

			$checkUpdate = $this-><?php echo $class_name; ?>_model->updateContent($data);

			// ############# Images Upload ############# //
				$data_image = array(
								'menu_link'				=> $this->menu['menu_link'],
								'main_id'				=> $main_id,
								'default_main_id'		=> $this->input->post("default_main_id"),
								'attachment_id'			=> $this->input->post("attachment_id"),
								'attachment_name'		=> $this->input->post("attachment_name"),
								'attachment_name_old'	=> $this->input->post("attachment_name_old"),
								'attachment_name_main'	=> $this->input->post("attachment_name_main"),
								'attachment_detail'		=> $this->input->post("attachment_detail")
							);

				$this->image_library->updateContent($data_image);

			// ############# Files Upload ############# //
				if($this->_data['file_thumbnail']) {

					$data['file_thumb'] = $this->_data['file_thumbnail'];
					if($data['file_thumb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['file_thumb'] as $thumbnail )
						{
							$this-><?php echo $class_name; ?>_model->insertContent(
								$main_id,
								$thumbnail
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['file_thumb'] as $thumbnail )
						// {
						// 	$this-><?php echo $class_name; ?>_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

			if(!$checkUpdate) {

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$this->session->set_flashdata("success_message","Content can't create.");
					$this->logs($data, __Function__, "add", "failure");

			} else {

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$this->session->set_flashdata("success_message","Content has been create.");
					$this->logs($data, __Function__, "add", "success");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$this->rewrite_update($data, 'detail');
			}

			// ---------- Redirect Page ---------- //
				if($this->_data['language']=="single") {
					admin_redirect($this->menu['menu_link']."/edit/".$main_id);
				} else {
					admin_redirect($this->menu['menu_link']."/edit/".$main_id."/".$lang_id);
				}
		}
	}

	public function edit($main_id,$lang_id=NULL)
	{
		$this->_data['lang_id'] = $this->language_setting($lang_id);
		$lang_id 				= $this->_data['lang_id'];
		$this->_data['main_id']	= $main_id;

		$this->_data['row'] = $this-><?php echo $class_name; ?>_model->getDetail($main_id,$lang_id);

		if(!$this->_data['row']) {
			$this-><?php echo $class_name; ?>_model->addLanguage($main_id,$lang_id);
			$this->_data['row'] = $this-><?php echo $class_name; ?>_model->getDetail($main_id,$lang_id);
		}

		if(!$this->_data['row']) {
			show_error("ไม่พบข้อมูลในระบบ");
		}

		if($this->_data['row']['content_thumbnail']) {
			$this->_data['row']['content_thumbnail'] = $this-><?php echo $class_name; ?>_model->getThumbnail_img($this->_data['row']['content_thumbnail'])
																			->row_array();
		}

		// $this->admin_library->add_breadcrumb(
		//										$this->admin_library->getLanguagename($this->_data['row']['lang_id']),
		//										"<?php echo $class_name; ?>/edit/".$this->_data['row']['main_id']."/".$this->_data['row']['lang_id'],
		//										"icon-globe"
		//									);

		$this->load->library('form_validation');
		$this->form_validation->set_rules("content_subject","Title Name","trim|required|max_length[255]");
		$this->form_validation->set_rules("content_detail","Description","trim");

		// เช็คอัพโหลดไฟล์
		// $this->form_validation->set_rules("file_thumb[]","อัพโหลดไฟล์","callback_fileupload_files");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']			= "Edit ".$this->menu['menu_label'];
			$this->_data['_menu_icon']			= "glyphicon-plus-sign";
			$this->_data['_menu_title']			= $this->menu['menu_title'];
			$this->_data['_menu_link']  		= $this->menu['menu_link'];
			$this->_data['system_image'] 		= $this->image_library->checkUploadImage($this->menu['menu_link'])->result_array();

			// $this->_data['rs_file'] = $this-><?php echo $class_name; ?>_model->getDetail_file($main_id);

			$this->_data['validation_errors'] 	= validation_errors();
			$this->_data['error_message'] 		= $this->session->flashdata("error_message");
			$this->_data['success_message'] 	= $this->session->flashdata('success_message');
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("<?php echo strtolower($class_name); ?>/edit",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$this-><?php echo $class_name; ?>_model->setDate($main_id, $this->input->post("post_date"));

			$data = array(
						'main_id'				=> $main_id,
						'lang_id'				=> $lang_id,
						'content_subject' 		=> $this->input->post("content_subject"),
						'content_detail'		=> $this->input->post("content_detail"),
						'content_title'			=> ($this->input->post("content_subject")==$this->_data['row']['content_subject'] ? 
														$this->input->post("content_title") : 
															$this->input->post("content_subject")),
						'content_keyword'		=> $this->input->post("content_keyword"),
						'content_description'	=> $this->input->post("content_description"),
						'content_status'		=> $this->input->post("content_status"),
						'content_seo'			=> ($this->input->post("content_subject")==$this->_data['row']['content_subject'] ? 
														$this->input->post("content_seo") : 
															$this->input->post("content_subject")),
						'content_rewrite_id'	=> ($this->_data['row']['content_rewrite_id'] ? 
														$this->_data['row']['content_rewrite_id'] :
															md5($this->menu['menu_id'].$main_id.time()))
					);

			$checkUpdate = $this-><?php echo $class_name; ?>_model->updateContent($data);

			// ############# Images Upload ############# //
				$data_image = array(
								'menu_link'				=> $this->menu['menu_link'],
								'main_id'				=> $main_id,
								'default_main_id'		=> $this->input->post("default_main_id"),
								'attachment_id'			=> $this->input->post("attachment_id"),
								'attachment_name'		=> $this->input->post("attachment_name"),
								'attachment_name_old'	=> $this->input->post("attachment_name_old"),
								'attachment_name_main'	=> $this->input->post("attachment_name_main"),
								'attachment_detail'		=> $this->input->post("attachment_detail")
							);

				$this->image_library->updateContent($data_image);

			// ############# Files Upload ############# //
				if($this->_data['file_thumbnail']) {

					$data['file_thumb'] = $this->_data['file_thumbnail'];
					if($data['file_thumb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['file_thumb'] as $thumbnail )
						{
							$this-><?php echo $class_name; ?>_model->insertContent(
								$main_id,
								$thumbnail
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['file_thumb'] as $thumbnail )
						// {
						// 	$this-><?php echo $class_name; ?>_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

			if(!$checkUpdate) {

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$this->session->set_flashdata("success_message","Content can't update.");
					$this->logs($data, __Function__, "update", "failure");

			} else {

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$this->session->set_flashdata("success_message","Content has been update.");
					$this->logs($data, __Function__, "update", "success");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$this->rewrite_update($data, 'detail');
			}

			// ---------- Redirect Page ---------- //
				if($this->_data['language']=="single") {
					// admin_redirect($this->menu['menu_link']."/edit/".$main_id);
					admin_redirect($this->menu['menu_link']."/index");
				} else {
					// admin_redirect($this->menu['menu_link']."/edit/".$main_id."/".$lang_id);
					admin_redirect($this->menu['menu_link']."/index/".$lang_id);
				}
		}
	}

	public function delete($main_id,$lang_id=NULL)
	{
		$data 			= $this-><?php echo $class_name; ?>_model->getDetail($main_id, $lang_id);
		$checkUpdate 	= $this-><?php echo $class_name; ?>_model->deleteContent($data);

		// ---------- Re-set Sequence after Delete content ---------- //
			$queryAll 		= $this-><?php echo $class_name; ?>_model->getAllContent();
			$queryResult 	= $this-><?php echo $class_name; ?>_model->getAllContent()->result_array();

			for($i=0; $i<$queryAll->num_rows(); $i++) {

				$this-><?php echo $class_name; ?>_model->setSequence($queryResult[$i]['main_id'], ($i+1));
			}

		if(!$checkUpdate) {

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$this->session->set_flashdata("success_message","Cant' delete centent.");
				$this->logs($data, __Function__, "delete", "failure");

		} else {

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$this->session->set_flashdata("success_message","Delete centent success.");
				$this->logs($data, __Function__, "delete", "success");

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$this->rewrite_delete($data, 'detail');
		}

		// ---------- Redirect Page ---------- //
			if($this->_data['language']=="single") {
				admin_redirect($this->menu['menu_link']."/index");
			} else {
				admin_redirect($this->menu['menu_link']."/index/".$lang_id);
			}
	}

	public function handle_delete($lang_id=NULL)
	{
		$data = array(
					'main_id' 	=> $this->input->post("main_id")
				);

		foreach($data['main_id'] as $id) {

			$data 			= $this-><?php echo $class_name; ?>_model->getDetail($id, $lang_id);
			$this-><?php echo $class_name; ?>_model->deleteContent($data);

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$this->session->set_flashdata("success_message","Delete centent success.");
				$this->logs($data, __Function__, "delete", "success");

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$this->rewrite_delete($data, 'detail');
		}

		// ---------- Re-set Sequence after Delete content ---------- //
			$queryAll = $this-><?php echo $class_name; ?>_model->getAllContent();
			$queryResult = $this-><?php echo $class_name; ?>_model->getAllContent()->result_array();

			for($i=0; $i<$queryAll->num_rows(); $i++) {

				$this-><?php echo $class_name; ?>_model->setSequence($queryResult[$i]['main_id'], ($i+1));
			}

		// ---------- Redirect Page ---------- //
			if($this->_data['language']=="single") {
				admin_redirect($this->menu['menu_link']."/index");
			} else {
				admin_redirect($this->menu['menu_link']."/index/".$lang_id);
			}
	}

	public function handle_suspend($lang_id=NULL)
	{
		$data = array(
					'main_id' 	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);

		foreach($data['main_id'] as $id) {
			$this-><?php echo $class_name; ?>_model->setStatus($id,$data['lang_id'],"pending");
		}

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$this->session->set_flashdata("success_message","Update status success.");
			$this->logs($data, __Function__, "update", "success");

		// ---------- Redirect Page ---------- //
			if($this->_data['language']=="single") {
				admin_redirect($this->menu['menu_link']."/index");
			} else {
				admin_redirect($this->menu['menu_link']."/index/".$lang_id);
			}
	}

	public function handle_unsuspend($lang_id=NULL)
	{
		$data = array(
					'main_id'	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);

		foreach($data['main_id'] as $id) {
			$this-><?php echo $class_name; ?>_model->setStatus($id,$data['lang_id'],"active");
		}

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$this->session->set_flashdata("success_message","Update status success.");
			$this->logs($data, __Function__, "update", "success");

		// ---------- Redirect Page ---------- //
			if($this->_data['language']=="single") {
				admin_redirect($this->menu['menu_link']."/index");
			} else {
				admin_redirect($this->menu['menu_link']."/index/".$lang_id);
			}
	}

	public function set_default_img($default_main_id,$content_id,$attachment_id,$lang_id=NULL)
	{
		$this-><?php echo $class_name; ?>_model->setDefaultImg($content_id,$attachment_id,$lang_id);

		// ---------- Redirect Page ---------- //
			if($this->_data['language']=="single") {
				admin_redirect($this->menu['menu_link']."/edit/".$default_main_id);
			} else {
				admin_redirect($this->menu['menu_link']."/edit/".$default_main_id."/".$lang_id);
			}
	}

	// ################### Check uploads Files. ######################

	public function fileupload_files()
	{
		$this->_data['file_thumbnail'] 	= array();
		$number_of_files 				= sizeof($_FILES['file_thumb']['tmp_name']);
		$files 							= $_FILES['file_thumb'];

	    // ถ้าต้องการเช็คอัพโหลดรูป
		// if($files['tmp_name']) {
		//     for($i=0;$i<$number_of_files;$i++)
		//     {
		//       if($_FILES['file_thumb']['error'][$i] != 0)
		//       {
		//         $this->form_validation->set_message('fileupload_files', 'The Upload Files Attachment field is required.');
		//         return FALSE;
		//       }
		//     }
		// }

		// ถ้าไม่ต้องการเช็คอัพโหลดไฟล์
		if($files['tmp_name']) {

			if($files['tmp_name'][0]==NULL) {

				return TRUE;
			}
		} else {

			return TRUE;
		}

		$config['upload_path'] 		= FCPATH.'./public/uploads/<?php echo strtolower($class_name); ?>/files';
		$config['allowed_types'] 	= 'pdf|doc|docx|rar|zip|';
		$config['max_size']			= 204800;
		$config['encrypt_name'] 	= TRUE;

		for ($i = 0; $i < $number_of_files; $i++) {

			$_FILES['file_thumb']['name'] 		= $files['name'][$i];
			$_FILES['file_thumb']['type'] 		= $files['type'][$i];
			$_FILES['file_thumb']['tmp_name'] 	= $files['tmp_name'][$i];
			$_FILES['file_thumb']['error'] 		= $files['error'][$i];
			$_FILES['file_thumb']['size'] 		= $files['size'][$i];

			$this->upload->initialize($config);
			if ($this->upload->do_upload('file_thumb')) {

				$data  								= $this->upload->data();
				$this->_data['file_thumbnail'][] 	= $data['file_name'];

		      	// เช็คจำนวนอัพโหลดมากสุด
				if(count($this->_data['file_thumbnail']) > 1) {

					$this->form_validation->set_message('fileupload_files', 'The Upload Files Attachment limit 1');
					return FALSE;
				}
			} else {

				$this->form_validation->set_message('fileupload_files', $this->upload->display_errors());
				return FALSE;
			}
		}

	    return TRUE;
	}

	// ################### SEO Setting View & Update ######################

	public function seo_setting($action=NULL,$main_id=NULL,$lang_id=NULL)
	{
		$this->_data['lang_id'] = $this->language_setting($lang_id);
		$lang_id 				= $this->_data['lang_id'];
		$this->_data['main_id'] = $main_id;

		$this->_data['row'] 	= $this->language_setting_model->getDetail($main_id)->row_array();

		if(!$this->_data['row']) {
			show_error("ไม่พบข้อมูลในระบบ");
		}

		$list_language = $this->language_setting_model->action_language('list');
		if(!empty($list_language)) {
			$this->_data['row']['language'] = $list_language->result_array();
		} else {
			$this->_data['row']['language'] = null;
		}

		$data = ['label' => strtolower($this->_data['row']['seo_name'])];
		$list_translate = $this->language_setting_model->action_translate('list', $data);

		if(!empty($list_translate)) {
			$this->_data['row']['translate'] = $list_translate['translation_keys'];
		} else {
			$this->_data['row']['translate'] = null;
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules("seo_name","Menu name","trim|required|max_length[255]");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']	= "Edit ".$this->menu['menu_label'];
			$this->_data['_menu_icon']	= "glyphicon-plus-sign";
			$this->_data['_menu_title']	= $this->menu['menu_title'];
			$this->_data['_menu_link']  = $this->menu['menu_link'];

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("seosetting/edit",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			// ################### Update SEO content ################### //
				$data = [
							'id' 				=> $main_id,
							'seo_name' 			=> $this->input->post('seo_name'),
							'seo_tracking_id' 	=> $this->input->post('seo_tracking_id'),
							'seo_tracking_code' => $this->input->post('seo_tracking_code'),
							'seo_code' 			=> $this->input->post('seo_code'),
							'seo_status' 		=> $this->input->post('seo_status')
						];

				$this->language_setting_model->updateContent($data);

			// ################### Setting Key language ################### //
				if($this->input->post('label')) {

					$data_translate = [
								'label'			=> strtolower($this->input->post('seo_name'))."_".$this->input->post('label'),
								'language_id'	=> $this->input->post('language_id'),
								'value'			=> $this->input->post('value')
							];

					$this->language_setting_model->action_translate('insert', $data_translate);
				}

			// ################### Update Value language ################### //
				if(!empty($this->input->post('label_update'))) {

					foreach ($this->input->post('label_update') as $key => $value) {
						
						$data_update[$key] = [
									'label' 		=> $value,
									'value' 		=> $this->input->post('value_update')
						];
					}

					$this->language_setting_model->action_translate('update', $data_update);
				}

			// ---------- Redirect Page ---------- //
				if($this->_data['language']=="single") {
					admin_redirect($this->menu['menu_link']."/seo_setting/".$action."/".$main_id);
				} else {
					admin_redirect($this->menu['menu_link']."/seo_setting/".$action."/".$main_id.'/'.$lang_id);
				}
		}
	}

	public function delete_key($main_id=null, $key_id=null, $lang_id=NULL)
	{
		if($main_id && $key_id) {

			$data = [ 'id' => $key_id ];
			$this->language_setting_model->action_translate('delete', $data);

			$this->session->set_flashdata("success_message","Delete centent success.");

			// ---------- Redirect Page ---------- //
				if($this->_data['language']=="single") {
					admin_redirect($this->menu['menu_link']."/seo_setting/edit/".$main_id);
				} else {
					admin_redirect($this->menu['menu_link']."/seo_setting/edit/".$main_id.'/'.$lang_id);
				}
		}
	}

	// ################### Language Setting ######################

	private function language_setting($lang_id)
	{
		return $lang_id = (empty($lang_id)) ? $this->setLanguage : $lang_id;
	}

	// ################### Url Rewrite Setting ######################

	private function rewrite_update(array $data = array(), $path="detail")
	{
		$seo = array(
					'lang_id'				=> $data['lang_id'],
					'menu_link' 			=> $this->menu['menu_link'],
					'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_".$path,
					'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
					'content_subject'		=> $data['content_seo'],
					'content_rewrite_id'	=> $data['content_rewrite_id'],
					'language'				=> $this->_data['language']
				);

		$this->seo_library->rewrite_update($seo);
	}

	private function rewrite_delete(array $data = array(), $path="detail")
	{
		$seo = array(
					'lang_id'				=> $data['lang_id'],
					'menu_link' 			=> $this->menu['menu_link'],
					'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_".$path,
					'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
					'content_subject'		=> $data['content_seo'],
					'content_rewrite_id'	=> $data['content_rewrite_id']
				);

		$this->seo_library->rewrite_delete($seo);
	}

	// ################### Logs System ######################

	private function logs(array $data = array(), $function, $action, $status)
	{
		if($status=="failure") {

			switch ($action) {

	            case 'add':
	                    $message = "Content can't create.";
	                    break;
	            
	            case 'update':
	                    $message = "Content can't update.";
	                    break;

	            case 'delete':
	                    $message = "Can't delete centent.";
	                    break;
	        }
	    } else {

			switch ($action) {

	            case 'add':
	                    $message = "Content has been created.";
	                    break;
	            
	            case 'update':
	                    $message = "Content has been updated.";
	                    break;

	            case 'delete':
	                    $message = "Content has been deleted.";
	                    break;
	        }
		}

		$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
		$logs = array(
					'menu_link'		=> $this->menu['menu_link'],
					'menu_id' 		=> $this->menu['menu_id'],
					'submenu_id' 	=> $function,
					'action'		=> $action,
					'username' 		=> $user['username'],
					'session' 		=> $this->session->session_id,
					'messages' 		=> $message,
					'data'			=> serialize($data),
					'status' 		=> $status
				);

		$this->logs_library->menu_backend_log($logs);
	}
}