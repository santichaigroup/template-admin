<style type="text/css">
  


</style>
<!---------------------------- Search box ---------------------------->
  <div class="box collapsed-box">
    <div class="box-header with-border">
      <h3 class="box-title">Filtering</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool refreshColumnFilter"><i class="fa fa-refresh"></i></button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <table id="datatable_search" class="table table-striped">
        <thead>
          <tr>
            <th class="dt-body-center"><i class="glyphicon glyphicon-list-alt"></i> Title name</th>
            <th class="dt-body-center"><i class="glyphicon glyphicon-calendar"></i> Create date</th>
            <th class="dt-body-center"><i class="glyphicon glyphicon-check"></i> Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td align="center" id="filter_col2" data-column="2">
              <input type="text" class="form-control column_filter text-center" id="col2_filter">
            </td>
            <td align="center" id="filter_col3" data-column="3">
              <input type="text" class="form-control column_filter text-center dateRangePicker" id="col3_filter" readonly>
            </td>
            <td align="center" id="filter_col4" data-column="4">
              <select class="form-control column_filter text-center selectStatus" id="col4_filter">
                <option></option>
                <option value="active">แสดงข้อมูล</option>
                <option value="pending">ไม่แสดงข้อมูล</option>
              </select>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

<!---------------------------- From box ---------------------------->
  <div class="box">
    <div class="box-header with-border">
      <div class="pull-left">
        <div class="btn-group">
          <button type="button" class="btn btn-default" data-toggle="dropdown">
            <i class="icon-user"></i> Tools (<span class="select_count">0</span>) <span class="icon-caret-down"></span>
          </button>
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>

         <ul class="dropdown-menu">
            <li><a href="javascript:;" onclick="selectdata_delete();">ลบข้อมูล</a></li>
            <li><a href="javascript:;" onclick="selectdata_unsuspend();">แสดงข้อมูล</a></li>
            <li><a href="javascript:;" onclick="selectdata_suspend();">ไม่แสดงข้อมูล</a></li>
         </ul>
        </div>
      </div>

      <div class="pull-right">

        <?php if($language=="multiple") { ?>
          <div class="controls btn-group pull-right">
            <button class="btn dropdown-toggle" data-toggle="dropdown">
              <img src="images/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
                <?php echo $this->admin_library->getLanguagename($lang_id); ?> 
               <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
              <?php foreach($this->admin_library->getLanguageList() as $lang){
              if($lang_id <> $lang['lang_id']){
              ?>
                <li>
                  <a href="<?php echo admin_url($this->menu['menu_link']."/index/".$lang['lang_id']); ?>">
                    <img src="images/flags/<?php echo $lang['lang_flag']; ?>">
                    &nbsp;<?php echo $lang['lang_name']; ?>
                  </a>
                </li>
              <?php }} ?>
            </ul>
          </div>

          <a href="<?php echo admin_url($_menu_link."/add/".$lang_id); ?>" class="btn btn-success pull-right" style="width: 150px;">
            <i class="glyphicon glyphicon-plus"></i>&nbsp;  Add new <?php echo $_menu_name; ?>
          </a>
        <?php } else { ?>

          <a href="<?php echo admin_url($_menu_link."/add"); ?>" class="btn btn-success pull-right" style="width: 150px;">
            <i class="glyphicon glyphicon-plus"></i>&nbsp;  Add new <?php echo $_menu_name; ?>
          </a>
        <?php } ?>
      </div>
    </div>
    <div class="box-body">
      <!--  Error Alert  -->
      <?php if(@$success_message!=NULL){ ?>
        <div class="alert alert-success"> 
          <button class="close" data-dismiss="alert">×</button>
          <strong>Success !</strong> <?php echo $success_message; ?>
        </div>
      <?php } ?>

      <?php echo form_open('', 'name="mainForm" id="mainForm"'); ?>
        <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>">
        <table id="datatable_list" class="table table-bordered table-striped" cellspacing="0">
          <thead>
            <tr class="info">
              <th align="center" width="5"><input type="checkbox" class="select_all_item" style="margin-left:20px;" /></th>
              <th><i class="glyphicon glyphicon-list"></i> No.</th>
              <th><i class="glyphicon glyphicon-list-alt"></i> Title name</th>
              <th><i class="glyphicon glyphicon-calendar"></i> Create date</th>
              <th><i class="glyphicon glyphicon-check"></i> Status</th>
              <th class="dt-body-center"><i class="glyphicon glyphicon-wrench"></i> Actions</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      <?php echo form_close(); ?>
    </div>
  </div>

<script>
  $(function () {

    var table = $('#datatable_list').DataTable({
          "processing": true,
          "serverSide": true,
          "ajax":{
              "url": "<?php echo admin_url($_menu_link."/load_datatable/".$lang_id); ?>",
              "dataType": "json",
              "type": "POST"
          },
          // "language"  : {
          //   "sProcessing":   "กำลังดำเนินการ...",
          //   "sLengthMenu":   "แสดง _MENU_ แถว",
          //   "sZeroRecords":  "ไม่พบข้อมูล",
          //   "sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
          //   "sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
          //   "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
          //   "sInfoPostFix":  "",
          //   "sSearch":       "ค้นหาชื่อหัวข้อ: ",
          //   "sUrl":          "",
          //   "oPaginate": {
          //       "sFirst":    "เิริ่มต้น",
          //       "sPrevious": "ก่อนหน้า ",
          //       "sNext":     " ถัดไป",
          //       "sLast":     "สุดท้าย"
          //   }
          // },
          //  Set title Table.
          "columns":[
            {
              "data": "main_id"
            },
            {
              "data": "no"
            },
            {
              "data": "content_subject"
            },
            {
              "data": "post_date_format"
            },
            {
              "data": "content_status"
            },
            {
              "data": "main_id",
              "sTitle":"<i class='glyphicon glyphicon-wrench'></i> Actions"
            }
          ],
          'columnDefs': [{
               'targets': 0,
               'orderable':false,
               'className':'dt-body-center',
               'render': function (data, type, full, meta) {
                   return '<input type="checkbox" name="main_id[]" value="' 
                      + $('<div/>').text(data).html() + '">';
               }
            },
            {
              'targets': [1,2],
              'className':'dt-body-center'
            },
            {
              'targets': [3],
              'orderable':false,
              'className':'dt-body-center'
            },
            // {
            //   'targets': 3,
            //   'className':'dt-body-center',
            //   'render': function (data, type, full, meta) {

            //     return "<img data-src='holder.js/100%x180' src='<?php echo $asset_url; ?>uploads/layout/"+data+"'>";
            //  }
            // },
            {
              'targets': 4,
              'className':'dt-body-center',
              'render': function (data, type, full, meta) {

                switch(data) {
                    case "active" :
                    color = "green";
                    data = "แสดงข้อมูล";
                    break;
                    case "pending" :
                    color = "orange";
                    data = "ไม่แสดงข้อมูล";
                    break;
                    case "deleted" :
                    color = "red";
                    data = "ลบข้อมูล";
                    break;
                }

                return "<font color='"+color+"'>"+data+"</font>";
              }
            },
            {
             'targets': 5,
             'searchable':false,
             'orderable':false,
             'className':'dt-body-center',
             'width': '270px',
             'render': function (data, type, full, meta) {
              <?php if($language=="multiple") { ?>
                 return '<a href="<?php echo admin_url($_menu_link."/edit/"); ?>' 
                    + $('<div/>').text(data).html() + '/<?php echo $lang_id; ?>" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i> Edit</a>'
                    + ' <a onclick="delete_data('+ $('<div/>').text(data).html() +',\'<?php echo $lang_id; ?>\');" class="btn btn-danger btn-delete-row"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                    // + '<a type="button" class="btn btn-flat" style="background-color: transparent;"><i class="glyphicon glyphicon-move"></i></a><input type="hidden" class="seq" id="seq_'+ $('<div/>').text(data).html() 
                    // + '" name="content_id['+ $('<div/>').text(data).html() + ']" value="'+ full['no'] + '">';
              <?php } else { ?>
                 return '<a href="<?php echo admin_url($_menu_link."/edit/"); ?>' 
                    + $('<div/>').text(data).html() + '" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i> Edit</a>'
                    + ' <a onclick="delete_data('+ $('<div/>').text(data).html() +');" class="btn btn-danger btn-delete-row"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                    // + '<a type="button" class="btn btn-flat" style="background-color: transparent;"><i class="glyphicon glyphicon-move"></i></a><input type="hidden" class="seq" id="seq_'+ $('<div/>').text(data).html() 
                    // + '" name="content_id['+ $('<div/>').text(data).html() + ']" value="'+ full['no'] + '">';
              <?php } ?>
            }
          }]
       });

      // ---------- Tools Select All ---------- //
        $('.select_all_item').on('click', function() {

            var rows = table.rows({ 'search': 'applied' }).nodes();
            $('input[name="main_id[]"]', rows).prop('checked', this.checked);

            var select_length = $(rows).find('input[name="main_id[]"]:checked').length;
            $(".select_count").text(select_length);

            if(!this.checked) {
              $(rows).find(".btn-delete-row").removeClass('disabled');
              $(rows).find(".btn-warning").removeClass('disabled');
              $('.dataTables_paginate').find(".paginate_button ").removeClass('disabled');
            } else {
              $(rows).find(".btn-delete-row").addClass('disabled');
              $(rows).find(".btn-warning").addClass('disabled');
              $('.dataTables_paginate').find(".paginate_button ").addClass('disabled');
            }
        });

        $('#datatable_list_wrapper tbody').on('change', 'input[name="main_id[]"]', function() {

          var rows = table.rows({ 'search': 'applied' }).nodes();
          var select_length = $(rows).find('input[name="main_id[]"]:checked').length;
          $(".select_count").text(select_length);

          if(select_length > 0) {
            $(rows).find(".btn-delete-row").addClass('disabled');
            $(rows).find(".btn-warning").addClass('disabled');
          } else {
            $(rows).find(".btn-delete-row").removeClass('disabled');
            $(rows).find(".btn-warning").removeClass('disabled');
            $(".select_all_item").prop('indeterminate', false).prop('checked', false);
          }

          if(!this.checked){

             var el = $('.select_all_item').get(0);
             if(el && el.checked && ('indeterminate' in el)) {

                el.indeterminate = true;
             }
          }
        });

      // ---------- Tools Search box ---------- //
        $('.column_filter').on('keyup change',function() {
            filterColumn($(this).parents('td').attr('data-column'));
        });

        $('.dateRangePicker').on('apply.daterangepicker', function(ev, picker) {
          $(this).val( picker.startDate.format('YYYY-MM-DD HH:mm:ss') + ' - ' + picker.endDate.format('YYYY-MM-DD HH:mm:ss') );
          filterColumn($(this).parents('td').attr('data-column'));
        });

        $('.refreshColumnFilter').click(function() {
          $('.column_filter').val('').trigger('keyup', function() {
            filterColumn($(this).parents('td').attr('data-column'));
          });
        });

      // ---------- Tools Sequent ---------- //
        $("tbody").sortable({
          placeholder:    'sort-placeholder',
          forcePlaceholderSize: true,
          start: function( e, ui ) {

            ui.item.data( 'start-pos', ui.item.index()+1 );
          },
          change: function( e, ui ) {
            var seq,startPos = ui.item.data( 'start-pos' ),$index,correction;

            // if startPos < placeholder pos, we go from top to bottom
            // else startPos > placeholder pos, we go from bottom to top and we need to correct the index with +1
            //
            correction = startPos <= ui.placeholder.index() ? 0 : 1;

            ui.item.parent().find( 'tr').each( function( idx, el ) {

              var $this = $( el ), $index = $this.index();

              // correction 0 means moving top to bottom, correction 1 means bottom to top
              //
              if ( ( $index+1 >= startPos && correction === 0) || ($index+1 <= startPos && correction === 1 ) ) {
                $index = $index + correction;
                // console.log($index);
                $this.find('td .seq').val( $index );
              }
            });
            // handle dragged item separatelly
            seq = ui.item.parent().find( 'tr.sort-placeholder').index() + correction;
            ui.item.find( 'td .seq' ).val( seq );
          },
          update: function() {
            $.ajax({
              data: $("#mainForm").serialize(),
              type: 'POST',
              url: '<?php echo admin_url($_menu_link."/index"); ?>'
            });
            table.ajax.reload();        
          }
        });
  });

  function filterColumn(i)
  {
    if($('#col'+i+'_filter').val().length >= 2 || $('.selectStatus').val()==""){
      $('#datatable_list').DataTable().column(i).search(
          $('#col'+i+'_filter').val()
      ).draw();
    }
  }

  function delete_data(news_media_id, lang_id='')
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#mainForm").attr("action","<?php echo admin_url($_menu_link."/delete/"); ?>"+news_media_id+"/"+lang_id);
    $("#mainForm").submit();
    }
  }

  function selectdata_delete()
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#mainForm").attr("action","<?php echo admin_url($_menu_link."/handle_delete/".$lang_id); ?>");
    $("#mainForm").submit();
    }
  }

  function selectdata_suspend()
  {
    $("#mainForm").attr("action","<?php echo admin_url($_menu_link."/handle_suspend/".$lang_id); ?>");
    $("#mainForm").submit();
  }

  function selectdata_unsuspend()
  {
    $("#mainForm").attr("action","<?php echo admin_url($_menu_link."/handle_unsuspend/".$lang_id); ?>");
    $("#mainForm").submit();
  }
</script>