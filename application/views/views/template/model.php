class <?php echo ucfirst($class_name); ?>_model extends CI_Model {

	public function dataTable($lang_id="TH", $limit=null, $start=null, $is_order = array(), $is_search = array())
	{
		$this->db->select('*');
		$this->db->from('<?php echo strtolower($class_name); ?>_id', '<?php echo strtolower($class_name); ?>_content');

		$this->db->join("<?php echo strtolower($class_name); ?>_content","<?php echo strtolower($class_name); ?>_content.main_id = <?php echo strtolower($class_name); ?>_id.main_id", "left");

		// Search from Datatable
			if(!empty($is_search)) {
				foreach ($is_search as $key => $search) {

					switch ($key) {
						case 'post_date_format':
							$dateFillter = explode(' - ', $search);

							$this->db->where("<?php echo strtolower($class_name); ?>_id.post_date >=", $dateFillter[0]);
							$this->db->where("<?php echo strtolower($class_name); ?>_id.post_date <=", $dateFillter[1]);
							$this->db->where("<?php echo strtolower($class_name); ?>_content.lang_id", $lang_id);
							break;

						default:
							// Check Column name
							if($this->db->field_exists($key, '<?php echo strtolower($class_name); ?>_id')) {

								$this->db->where("<?php echo strtolower($class_name); ?>_id.".$key, $search);
								$this->db->where("<?php echo strtolower($class_name); ?>_content.lang_id", $lang_id);
							}
							// Check Column name
							if($this->db->field_exists($key, '<?php echo strtolower($class_name); ?>_content')) {

								$this->db->like("<?php echo strtolower($class_name); ?>_content.".$key, $search);
								$this->db->where("<?php echo strtolower($class_name); ?>_content.lang_id", $lang_id);
							}
							break;
					}
				}

				$this->db->where("<?php echo strtolower($class_name); ?>_id.main_status <>", "deleted");
				$this->db->where("<?php echo strtolower($class_name); ?>_content.content_status <>", "deleted");
			} else {

				$this->db->where("<?php echo strtolower($class_name); ?>_content.lang_id", $lang_id);
				$this->db->where("<?php echo strtolower($class_name); ?>_id.main_status <>", "deleted");
				$this->db->where("<?php echo strtolower($class_name); ?>_content.content_status <>", "deleted");
			}

		// Sorting from Datatable
			if(!empty($is_order)) {
				foreach ($is_order as $key => $order) {

					switch ($key) {
						case 'no':
							$this->db->order_by("<?php echo strtolower($class_name); ?>_id.main_id", $order);
							break;

						default:
							// Check Column name
								if($this->db->field_exists($key, '<?php echo strtolower($class_name); ?>_id')) {

									$this->db->order_by("<?php echo strtolower($class_name); ?>_id.".$key, $order);
								}
							// Check Column name
								if($this->db->field_exists($key, '<?php echo strtolower($class_name); ?>_content')) {

									$this->db->order_by("<?php echo strtolower($class_name); ?>_content.".$key, $order);
								}
							break;
					}
				}
			} else {
				$this->db->order_by("<?php echo strtolower($class_name); ?>_id.sequence", "ASC");
			}

		// Limit Start Filtered page
			if($limit || $start) {
				$this->db->limit($limit, $start);

				return $this->db->get();
			} else {

				return $this->db->count_all_results();
			}
	}

	public function getAllContent()
	{
		$this->db->select('main_id');
		$this->db->where('main_status <>','deleted');
		$this->db->order_by('main_id','DESC');

		return $this->db->get('<?php echo strtolower($class_name); ?>_id');
	}

	public function getDetail($<?php echo strtolower($class_name); ?>_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('<?php echo strtolower($class_name); ?>_id', '<?php echo strtolower($class_name); ?>_content');

		if($lang_id){
			$this->db->join("<?php echo strtolower($class_name); ?>_content","<?php echo strtolower($class_name); ?>_content.main_id = <?php echo strtolower($class_name); ?>_id.main_id AND <?php echo strtolower($class_name); ?>_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("<?php echo strtolower($class_name); ?>_content","<?php echo strtolower($class_name); ?>_content.main_id = <?php echo strtolower($class_name); ?>_id.main_id AND <?php echo strtolower($class_name); ?>_content.content_id = <?php echo strtolower($class_name); ?>_id.default_main_id");
		}

		$this->db->where("<?php echo strtolower($class_name); ?>_id.main_id",$<?php echo strtolower($class_name); ?>_id);
		$this->db->where("<?php echo strtolower($class_name); ?>_id.main_status <>",'deleted');

		return  $this->db->get()->row_array();
	}

	public function addData()
	{
		$this->db->select("count(*) AS countSequence");
		$this->db->where("main_status <>","deleted");
		$countRow = $this->db->get("<?php echo strtolower($class_name); ?>_id")->row_array();

		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->set("sequence", ($countRow['countSequence']+1) );
		$this->db->insert("<?php echo strtolower($class_name); ?>_id");

		$<?php echo strtolower($class_name); ?>_id = $this->db->insert_id();
		if(!$<?php echo strtolower($class_name); ?>_id) {
			show_error("Cannot create <?php echo strtolower($class_name); ?> id");
		}

		return $<?php echo strtolower($class_name); ?>_id;
	}

	public function setDate($<?php echo strtolower($class_name); ?>_id, $post_date=null)
	{
		if($post_date) {
			$this->db->set("post_date",$post_date);
		}

		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("main_id",$<?php echo strtolower($class_name); ?>_id);

		return $this->db->update("<?php echo strtolower($class_name); ?>_id");
	}

	public function setStatus($main_id,$lang_id,$status)
	{
		$this->db->set("content_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("main_id",$main_id);
		$this->db->where("lang_id",$lang_id);

		return $this->db->update("<?php echo strtolower($class_name); ?>_content");
	}

	public function setDefaultContent($<?php echo strtolower($class_name); ?>_id,$lang_id,$content_id)
	{
		$this->db->set("default_main_id",$content_id);
		$this->db->where("main_id",$<?php echo strtolower($class_name); ?>_id);

		return $this->db->update("<?php echo strtolower($class_name); ?>_id");
	}

	public function addLanguage($<?php echo strtolower($class_name); ?>_id,$lang_id)
	{
		$this->db->where("main_id",$<?php echo strtolower($class_name); ?>_id);
		$this->db->where("lang_id",$lang_id);
		$has = $this->db->get("<?php echo strtolower($class_name); ?>_content");

		if($has->num_rows() == 0) {
			$this->db->set("main_id",$<?php echo strtolower($class_name); ?>_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("<?php echo strtolower($class_name); ?>_content");

			return $this->db->insert_id();
		} else {
			$r= $has->row_array();

			return 	@$r['content_id'];
		}
	}

	public function updateContent(array $data = array())
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());

		$this->db->where('main_id',$data['main_id']);
		$this->db->where('lang_id',$data['lang_id']);

		$update_content = $this->db->update('<?php echo strtolower($class_name); ?>_content', $data);

		return $update_content;
	}

	public function deleteContent(array $data = array())
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->set("main_status","deleted");
		$this->db->set("sequence",NULL);
		$this->db->where("main_id",$data['main_id']);

		return $this->db->update("<?php echo strtolower($class_name); ?>_id");
	}

	public function getThumbnail_img($content_thumbnail)
	{
		$this->db->where('<?php echo strtolower($class_name); ?>_attachment.attachment_id', $content_thumbnail);
		$this->db->limit(1);

		return $this->db->get('<?php echo strtolower($class_name); ?>_attachment');
	}

	public function getDetail_file($main_id,$lang_id=NULL)
	{
		$type_file = array('pdf','doc','docx','rar','zip');

		$this->db->select('*');
		$this->db->from('<?php echo strtolower($class_name); ?>_id');

		$this->db->join("<?php echo strtolower($class_name); ?>_content","<?php echo strtolower($class_name); ?>_content.main_id = <?php echo strtolower($class_name); ?>_id.main_id AND <?php echo strtolower($class_name); ?>_content.content_id = <?php echo strtolower($class_name); ?>_id.default_main_id");
		$this->db->join("<?php echo strtolower($class_name); ?>_attachment","<?php echo strtolower($class_name); ?>_attachment.default_main_id = <?php echo strtolower($class_name); ?>_id.main_id ","right");

		$this->db->where("<?php echo strtolower($class_name); ?>_id.main_id",$main_id);
		$this->db->where_in("<?php echo strtolower($class_name); ?>_attachment.attachment_type", $type_file);
		$this->db->where("<?php echo strtolower($class_name); ?>_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("<?php echo strtolower($class_name); ?>_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("<?php echo strtolower($class_name); ?>_attachment.sequence","ASC");
		$this->db->limit(1000);

		return  $this->db->get();
	}

	public function setSequence($main_id,$new_sequence)
	{
		$this->db->set("sequence",$new_sequence);
		$this->db->where("main_id",$main_id);

		return $this->db->update("<?php echo strtolower($class_name); ?>_id");
	}
}