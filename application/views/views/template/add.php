<style type="text/css">
    
</style>

<div class="row">
  <?php echo form_open_multipart('', 'name="optionform" id="optionform" enctype="multipart/form-data"'); ?>
    <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>" />
    <input type="hidden" name="images" id="images" value="" />

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border toggle-click">
            <i class="glyphicon glyphicon-edit"></i>
            <h3 class="box-title">Infomation Box</h3>
        </div>
        <div class="box-body">

            <!--  Error Alert  -->
            <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
              <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <h4><i class="icon fa fa-ban"></i> Error!</h4> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
            <?php }?>
            <!--  Error Alert  -->

          <?php
          /************************* Infomation Box *************************/
          ?>
            <div class="col-md-12">
              <div class="col-md-9">
                <div class="form-group">
                  <label for="content_subject" class="control-label">Title: &nbsp;<span style="color:#F00;">*</span></label>
                  <input type="text" name="content_subject" class="form-control" id="content_subject" placeholder="" value="<?php echo set_value('content_subject'); ?>">
                </div>

                <div class="form-group">
                  <label for="content_detail" class="control-label">Description:</label>
                    <textarea class="ckeditor" name="content_detail" id="content_detail" rows="10" cols="80"><?php echo set_value('content_detail'); ?></textarea>
                </div>
              </div>

              <div class="col-md-3">

                <?php if($language=="multiple") { ?>
                  <div class="form-group">
                    <label for="menu_status" class="control-label">Language: </label><br>
                    <div class="controls btn-group">
                      <button class="btn dropdown-toggle" data-toggle="dropdown">
                        <img src="images/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
                          <?php echo $this->admin_library->getLanguagename($lang_id); ?> 
                         <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu">
                        <?php foreach($this->admin_library->getLanguageList() as $lang){
                          if($lang_id <> $lang['lang_id']){
                        ?>
                          <li>
                            <a href="<?php echo admin_url($this->menu['menu_link'].$this->submenu['menu_link']."/add/".$lang['lang_id']); ?>"><img src="images/flags/<?php echo $lang['lang_flag']; ?>">&nbsp;<?php echo $lang['lang_name']; ?></a>
                          </li>
                        <?php }} ?>
                      </ul>
                    </div>
                  </div>
                <?php } ?>

                <!-- <div class="form-group">
                    <label for="create_by" class="control-label">เขียนโดย: </label>
                    <input type="text" class="form-control" name="post_by" id="post_by" disabled>
                </div> -->

                <div class="form-group">
                  <label for="post_date" class="control-label">Create date: </label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="glyphicon glyphicon-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right active date-picker" name="post_date" id="post_date" value="<?php echo set_value("post_date", date("Y-m-d H:i:s")); ?>">
                  </div>
                </div>

                <!-- <div class="form-group">
                    <label for="upadte_by" class="control-label">แก้ไขโดย: </label>
                    <input type="text" class="form-control" name="upadte_by" id="upadte_by" disabled>
                </div>

                <div class="form-group">
                    <label for="update_date" class="control-label">แก้ไขเมื่อ: </label>
                    <input type="text" class="form-control" name="update_date" id="update_date">
                </div> -->

                <div class="form-group">
                  <label for="content_status" class="control-label">Status: </label>

                  <select name="content_status" id="content_status" class="form-control">
                    <option value="active" <?php if(set_value("content_status")=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                    <option value="pending" <?php if(set_value("content_status")=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
                  </select>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <?php if($language=="multiple") { ?>
                        <a href="<?php echo admin_url($_menu_link."/index/".$lang_id); ?>" class="btn btn-block btn-danger">
                          <i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;&nbsp;Back
                        </a>
                      <?php } else { ?>
                        <a href="<?php echo admin_url($_menu_link."/index"); ?>" class="btn btn-block btn-danger">
                          <i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;&nbsp;Back
                        </a>
                      <?php } ?>
                    </div>
                    <div class="col-md-6">
                      <button type="submit" class="btn btn-block btn-primary pull-right">
                        <i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          <?php
          /************************* Images upload Box *************************/
          ?>
            <div class="col-md-12">
              <?php if(!empty($system_image)) : foreach ($system_image as $si => $image) : ?>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="photo" class="control-label"><?php echo $image['image_title']; ?>:</label>
                    <div class="row fileupload-buttonbar">
                      <div class="col-lg-7">
                        <!-- The global button select file -->
                        <div class="col-xs-4">
                          <!-- The fileinput-button span is used to style the file input field as button -->
                          <span class="btn btn-success fileinput-button">
                              <i class="glyphicon glyphicon-plus"></i>
                              <span>Browse&hellip;</span>
                              <input type="file" name="<?php echo $image['image_name']; ?>[]" data-image_cate="<?php echo $image['image_cate']; ?>" multiple>
                          </span>
                          <button type="button" class="btn btn-danger delete hidden">
                              <i class="glyphicon glyphicon-trash"></i>
                              <span>Delete All</span>
                          </button>
                          <input type="checkbox" class="toggle hidden" checked>
                          <!-- The global file processing state -->
                          <!-- <span class="fileupload-process"></span> -->
                        </div>
                        <!-- The global progress state -->
                        <?php /* ?>
                        <div class="col-lg-8 fileupload-progress fade">
                            <!-- The global progress bar -->
                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="margin-bottom: 0px;">
                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                            </div>
                            <!-- The extended global progress state -->
                            <div class="progress-extended">&nbsp;</div>
                        </div>
                        <?php */ ?>
                        <!-- Message image detail -->
                        <div class="col-xs-12">
                          <?php
                          $image_allowed          = "";
                          $image_allowed_explode  = explode('|', $image['image_allowed']);
                          foreach ($image_allowed_explode as $iae => $explode) {
                            $image_allowed .= ".".$explode." ";
                          }
                          ?>
                          <p class="help-block">
                            *ขนาดรูป <?php echo $image['image_width']; ?>x<?php echo $image['image_height']; ?> 
                            พิกเซล(Pixel) ไม่เกิน <?php echo number_format($image['image_size'] / 1024); ?> เมกะไบต์(MB) 
                            รองรับเฉพาะไฟล์ <?php echo $image_allowed; ?> มากสุด <?php echo $image['image_limit']; ?> รูป</p>
                        </div>
                      </div>
                    </div>
                    <!-- The table listing the files available for upload/download -->
                    <div class="box">
                      <div class="box-header with-border">
                        <label class="control-label">Pictures in Database</label>
                        <div class="box-tools pull-right">
                          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                      </div>
                      <div class="box-body">
                        <div class="row <?php echo $image['image_cate']; ?> sortable" data-image_cate="<?php echo $image['image_cate']; ?>"></div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endforeach; endif; ?>
            </div>

          <?php
          /************************* Files upload Box *************************
          ?>
            <div class="col-md-12">
              <div class="col-md-12">
                  <div class="form-group">
                    <label for="content_subject" class="control-label">Upload Files Attachment: </label>
                      <div class="input-group" style="width: 40%;">
                          <label class="input-group-btn">
                              <span class="btn btn-primary">
                                  Browse&hellip; <input type="file" style="display: none;" name="file_thumb[]" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf" multiple>
                              </span>
                          </label>
                          <input type="text" class="form-control btn btn-block" readonly>
                      </div>
                      <p class="help-block">*ไฟล์ขนาดไม่เกิน 20 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .pdf, doc, docx, rar, zip จำนวน 1 ไฟล์</p>
                  </div>
                </div>
              </div>
          <?php
          */
          ?>

        </div>
      </div>
    </div>

  <?php
  /************************************************** SEO Box **************************************************/
  ?>

    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">

            <i class="glyphicon glyphicon-check"></i>
            <h3 class="box-title">SEO Box</h3>

        </div>
        <div class="box-body">

            <div class="form-group">
              <label for="content_title" class="control-label">Meta Title: </label>
              <input type="text" name="content_title" class="form-control" id="content_title" placeholder="หัวข้อเว็บไซต์" value="<?php echo set_value('content_title'); ?>">
            </div>

            <div class="form-group">
              <label for="content_description" class="control-label">Meta Description: </label>
              <textarea name="content_description" class="form-control" id="content_description" rows="3" placeholder="รายละเอียดเว็บไซต์"><?php echo set_value('content_description'); ?></textarea>
            </div>

            <div class="form-group">
              <label for="content_keyword" class="control-label">Meta Keywords: </label>
              <input type="text" name="content_keyword" class="form-control" id="content_keyword" placeholder="คำค้นหา สำหรับ SEO" value="<?php echo set_value('content_keyword'); ?>">
              <p class="help-block">ใช้ , คั่นระหว่างคำ</p>
            </div>
              
        </div>
      </div>
    </div>

  <?php echo form_close(); ?>
</div>