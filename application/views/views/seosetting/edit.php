<style type="text/css">
    
</style>

<div class="row">
  <?php echo form_open('', 'name="optionform" id="optionform"'); ?>
    <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>" />
    <input type="hidden" name="images" id="images" value="" />

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border toggle-click">
            <i class="glyphicon glyphicon-edit"></i>
            <h3 class="box-title">Infomation Box</h3>
        </div>
        <div class="box-body">

            <!--  Error Alert  -->
            <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
              <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <h4><i class="icon fa fa-ban"></i> Error!</h4> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
            <?php }?>
            <!--  Error Alert  -->

          <?php
          /************************* Infomation Box *************************/
          ?>
            <div class="col-md-12">
              <div class="col-md-9">

                <div class="form-group">
                  <label for="seo_name" class="control-label">Menu name: &nbsp;<span style="color:#F00;">*</span></label>
                  <input type="text" name="seo_name" class="form-control" id="seo_name" placeholder="" value="<?php echo set_value('seo_name', $row['seo_name']); ?>">
                </div>

                <div class="form-group">
                  <label for="seo_name" class="control-label">Setting Key</label>
                  <table class="table table-striped">
                    <tr>
                      <td align="center" width="10%">Key</td>
                      <td>
                        <div class="input-group">
                          <span class="input-group-addon"><?php echo strtolower($row['seo_name']); ?>_</span>
                          <input type="text" name="label" class="form-control" value="<?php echo set_value('label'); ?>">
                        </div>
                      </td>
                    </tr>

                    <?php
                      $display = '';
                      if($language=="single") {
                        $display = 'class="hidden"';
                      }

                      if(!empty($row['language'])) {
                        foreach ($row['language'] as $key => $value) {

                          if($key==0) {
                    ?>
                            <tr>
                              <td align="center">
                                <?php if(empty($display)) { ?>
                                  <?php echo $value['name']; ?> (<?php echo $value['language_code']; ?>)
                                <?php } ?>
                              </td>
                              <td>
                                <input type="hidden" name="language_id[]" value="<?php echo $value['id']; ?>">
                                <textarea name="value[]" class="form-control" rows="3"></textarea>
                              </td>
                            </tr>
                    <?php
                          } else {
                    ?>
                            <tr <?php echo $display; ?>>
                              <td align="center"><?php echo $value['name']; ?> (<?php echo $value['language_code']; ?>)</td>
                              <td>
                                <input type="hidden" name="language_id[]" value="<?php echo $value['id']; ?>">
                                <textarea name="value[]" class="form-control" rows="3"></textarea>
                              </td>
                            </tr>
                    <?php
                          }
                        }
                      }
                    ?>
                  </table>
                </div>

              </div>

              <div class="col-md-3">
                <!-- <div class="form-group">
                    <label for="create_by" class="control-label">เขียนโดย: </label>
                    <input type="text" class="form-control" name="post_by" id="post_by" disabled>
                </div> -->

                <div class="form-group">
                  <label for="post_date" class="control-label">Create date: </label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="glyphicon glyphicon-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right active date-picker" name="post_date" id="post_date" value="<?php echo set_value("post_date",$row['post_date']); ?>">
                  </div>
                </div>

                <?php
                if($row['update_by']) {
                ?>
                <div class="form-group">
                    <label for="upadte_by" class="control-label">Update by: </label>
                    <input type="text" class="form-control" name="upadte_by" id="upadte_by" value="<?php $update_name = $this->admin_library->getuserinfo($row['update_by']); echo $update_name['user_fullname']; ?>" disabled>
                </div>
                <?php
                }
                ?>
                <?php
                if($row['update_date']) {
                ?>
                <div class="form-group">
                    <label for="update_date" class="control-label">Update date: </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" name="update_date" id="update_date" value="<?php echo set_value("update_date",$row['update_date']); ?>" readonly="readonly">
                    </div>
                </div>
                <?php
                }
                ?>

                <div class="form-group">
                  <label for="seo_status" class="control-label">Status: </label>

                  <select name="seo_status" id="seo_status" class="form-control">
                    <option value="active" <?php if(set_value("seo_status")=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                    <option value="pending" <?php if(set_value("seo_status")=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
                  </select>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <?php if($language=="multiple") { ?>
                        <a href="<?php echo admin_url($_menu_link."/index/".$lang_id); ?>" class="btn btn-block btn-danger">
                          <i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;&nbsp;Back
                        </a>
                      <?php } else { ?>
                        <a href="<?php echo admin_url($_menu_link."/index"); ?>" class="btn btn-block btn-danger">
                          <i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;&nbsp;Back
                        </a>
                      <?php } ?>
                    </div>
                    <div class="col-md-6">
                      <button type="submit" class="btn btn-block btn-primary pull-right">
                        <i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

        </div>
      </div>
    </div>

  <?php
  /************************************************** Key & Value **************************************************/

    if(!empty($row['translate'])) {
  ?>

    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">

            <i class="glyphicon glyphicon-check"></i>
            <h3 class="box-title">Setting Value</h3>

        </div>
        <div class="box-body">

          <?php foreach ($row['translate'] as $k => $v) { ?>
            <div class="col-md-6">
              <div class="form-group">
                <table class="table table-striped">
                  <tr>
                    <td align="center" width="15%">Key</td>
                    <td>
                      <input type="text" name="label_update[<?php echo $v['id']; ?>]" class="form-control" value="<?php echo $v['label']; ?>">
                    </td>
                  </tr>

                  <?php
                    if(!empty($v['values'])) {
                      foreach ($v['values'] as $key => $value) {

                        if($key==0) {
                  ?>
                          <tr>
                            <td align="center">
                              <?php if(empty($display)) { ?>
                                <?php echo $value['name']; ?> (<?php echo $value['language_code']; ?>)
                              <?php } ?>
                            </td>
                            <td>
                              <textarea name="value_update[<?php echo $value['id']; ?>]" class="form-control" rows="3"><?php echo $value['value']; ?></textarea>
                            </td>
                          </tr>
                  <?php
                        } else {
                  ?>
                          <tr <?php echo $display; ?>>
                            <td align="center"><?php echo $value['name']; ?> (<?php echo $value['language_code']; ?>)</td>
                            <td>
                              <textarea name="value_update[<?php echo $value['id']; ?>]" class="form-control" rows="3"><?php echo $value['value']; ?></textarea>
                            </td>
                          </tr>
                  <?php
                        }
                      }
                    }
                  ?>

                  <tr>
                    <td colspan="2" align="right">
                      <a onclick="delete_data(<?php echo $main_id; ?>,<?php echo $v['id']; ?>);" class="btn btn-block btn-danger" style="width: 100px;">
                        <i class="glyphicon glyphicon-trash"></i> Delete
                      </a>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          <?php } ?>

        </div>
      </div>
    </div>
  <?php
    }
  ?>

  <?php
  /************************************************** SEO Box **************************************************/
  ?>

    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">

            <i class="glyphicon glyphicon-check"></i>
            <h3 class="box-title">SEO Box</h3>

        </div>
        <div class="box-body">
          <div class="col-md-12">
              <div class="form-group">
                <label for="seo_tracking_id" class="control-label">Tracking ID: </label>
                <input type="text" name="seo_tracking_id" class="form-control" id="seo_tracking_id" placeholder="Google Analytics Tracking ID. Ex. UA-12345678-1" value="<?php echo set_value('seo_tracking_id', $row['seo_tracking_id']); ?>">
              </div>

              <div class="form-group">
                <label for="seo_tracking_code" class="control-label">Tracking Code: </label>
                <textarea name="seo_tracking_code" class="form-control" id="seo_tracking_code" rows="3" placeholder=""><?php echo set_value('seo_tracking_code', $row['seo_tracking_code']); ?></textarea>
              </div>

              <div class="form-group">
                <label for="seo_code" class="control-label">Additional &lt;head&gt; code: </label>
                <textarea name="seo_code" class="form-control" id="seo_code" rows="3" placeholder=""><?php echo set_value('seo_code', $row['seo_code']); ?></textarea>
              </div>
          </div>
        </div>
      </div>
    </div>

  <?php echo form_close(); ?>
</div>

<script>
  function delete_data(main_id, key_id)
  {
    if(confirm("Delete Data !. Are you sure ?")){
      window.location.href = "<?php echo admin_url($_menu_link."/delete_key/"); ?>"+main_id+"/"+key_id;
    }
  }
</script>