<style type="text/css">
    
</style>

<div class="row">
  <?php echo form_open_multipart('', 'name="user_listform" id="user_listform"'); ?>
    <input type="hidden" name="main_date" id="main_date" value="<?php echo set_value("main_date",date("d-m-Y")); ?>" />
    <input type="hidden" name="user_status" id="user_status" value="<?php echo set_value("user_status", $rs['user_status']); ?>" />
    <input type="hidden" name="user_id" value="<?php echo @$rs['user_id']; ?>" />

  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="glyphicon glyphicon-edit"></i>
        <h3 class="box-title">Information Box</h3>
      </div>
      <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                <button class="close" data-dismiss="alert">×</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
            </div>
          <?php }?>
          <!--  Error Alert  -->

        <div class="col-md-12">
          <div class="col-md-9">
          
            <div class="form-group">
              <label for="user_fullname" class="control-label">Full Name: &nbsp;<span style="color:#F00;">*</span></label>
              <input type="text" name="user_fullname" class="form-control" id="user_fullname" placeholder="กรุณากรอกชื่อ-นามสกุล" value="<?php echo set_value('user_fullname', $rs['user_fullname']); ?>">
            </div>

            <div class="form-group">
              <label for="user_email" class="control-label">Email: &nbsp;<span style="color:#F00;">*</span></label>
              <div class="input-group">
                  <div class="input-group-addon">
                      <i class="glyphicon glyphicon-envelope"></i>
                  </div>
                  <div class="form-control"><?php echo $rs['user_email']; ?></div>
                  <input type="hidden" name="user_email" class="form-control" id="user_email" value="<?php echo set_value('user_email',$rs['user_email']); ?>">
              </div>
            </div>

            <div class="form-group">
              <label for="user_mobileno" class="control-label">Mobile Phone: </label>
              <div class="input-group">
                  <div class="input-group-addon">
                      <i class="glyphicon glyphicon-phone"></i>
                  </div>
                  <input type="number" name="user_mobileno" class="form-control" id="user_mobileno" placeholder="กรุณากรอกเบอร์โทรศัพท์" value="<?php echo set_value('user_mobileno', $rs['user_mobileno']); ?>">
              </div>
            </div>

            <div class="form-group">
                 <label class="control-label" for="user_group">User Group: </label>
                 <div class="controls">
                    <select name="user_group" class="form-control" disabled="disabled">
                       <option value="">กรุณาเลือก User Group</option>
                       <?php foreach($this->admin_library->getAllGroup()->result_array() as $row){ ?>
                       <option value="<?php echo $row['group_id']; ?>" <?php if(set_value("user_group",@$rs['user_group'])==$row['group_id']){ ?> selected="selected" <?php } ?>><?php echo $row['group_name']; ?></option>
                       <?php } ?>
                    </select>
                 </div>
            </div>

            <div style="clear:both"></div>

            <div class="form-group col-md-6">
              <label for="username" class="control-label">Username: </label>
              <div class="form-control"><?php echo $rs['username']; ?></div>
              <input type="hidden" name="username" class="form-control" id="username" value="<?php echo set_value('username',$rs['username']); ?>">
            </div>

            <div class="form-group col-md-6">
              <label class="control-label"></label>
              <div class="checkbox">

              </div>
            </div>

            <div style="clear:both"></div>

            <div class="form-group col-md-6">
              <label for="password" class="control-label">Password: <span style="color: #F00;">* หากต้องการตั้งรหัสผ่านใหม่</span></label>
              <input type="password" name="password" class="form-control" id="password" placeholder="Password 8 - 12 ตัวอักษร" value="<?php echo set_value('password'); ?>">
            </div>

            <div class="form-group col-md-2">
              <label class="control-label">&nbsp;</label>
              <a class="btn btn-primary form-control" onclick="generatePassword();"><i class="fa fa-refresh"></i>&nbsp;&nbsp;&nbsp;<b>สุ่มรหัสผ่าน</b></a>
            </div>

            <div class="form-group col-md-4">
              <label class="control-label">&nbsp;</label>
              <input type="text" name="random_code" class="form-control" id="random_code" placeholder="" value="<?php echo set_value('random_code'); ?>">
            </div>

            <div style="clear:both"></div>

            <div class="form-group col-md-6">
              <label for="confirm_password" class="control-label">Confirm Password: </label>
              <input type="password" name="confirm_password" class="form-control" id="confirm_password" placeholder="" value="<?php echo set_value('confirm_password'); ?>">
            </div>

            <div style="clear:both"></div>

            <div class="form-group">
              <label for="image_thumb" class="control-label">อัพโหลดรูปประจำตัว: </label>
                <input type="file" name="image_thumb[]" id="image_thumb" accept="image/*"/>
                <p class="help-block">ขนาดรูป 640x360 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif จำนวน 1 รูป</p>
            </div>

            <div style="clear:both"></div>

            <div class="row" id="sortable">
              <?php if($rs['user_thumbnail']) { ?>
              <div class="col-md-3 ui-state-default">
                  <a class="fancybox-buttons thumbnail" data-fancybox-group="button" href="<?php echo base_url()."public/uploads/system_users/images/".$rs['user_thumbnail']; ?>">
                    <img class="img-responsive" data-src="holder.js/100%x180" data-holder-rendered="true" src="<?php echo base_url()."public/uploads/system_users/images/".$rs['user_thumbnail']; ?>" alt="" />
                  </a>
              </div>
              <?php } ?>
            </div>

            <div style="clear:both"></div>
          </div>

          <?php
          /************************************************** Tools Box **************************************************/
          ?>

          <div class="col-md-3">
            <div class="form-group">
              <label for="post_date" class="control-label">Join Date: </label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="glyphicon glyphicon-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right active date-picker" name="post_date" id="post_date" value="<?php echo set_value("post_date",date("d-m-Y")); ?>"  readonly="readonly">
              </div>
            </div>

            <?php
            if($user_info['group_superadmin']=="yes") :
            ?>
              <div class="form-group">
                <label for="user_status_select" class="control-label">User Status: </label>
                <select name="user_status_select" id="user_status_select" class="form-control">
                  <option value="active" <?php if(set_value("user_status")=="active"){ ?>selected="selected" <?php } ?>>อนุมัติใช้งาน</option>
                  <option value="pending" <?php if(set_value("user_status")=="pending"){ ?>selected="selected" <?php } ?>>ไม่อนุมัติใช้งาน</option>
                </select>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <a href="<?php echo admin_url($_menu_link."/userlist"); ?>" class="btn btn-block btn-danger">
                      <i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;&nbsp;Back
                    </a>
                  </div>
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-block btn-primary pull-right">
                      <i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save
                    </button>
                  </div>
                </div>
              </div>
            <?php
            else :
            ?>
              <div class="form-group">
                <label for="user_status_select" class="control-label">User Status: </label>
                <select name="user_status_select" id="user_status_select" class="form-control" disabled>
                  <option value="active" <?php if(set_value("user_status")=="active"){ ?>selected="selected" <?php } ?>>อนุมัติใช้งาน</option>
                  <option value="pending" <?php if(set_value("user_status")=="pending"){ ?>selected="selected" <?php } ?>>ไม่อนุมัติใช้งาน</option>
                </select>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6 pull-right">
                    <button type="submit" class="btn btn-block btn-primary pull-right">
                      <i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save
                    </button>
                  </div>
                </div>
              </div>
            <?php
            endif;
            ?>

          </div>
        </div>

      </div>
    </div>
  </div>

  <?php echo form_close(); ?>
</div>

<script src="plugins/validate/jquery.validate.min.js"></script>

<script type="text/javascript">

  $(function () {

    $("#user_listform").validate({
        rules: {
          content_subject: "required",
          password: {
            required: false,
            minlength: 5
          },
          confirm_password: {
            required: false,
            minlength: 5,
            equalTo: "#password"
          }
        },

        messages: {
          content_subject: "กรุณากรอกชื่อ-นามสกุล",
          password: {
            required: "กรุณากรอก Password",
            minlength: "กรุณากรอก Password อย่างน้อย 5 ตัวอักษร"
          },
          confirm_password: {
            required: "กรุณากรอก Confirm Password",
            minlength: "กรุณากรอก Confirm Password อย่างน้อย 5 ตัวอักษร",
            equalTo: "กรุณากรอก Confirm Password ให้ตรงกับ Password"
          }
        }
      });

  });

  function generatePassword() {

    var length = 8,
        charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }

    $("#random_code").val(retVal);
    $("#password").val(retVal);
    $("#confirm_password").val(retVal);
  }

  function save_form()
  {
    $("#user_status").val($("#user_status_select").val());
    $("#main_date").val($("#post_date").val());
    $("form#user_listform").submit();
  }
</script>