<div class="row">
  <div class="col-md-9">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Form Box</h3>

      </div>
      <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
          <?php }?>
          <!--  Error Alert  -->

          <form name="optionform" id="optionform" method="post" enctype="multipart/form-data" role="form">
              <input type="hidden" name="content_id" id="content_id" value="<?php echo $row['content_id']; ?>" />
            
              <div class="form-group">
                <label for="content_subject" class="control-label">E-mail Admin: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_email" class="form-control" id="content_email" placeholder="" value="<?php echo set_value("content_email", $row['content_email']); ?>">
              </div>

              <div class="form-group">
                <label for="content_subject" class="control-label">E-mail Admin CC : (email_1@xxx.ocm , email_2@xxx.com)&nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_cc" class="form-control" id="content_cc" placeholder="" value="<?php echo set_value("content_cc", $row['content_cc']); ?>">
              </div>

<!--               <div class="form-group">
                <label for="content_subject" class="control-label">E-mail Account Paysbuy : &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_mail_paysbuy" class="form-control" id="content_mail_paysbuy" placeholder="" value="<?php echo set_value("content_mail_paysbuy", $row['content_mail_paysbuy']); ?>">
              </div>

              <div class="form-group">
                <label for="content_subject" class="control-label">หมายเลยประจาตัวของร้านค้า Paysbuy (psbID) : &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_psbID" class="form-control" id="content_psbID" placeholder="" value="<?php echo set_value("content_psbID", $row['content_psbID']); ?>">
              </div>

              <div class="form-group">
                <label for="content_subject" class="control-label">Pin Paysbuy : &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_pin" class="form-control" id="content_pin" placeholder="" value="<?php echo set_value("content_pin", $row['content_pin']); ?>">
              </div>

              <div class="form-group">
                <label for="content_subject" class="control-label">Secure Code Paysbuy : &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_secureCode" class="form-control" id="content_secureCode" placeholder="" value="<?php echo set_value("content_secureCode", $row['content_secureCode']); ?>">
              </div> -->

          </form>

      </div>
    </div>

  </div>

<?php
/************************************************** Tools Box **************************************************/
?>

  <div class="col-md-3">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Tools Box</h3>

      </div>
      <div class="box-body">

          <div class="form-group">
              <label for="content_status_select" class="control-label">การแสดงผล: </label>

              <select name="content_status_select" id="content_status_select" class="form-control">
                <option value="active" <?php if(set_value("content_status", $row['content_status'])=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                <option value="pending" <?php if(set_value("content_status", $row['content_status'])=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
              </select>

          </div>

      </div>
      <div class="box-footer">
          <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
      </div>
    </div>

  </div>

</div>

<script type="text/javascript">
  function save_form()
  {
    $("#main_status").val($("#main_status_select").val());
    $("#main_date").val($("#main_date_edit").val());
    $("#content_keyword").val($("#content_keyword_edit").val());
    $("form#optionform").submit();  
  }
</script>