<div class="row">
  <?php echo form_open('', 'name="optionform" id="optionform"'); ?>
    <div class="col-md-12">

      <div class="box box-primary">
        <div class="box-header with-border toggle-click">

            <i class="glyphicon glyphicon-edit"></i>
            <h3 class="box-title">Form Box</h3>

        </div>
        <div class="box-body">

            <!--  Error Alert  -->
            <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
              <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <h4><i class="icon fa fa-ban"></i> Error!</h4> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
            <?php }?>
            <!--  Error Alert  -->

            <div class="row">
              <div class="col-md-9">
                <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $row['lang_id']; ?>" />
                <input type="hidden" name="email_id" id="email_id" value="<?php echo $row['email_id']; ?>" />
              
                <div class="form-group">
                    <label for="content_all_menu" class="control-label">ประเภทอีเมล: &nbsp;<span style="color:#F00;">*</span></label>

                    <?php
                    if(empty($row['content_detail'])) {

                      echo '<input type="text" name="content_detail" class="form-control" id="content_detail" placeholder="" value="'.set_value('content_detail', $row['content_detail']).'">';

                    } else {
                    ?>
                        <select name="content_all_menu" id="content_all_menu" class="form-control" onchange="if (this.value) window.location.href=this.value">
                          <?php
                          foreach ($row_all->result_array() as $value) {

                            if($row['email_id']==$value['email_id']) {
                              $selected = "selected";
                            } else {
                              $selected = "";
                            }
                            
                            echo "<option value='".admin_url('contact/edit_mail/'.$value['email_id']."/".$lang_id)."' ".$selected.">".$value['content_detail']."</option>";
                          }
                          ?>
                        </select>
                    <?php

                      echo '<input type="hidden" name="content_detail" class="form-control" id="content_detail" placeholder="" value="'.set_value('content_detail', $row['content_detail']).'">';
                    }
                    ?>
                </div>

                <div class="form-group">
                  <label for="content_subject" class="control-label">ชื่อผู้ส่งอีเมล: &nbsp;<span style="color:#F00;">*</span></label>
                  <input type="text" name="content_subject" class="form-control" id="content_subject" placeholder="" value="<?php echo set_value('content_subject', $row['content_subject']); ?>">
                </div>

                <div class="form-group">
                  <label for="content_email" class="control-label">อีเมล: &nbsp;<span style="color:#F00;">*</span></label>
                  <input type="text" name="content_email" class="form-control" id="content_email" placeholder="" value="<?php echo set_value('content_email', $row['content_email']); ?>">
                </div>

                <div class="form-group">
                  <label for="content_email_cc" class="control-label">สำเนาอีเมล (E-mail CC : exemple_mail_1@gmail.com , excemple_mail_2@gmail.com):</label>
                  <input type="text" name="content_email_cc" class="form-control" id="content_email_cc" placeholder="" value="<?php echo set_value('content_email_cc', $row['content_email_cc']); ?>">
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                    <label for="menu_status" class="control-label">เปลี่ยนภาษา: </label><br>
                    <div class="controls btn-group">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">
                            <img src="images/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
                              <?php echo $this->admin_library->getLanguagename($lang_id); ?> 
                             <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          <?php foreach($this->admin_library->getLanguageList() as $lang){
                          if($row['lang_id'] <> $lang['lang_id']){
                          ?>
                            <li>
                              <a href="<?php echo admin_url("contact/edit_mail/".$row['email_id']."/".$lang['lang_id']); ?>">
                                <img src="images/flags/<?php echo $lang['lang_flag']; ?>">&nbsp;<?php echo $lang['lang_name']; ?>
                              </a>
                            </li>
                          <?php }} ?>
                        </ul>
                    </div>
                </div>

                <div class="form-group">
                    <label for="content_status" class="control-label">การแสดงผล: </label>

                    <select name="content_status" id="content_status" class="form-control">
                      <option value="active" <?php if(set_value("content_status")=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                      <option value="pending" <?php if(set_value("content_status")=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
                    </select>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <a href="<?php echo admin_url('contact/add_email/'.$lang_id); ?>" class="btn btn-block btn-success">
                        <i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;เพิ่มประเภท Email
                      </a>
                    </div>
                    <div class="col-md-6">
                      <button type="submit" class="btn btn-block btn-primary pull-right">
                        <i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

        </div>
      </div>

    </div>
  <?php echo form_close(); ?>
</div>

<script type="text/javascript">
  function save_form()
  {
    $("form#optionform").submit();  
  }
</script>