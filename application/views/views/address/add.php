<div class="row">
  <div class="col-md-9">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Form Box</h3>

      </div>
      <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                <button class="close" data-dismiss="alert">×</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
            </div>
          <?php }?>
          <!--  Error Alert  -->

          <?php echo form_open_multipart('', 'name="address_optionform" id="address_optionform"'); ?>
              <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>" />
            
              <div class="form-group">
                <label for="content_subject" class="control-label">ชื่อสถานที่: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_subject" class="form-control" id="content_subject" placeholder="" value="<?php echo set_value("content_subject"); ?>">
              </div>

              <div class="form-group">
                <label for="content_group" class="control-label">สำนักงาน: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_group" class="form-control" id="content_group" placeholder="" value="<?php echo set_value("content_group"); ?>">
              </div>

              <div class="form-group">
                <label for="content_address" class="control-label">ที่อยู่: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_address" class="form-control" id="content_address" placeholder="" value="<?php echo set_value("content_address"); ?>">
              </div>

              <div class="form-group">
                <label for="content_phone" class="control-label">เบอร์โทรศัพท์: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_phone" class="form-control" id="content_phone" placeholder="" value="<?php echo set_value("content_phone"); ?>">
              </div>

              <div class="form-group">
                <label for="content_fax" class="control-label">เบอร์โทรสาร: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_fax" class="form-control" id="content_fax" placeholder="" value="<?php echo set_value("content_fax"); ?>">
              </div>

              <div class="form-group">
                <label for="content_email" class="control-label">อีเมล์: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_email" class="form-control" id="content_email" placeholder="" value="<?php echo set_value("content_email"); ?>">
              </div>

              <div class="form-group">
                <label for="content_facebook" class="control-label">Facebook:</label>
                <input type="text" name="content_facebook" class="form-control" id="content_facebook" placeholder="" value="<?php echo set_value("content_facebook"); ?>">
              </div>

              <div class="form-group">
                <label for="content_instagram" class="control-label">Instagram:</label>
                <input type="text" name="content_instagram" class="form-control" id="content_instagram" placeholder="" value="<?php echo set_value("content_instagram"); ?>">
              </div>

              <div class="form-group">
                <label for="content_google" class="control-label">Google+:</label>
                <input type="text" name="content_google" class="form-control" id="content_google" placeholder="" value="<?php echo set_value("content_google"); ?>">
              </div>

              <div class="form-group">
                <label for="content_youtube" class="control-label">Youtube:</label>
                <input type="text" name="content_youtube" class="form-control" id="content_youtube" placeholder="" value="<?php echo set_value("content_youtube"); ?>">
              </div>

              <div class="form-group">
                <label for="content_twitter" class="control-label">Twitter:</label>
                <input type="text" name="content_twitter" class="form-control" id="content_twitter" placeholder="" value="<?php echo set_value("content_twitter"); ?>">
              </div>

              <div class="form-group">
                <label for="content_tumblr" class="control-label">Tumblr:</label>
                <input type="text" name="content_tumblr" class="form-control" id="content_tumblr" placeholder="" value="<?php echo set_value("content_tumblr"); ?>">
              </div>

              <div class="form-group">
                <label for="content_pinterest" class="control-label">Pinterest:</label>
                <input type="text" name="content_pinterest" class="form-control" id="content_pinterest" placeholder="" value="<?php echo set_value("content_pinterest"); ?>">
              </div>

              <div class="form-group">
                <label for="content_line" class="control-label">Line:</label>
                <input type="text" name="content_line" class="form-control" id="content_line" placeholder="" value="<?php echo set_value("content_line"); ?>">
              </div>

              <div class="form-group">
                <label for="content_googlemap" class="control-label">ลิงก์แผนที่ Google Map:</label>
                <input type="text" name="content_googlemap" class="form-control" id="content_googlemap" placeholder="" value="<?php echo set_value("content_googlemap"); ?>">
              </div>

              <div class="form-group">
                <label for="content_detail" class="control-label">ข้อมูลแสดง: </label>
                  <textarea class="ckeditor" name="content_detail" id="content_detail" rows="10" cols="80"><?php echo set_value("content_detail"); ?></textarea>
              </div>

              <div class="form-group">
                <div class="control-group">
                     <label class="control-label" >Image Map : ขนาดรูปไม่เกินเกิน 5 M</label>
                     <div class="controls">
                        <input type="file" name="image_thumb" id="image_thumb" accept="image/*" />
                     </div>
                </div>
                <br><br>
                <div class="control-group">
                     <label class="control-label" >QR Code : ขนาดรูปไม่เกินเกิน 5 M</label>
                     <div class="controls">
                        <input type="file" name="content_qr_code" id="content_qr_code" accept="image/*" />
                     </div>
                </div>
              </div>

          <?php echo form_close(); ?>

      </div>
    </div>

  </div>

<?php
/************************************************** Tools Box **************************************************/
?>

  <div class="col-md-3">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Tools Box</h3>

      </div>
      <div class="box-body">

          <div class="form-group">
              <label for="menu_status" class="control-label">เปลี่ยนภาษา: </label><br>
              <div class="controls btn-group">
                      <button class="btn dropdown-toggle" data-toggle="dropdown">
                      <img src="images/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
            <?php echo $this->admin_library->getLanguagename($lang_id); ?> 
                       <span class="caret"></span>
                      </button>
                <ul class="dropdown-menu">
                  <?php foreach($this->admin_library->getLanguageList() as $lang){
                  if($lang_id <> $lang['lang_id']){
                  ?>
                    <li>
                      <a href="<?php echo admin_url($this->menu['menu_link']."/add/".$lang['lang_id']); ?>">
                        <img src="images/flags/<?php echo $lang['lang_flag']; ?>">
                        &nbsp;<?php echo $lang['lang_name']; ?>
                      </a>
                    </li>
                  <?php }} ?>
                </ul>
              </div>
          </div>

          <div class="form-group">
              <label for="content_status_select" class="control-label">การแสดงผล: </label>

              <select name="content_status_select" id="content_status_select" class="form-control">
                <option value="active">แสดงข้อมูล</option>
                <option value="pending">ไม่แสดงข้อมูล</option>
              </select>

          </div>

      </div>
      <div class="box-footer">
          <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
      </div>
    </div>

  </div>

</div>

<script type="text/javascript">
    function save_form()
    {
        $("#content_status").val($("#content_status_select").val());
        $("#address_date").val($("#address_date_edit").val());
        $("#content_keyword").val($("#content_keyword_edit").val());
        $("form#address_optionform").submit();  
    }
</script>