<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title_reset_password; ?></title>
  <meta name="description" content="">

  <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="favicon/favicon-194x194.png" sizes="194x194">
  <link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="favicon/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="favicon/manifest.json">
  <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
  <meta name="msapplication-config" content="favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <style>
  <?php
  if($fullname!="xxxxxxxx") {
  ?>
   body {
    padding: 0;
    margin: 0;
   }
  <?php
  }
  ?>
  </style>
</head>

<body>

<table style="width: 640px; margin-left: auto; margin-right: auto; border-collapse: collapse; font-size: 16px; color: #686868; font-family: tahoma;">
  <thead>
    <tr>
      <th><img src="<?php echo base_url($image_logo); ?>" width="640" alt="Q Fresh"></th>
    </tr>
    <tr>
      <th>
        <div style="color: #1f72a3; font-size: 28px; text-align: center; padding-bottom: 15px; font-weight: normal;">RESET PASSWORD</div>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="padding-top: 30px; padding-bottom: 30px; font-size: 12px; color: #535353; text-align: center;">
        <p><?php echo $messages_forget_password; ?></p>
        <p>
          <?php echo $messages_username; ?><br><?php echo $messages_password; ?>
        </p>
        <p>
          <?php echo $messages_click_reset; ?>
        </p>
        <a style="color:#e17400;" target="_blank" href="<?php echo $link_confirm; ?>"><i><?php echo $link_confirm; ?></i></a>

        <!-- <p>
          ถ้าคุณไม่ได้ทำคำขอนี้เป็นไปได้ว่าผู้ใช้รายอื่นได้ป้อนที่อยู่อีเมลของคุณโดยไม่ได้ตั้งใจและบัญชีของคุณยังปลอดภัยอยู่
        </p> -->
        <!-- <p>
          ถ้าคุณเชื่อว่าบุคคลที่ไม่ได้รับอนุญาตเข้าถึงบัญชีของคุณคุณสามารถรีเซ็ตรหัสผ่านของคุณที่ <a href="" style="font-weight: bold; color: #0f6ba6">qfresh.com</a>
        </p> -->
      </td>
    </tr>
    <tr>
      <td style="padding-bottom: 30px; border-top: 1px solid #a6a6a6; font-size: 12px; text-align: center; color: #535353; font-weight: bold;">
        <p><?php echo $thank_you; ?></p>
        <p><?php echo $company; ?></p>
      </td>
    </tr>
    <tr>
      <td style="background-color: #173d71; color: #ffffff; font-size: 11px; text-align: center; padding: 15px;">
        Copyright 2016 . Thai Union Frozen Products PCL. by Thai Union Group. All rights reserved.
      </td>
    </tr>
  </tbody>
</table>

</body>

</html>