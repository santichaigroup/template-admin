<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title_contact; ?></title>
  <meta name="description" content="">

  <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="favicon/favicon-194x194.png" sizes="194x194">
  <link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="favicon/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="favicon/manifest.json">
  <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
  <meta name="msapplication-config" content="favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <style>
  <?php
  if($fullname!="xxxxxxxx") {
  ?>
   body {
    padding: 0;
    margin: 0;
   }
   <?php
  }
   ?>
  </style>
</head>

<body>

<table style="width: 640px; margin-left: auto; margin-right: auto; border-collapse: collapse; font-size: 16px; color: #686868; font-family: tahoma;">
  <thead>
    <tr>
      <th><img src="<?php echo base_url($image_logo); ?>" width="640" alt="Q Fresh"></th>
    </tr>
    <tr>
      <th>
        <div style="color: #1f72a3; font-size: 28px; text-align: center; padding-bottom: 15px; font-weight: normal;">CONTACT US</div>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="padding-top: 30px; padding-bottom: 30px; font-size: 12px; color: #535353; text-align: left;">
        <p><b style="font-size:14px;"><?php echo $hello_admin; ?></b></p>
        <p><?php echo $date_time; ?> <br> มีผู้ติดต่อสอบถามผ่านทางแบบฟอร์มติดต่อเรา โดยมีรายละเอียดดังนี้</p>
        <table style="width: 100%; table-layout: collapse;">
          <tr>
            <td style="width: 120px; vertical-align: top;"><b>หัวข้อ :</b></td>
            <td><?php echo $topic; ?></td>
          </tr>
          <tr>
            <td style="width: 120px; vertical-align: top;"><b>ชื่อ-นามสกุล :</b></td>
            <td><?php echo $fullname; ?></td>
          </tr>
          <tr>
            <td style="width: 120px; vertical-align: top;"><b>อีเมล :</b></td>
            <td><?php echo $email; ?></td>
          </tr>
          <tr>
            <td style="width: 120px; vertical-align: top;"><b>รายละเอียด :</b></td>
            <td>
              <div>
                <?php echo $detail; ?>
              </div><br>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="padding-bottom: 30px; border-top: 1px solid #a6a6a6; font-size: 12px; text-align: center; color: #535353; font-weight: bold;">
        <p><?php echo $thank_you; ?></p>
        <p><?php echo $company; ?></p>
      </td>
    </tr>
    <tr>
      <td style="background-color: #173d71; color: #ffffff; font-size: 11px; text-align: center; padding: 15px;">
        Copyright 2016 . Thai Union Frozen Products PCL. by Thai Union Group. All rights reserved.
      </td>
    </tr>    
  </tbody>
</table>

</body>

</html>