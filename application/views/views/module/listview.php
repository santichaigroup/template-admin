<div class="row">
  <div class="col-md-12">

    <div class="box">
      <div class="box-header with-border">
        <div class="pull-right">
          <a href="<?php echo admin_url($_menu_link."/add_menu/th"); ?>" class="btn btn-success pull-right" style="width: 200px;"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;&nbsp;Add new Main Menu</a>
        </div>
      </div>
      <div class="box-body">

        <!--  Error Alert  -->
        <h4></h4>
        <?php if(@$success_message!=NULL){ ?>
        <div class="alert alert-success"> 
          <button class="close" data-dismiss="alert">×</button>
          <strong>Success !</strong> <?php echo $success_message; ?>
        </div>
        <?php } ?>

        <?php echo form_open('', 'name="usergroup_listform" id="usergroup_listform"'); ?>
          <table id="datatable_list" class="table table-bordered table-striped" cellspacing="0">
            <thead>
              <tr class="info">
                <th class="dt-body-center"><i class="glyphicon glyphicon-list-alt"></i> Menu Label</th>
                <th class="dt-body-center"><i class="glyphicon glyphicon-calendar"></i> Menu Link</th>
                <th class="dt-body-center"><i class="glyphicon glyphicon-calendar"></i> Menu Title</th>
                <th class="dt-body-center"><i class="glyphicon glyphicon-check"></i> Menu Sequent</th>
                <th class="dt-body-center"><i class="glyphicon glyphicon-wrench"></i> Actions</th>
              </tr>
            </thead>

            <tbody>
          <?php
          foreach($result AS $menu) {

            $menu_id      = $menu['id'];
            $menu_label   = $menu['label'];
            $menu_link    = $menu['link'];
            $menu_seo     = $menu['seo'];
            $menu_title   = $menu['title'];
            $menu_sequent = $menu['sequent'];
          ?>
              <tr>
                <td class="td-body-middle"><i class="fa fa-circle"></i>&nbsp;&nbsp;&nbsp;<?php echo $menu_label; ?></td>
                <td class="dt-body-center td-body-middle"><?php echo $menu_seo; ?></td>
                <td class="dt-body-center td-body-middle"><?php echo $menu_title; ?></td>
                <td class="dt-body-center"><b><?php echo $menu_sequent; ?></b></td>
                <td class="dt-body-center">
                  <a href="<?php echo admin_url($_menu_link."/add_sub_menu/".$menu_id); ?>" class="btn btn-sm btn-primary">
                    <i class="glyphicon glyphicon-plus"></i>&nbsp;Add Sub Menu
                  </a>
                  <a href="<?php echo admin_url($_menu_link."/edit_menu/".$menu_id); ?>" class="btn btn-sm btn-warning">
                    <i class="glyphicon glyphicon-edit"></i>&nbsp;Edit
                  </a>
                  <a href="javascript:;" onclick="delete_menu(<?php echo $menu_id; ?>);" class="btn btn-sm btn-danger">
                    <i class="glyphicon glyphicon-trash"></i>&nbsp;Delete
                  </a>
                </td>
              </tr>
              <?php
              if(count($menu['submenu_entry'])!=0) {

                  foreach($menu['submenu_entry'] AS $submenu) {

                    $submenu_id       = $submenu['id'];
                    $submenu_label    = $submenu['label'];
                    $submenu_link     = $submenu['link'];
                    $submenu_seo      = $submenu['seo'];
                    $submenu_title    = $submenu['title'];
                    $submenu_sequent  = $submenu['sequent'];
              ?>
                    <tr class="">
                      <td class="td-body-middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle-o"></i>&nbsp;&nbsp;&nbsp;<?php echo $submenu_label; ?></td>
                      <td class="dt-body-center td-body-middle"><?php echo ( $submenu_seo ? $menu_link."/".$submenu_seo : "" ); ?></td>
                      <td class="dt-body-center td-body-middle"><?php echo $submenu_title; ?></td>
                      <td class="dt-body-center"><?php echo $submenu_sequent; ?></td>
                      <td class="dt-body-center">
                        <!-- <a href="<?php echo admin_url($_menu_link."/edit_sub_menu/".$menu_id."/".$submenu_id); ?>" class="btn btn-sm btn-warning">
                          <i class="glyphicon glyphicon-edit"></i>&nbsp;Edit
                        </a> -->
                        <a href="javascript:;" onclick="delete_sub_menu(<?php echo $submenu_id; ?>);" class="btn btn-sm btn-danger">
                          <i class="glyphicon glyphicon-trash"></i>&nbsp;Delete
                        </a>
                      </td>
                    </tr>
              <?php
                  }
              }
          }
          ?>
            </tbody>

          </table>
        <?php echo form_close(); ?>

      </div>
    </div>

  </div>
</div>
<script>

  $(function () {

    var table = $('#datatable_list').DataTable({

          "pageLength": -1,
          "language"  : {
            "sProcessing":   "กำลังดำเนินการ...",
            "sLengthMenu":   "แสดง _MENU_ แถว",
            "sZeroRecords":  "ไม่พบข้อมูล",
            "sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
            "sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
            "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
            "sInfoPostFix":  "",
            "sSearch":       "ค้นหา: ",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "เิริ่มต้น",
                "sPrevious": "ก่อนหน้า ",
                "sNext":     " ถัดไป",
                "sLast":     "สุดท้าย"
            }
          },
          "bSort" : false
       });
  });

  function delete_menu(menu_id)
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#usergroup_listform").attr("action","<?php echo admin_url($_menu_link."/delete/"); ?>"+menu_id);
    $("#usergroup_listform").submit();
    }
  }

  function delete_sub_menu(submenu_id)
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#usergroup_listform").attr("action","<?php echo admin_url($_menu_link."/delete_submenu/"); ?>"+submenu_id);
    $("#usergroup_listform").submit();
    }
  }

</script>