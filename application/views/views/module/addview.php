<div class="row">
  <?php echo form_open('', 'name="optionform" id="optionform"'); ?>

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border toggle-click">
            <i class="glyphicon glyphicon-edit"></i>
            <h3 class="box-title">Menu Box</h3>
        </div>
        <div class="box-body form-horizontal">

            <!--  Error Alert  -->
            <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
              <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <h4><i class="icon fa fa-ban"></i> Error!</h4> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
            <?php }?>
            <!--  Error Alert  -->
            
            <div class="col-md-12">
              <div class="col-md-9">
                <div class="form-group">
                  <label for="menu_label" class="col-sm-3 control-label">Menu Label&nbsp;<span style="color:#F00;">*</span></label>
                  <div class="col-sm-5">
                      <input type="text" name="menu_label" class="form-control" id="menu_label" placeholder="* เฉพาะภาษาอังกฤษ" value="<?php echo set_value('menu_label'); ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="menu_title" class="col-sm-3 control-label">Menu Detail</label>
                  <div class="col-sm-4">
                      <input type="text" name="menu_title" class="form-control" id="menu_title" placeholder="* ชื่อเมนูภาษาไทย" value="<?php echo set_value('menu_title'); ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="menu_label" class="col-sm-3 control-label">Menu Icon</label>
                  <div class="col-sm-4">
                      <input type="text" name="menu_icon" class="form-control" id="menu_icon" placeholder="glyphicon-ok" value="<?php echo set_value('menu_icon'); ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="menu_link" class="col-sm-3 control-label">Menu Link&nbsp;<span style="color:#F00;">*</span></label>
                  <div class="col-sm-5">
                      <input type="text" name="menu_link" class="form-control" id="menu_link" placeholder="* เฉพาะภาษาอังกฤษตัวพิมเล็ก" value="<?php echo set_value('menu_link'); ?>">
                  </div>
                </div>

                <!-- <div class="form-group">
                  <label for="menu_seo" class="col-sm-3 control-label">Menu SEO Url</label>
                  <div class="col-sm-5">
                      <input type="text" name="menu_seo" class="form-control" id="menu_seo" placeholder="* เฉพาะภาษาอังกฤษตัวพิมเล็ก" value="<?php echo set_value(''); ?>">
                  </div>
                </div> -->

                <div class="form-group">
                  <label for="menu_sequent" class="col-sm-3 control-label">Menu Sequent</label>
                  <div class="col-sm-3">
                      <input type="text" name="menu_sequent" class="form-control" id="menu_sequent" placeholder="* เฉพาะตัวเลข" value="<?php echo set_value('menu_sequent'); ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="menu_sequent" class="col-sm-3 control-label">Language</label>
                  <div class="col-sm-3">
                      <div class="radio">
                        <label>
                          <input type="radio" name="menu_language" id="single" value="single">
                          Single
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="menu_language" id="multiple" value="multiple" checked="checked">
                          Multiple
                        </label>
                      </div>
                  </div>
                </div>
              </div>

              <div class="col-md-3">

                <!-- <div class="form-group">
                    <label for="create_by" class="col-sm-4 control-label">เขียนโดย</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="create_by" disabled>
                    </div>
                </div>

                <div class="form-group">
                    <label for="create_date" class="col-sm-4 control-label">เขียนเมื่อ</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="create_date" disabled>
                    </div>
                </div>

                <div class="form-group">
                    <label for="upadte_by" class="col-sm-4 control-label">แก้ไขโดย</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="upadte_by" disabled>
                    </div>
                </div>

                <div class="form-group">
                    <label for="update_date" class="col-sm-4 control-label">แก้ไขเมื่อ</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="update_date">
                    </div>
                </div> -->

                <div class="form-group">
                    <label for="menu_status" class="control-label">Status:</label>
                    <select name="menu_status" id="menu_status" class="form-control">
                      <option value="active">แสดงข้อมูล</option>
                      <option value="pending">ไม่แสดงข้อมูล</option>
                    </select>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <a href="<?php echo admin_url($_menu_link."/list_menu"); ?>" class="btn btn-block btn-danger">
                        <i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;&nbsp;Back
                      </a>
                    </div>
                    <div class="col-md-6">
                      <button type="submit" class="btn btn-block btn-primary pull-right" onclick="save_form();">
                        <i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

        </div>
      </div>
    </div>

<?php
/************************************************** Icon Box **************************************************/
?>

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-header with-border">
          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Icon Box</h3>
      </div>
      <div class="box-body form-horizontal">

            <ul class="bs-glyphicons">
              <li>
                <span class="glyphicon glyphicon-asterisk"></span>
                <span class="glyphicon-class">glyphicon-asterisk</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">glyphicon-plus</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-euro"></span>
                <span class="glyphicon-class">glyphicon-euro</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-eur"></span>
                <span class="glyphicon-class">glyphicon-eur</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-minus"></span>
                <span class="glyphicon-class">glyphicon-minus</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-cloud"></span>
                <span class="glyphicon-class">glyphicon-cloud</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-envelope"></span>
                <span class="glyphicon-class">glyphicon-envelope</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-pencil"></span>
                <span class="glyphicon-class">glyphicon-pencil</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-glass"></span>
                <span class="glyphicon-class">glyphicon-glass</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-music"></span>
                <span class="glyphicon-class">glyphicon-music</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-search"></span>
                <span class="glyphicon-class">glyphicon-search</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-heart"></span>
                <span class="glyphicon-class">glyphicon-heart</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon-class">glyphicon-star</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-star-empty"></span>
                <span class="glyphicon-class">glyphicon-star-empty</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-user"></span>
                <span class="glyphicon-class">glyphicon-user</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-film"></span>
                <span class="glyphicon-class">glyphicon-film</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-th-large"></span>
                <span class="glyphicon-class">glyphicon-th-large</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-th"></span>
                <span class="glyphicon-class">glyphicon-th</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-th-list"></span>
                <span class="glyphicon-class">glyphicon-th-list</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-ok"></span>
                <span class="glyphicon-class">glyphicon-ok</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-remove"></span>
                <span class="glyphicon-class">glyphicon-remove</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-zoom-in"></span>
                <span class="glyphicon-class">glyphicon-zoom-in</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-zoom-out"></span>
                <span class="glyphicon-class">glyphicon-zoom-out</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-off"></span>
                <span class="glyphicon-class">glyphicon-off</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-signal"></span>
                <span class="glyphicon-class">glyphicon-signal</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-cog"></span>
                <span class="glyphicon-class">glyphicon-cog</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-trash"></span>
                <span class="glyphicon-class">glyphicon-trash</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-home"></span>
                <span class="glyphicon-class">glyphicon-home</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-file"></span>
                <span class="glyphicon-class">glyphicon-file</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-time"></span>
                <span class="glyphicon-class">glyphicon-time</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-road"></span>
                <span class="glyphicon-class">glyphicon-road</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-download-alt"></span>
                <span class="glyphicon-class">glyphicon-download-alt</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-download"></span>
                <span class="glyphicon-class">glyphicon-download</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-upload"></span>
                <span class="glyphicon-class">glyphicon-upload</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-inbox"></span>
                <span class="glyphicon-class">glyphicon-inbox</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-play-circle"></span>
                <span class="glyphicon-class">glyphicon-play-circle</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-repeat"></span>
                <span class="glyphicon-class">glyphicon-repeat</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-refresh"></span>
                <span class="glyphicon-class">glyphicon-refresh</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-list-alt"></span>
                <span class="glyphicon-class">glyphicon-list-alt</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-lock"></span>
                <span class="glyphicon-class">glyphicon-lock</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-flag"></span>
                <span class="glyphicon-class">glyphicon-flag</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-headphones"></span>
                <span class="glyphicon-class">glyphicon-headphones</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-volume-off"></span>
                <span class="glyphicon-class">glyphicon-volume-off</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-volume-down"></span>
                <span class="glyphicon-class">glyphicon-volume-down</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-volume-up"></span>
                <span class="glyphicon-class">glyphicon-volume-up</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-qrcode"></span>
                <span class="glyphicon-class">glyphicon-qrcode</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-barcode"></span>
                <span class="glyphicon-class">glyphicon-barcode</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-tag"></span>
                <span class="glyphicon-class">glyphicon-tag</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-tags"></span>
                <span class="glyphicon-class">glyphicon-tags</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-book"></span>
                <span class="glyphicon-class">glyphicon-book</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-bookmark"></span>
                <span class="glyphicon-class">glyphicon-bookmark</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-print"></span>
                <span class="glyphicon-class">glyphicon-print</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-camera"></span>
                <span class="glyphicon-class">glyphicon-camera</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-font"></span>
                <span class="glyphicon-class">glyphicon-font</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-bold"></span>
                <span class="glyphicon-class">glyphicon-bold</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-italic"></span>
                <span class="glyphicon-class">glyphicon-italic</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-text-height"></span>
                <span class="glyphicon-class">glyphicon-text-height</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-text-width"></span>
                <span class="glyphicon-class">glyphicon-text-width</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-align-left"></span>
                <span class="glyphicon-class">glyphicon-align-left</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-align-center"></span>
                <span class="glyphicon-class">glyphicon-align-center</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-align-right"></span>
                <span class="glyphicon-class">glyphicon-align-right</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-align-justify"></span>
                <span class="glyphicon-class">glyphicon-align-justify</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-list"></span>
                <span class="glyphicon-class">glyphicon-list</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-indent-left"></span>
                <span class="glyphicon-class">glyphicon-indent-left</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-indent-right"></span>
                <span class="glyphicon-class">glyphicon-indent-right</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-facetime-video"></span>
                <span class="glyphicon-class">glyphicon-facetime-video</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-picture"></span>
                <span class="glyphicon-class">glyphicon-picture</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-map-marker"></span>
                <span class="glyphicon-class">glyphicon-map-marker</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-adjust"></span>
                <span class="glyphicon-class">glyphicon-adjust</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-tint"></span>
                <span class="glyphicon-class">glyphicon-tint</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-edit"></span>
                <span class="glyphicon-class">glyphicon-edit</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-share"></span>
                <span class="glyphicon-class">glyphicon-share</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-check"></span>
                <span class="glyphicon-class">glyphicon-check</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-move"></span>
                <span class="glyphicon-class">glyphicon-move</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-step-backward"></span>
                <span class="glyphicon-class">glyphicon-step-backward</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-fast-backward"></span>
                <span class="glyphicon-class">glyphicon-fast-backward</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-backward"></span>
                <span class="glyphicon-class">glyphicon-backward</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-play"></span>
                <span class="glyphicon-class">glyphicon-play</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-pause"></span>
                <span class="glyphicon-class">glyphicon-pause</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-stop"></span>
                <span class="glyphicon-class">glyphicon-stop</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-forward"></span>
                <span class="glyphicon-class">glyphicon-forward</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-fast-forward"></span>
                <span class="glyphicon-class">glyphicon-fast-forward</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-step-forward"></span>
                <span class="glyphicon-class">glyphicon-step-forward</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-eject"></span>
                <span class="glyphicon-class">glyphicon-eject</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="glyphicon-class">glyphicon-chevron-left</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="glyphicon-class">glyphicon-chevron-right</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-plus-sign"></span>
                <span class="glyphicon-class">glyphicon-plus-sign</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-minus-sign"></span>
                <span class="glyphicon-class">glyphicon-minus-sign</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-remove-sign"></span>
                <span class="glyphicon-class">glyphicon-remove-sign</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-ok-sign"></span>
                <span class="glyphicon-class">glyphicon-ok-sign</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-question-sign"></span>
                <span class="glyphicon-class">glyphicon-question-sign</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-info-sign"></span>
                <span class="glyphicon-class">glyphicon-info-sign</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-screenshot"></span>
                <span class="glyphicon-class">glyphicon-screenshot</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-remove-circle"></span>
                <span class="glyphicon-class">glyphicon-remove-circle</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-ok-circle"></span>
                <span class="glyphicon-class">glyphicon-ok-circle</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-ban-circle"></span>
                <span class="glyphicon-class">glyphicon-ban-circle</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-arrow-left"></span>
                <span class="glyphicon-class">glyphicon-arrow-left</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-arrow-right"></span>
                <span class="glyphicon-class">glyphicon-arrow-right</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-arrow-up"></span>
                <span class="glyphicon-class">glyphicon-arrow-up</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-arrow-down"></span>
                <span class="glyphicon-class">glyphicon-arrow-down</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-share-alt"></span>
                <span class="glyphicon-class">glyphicon-share-alt</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-resize-full"></span>
                <span class="glyphicon-class">glyphicon-resize-full</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-resize-small"></span>
                <span class="glyphicon-class">glyphicon-resize-small</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-exclamation-sign"></span>
                <span class="glyphicon-class">glyphicon-exclamation-sign</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-gift"></span>
                <span class="glyphicon-class">glyphicon-gift</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-leaf"></span>
                <span class="glyphicon-class">glyphicon-leaf</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-fire"></span>
                <span class="glyphicon-class">glyphicon-fire</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-eye-open"></span>
                <span class="glyphicon-class">glyphicon-eye-open</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-eye-close"></span>
                <span class="glyphicon-class">glyphicon-eye-close</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-warning-sign"></span>
                <span class="glyphicon-class">glyphicon-warning-sign</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-plane"></span>
                <span class="glyphicon-class">glyphicon-plane</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-calendar"></span>
                <span class="glyphicon-class">glyphicon-calendar</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-random"></span>
                <span class="glyphicon-class">glyphicon-random</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-comment"></span>
                <span class="glyphicon-class">glyphicon-comment</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-magnet"></span>
                <span class="glyphicon-class">glyphicon-magnet</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-chevron-up"></span>
                <span class="glyphicon-class">glyphicon-chevron-up</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-chevron-down"></span>
                <span class="glyphicon-class">glyphicon-chevron-down</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-retweet"></span>
                <span class="glyphicon-class">glyphicon-retweet</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-shopping-cart"></span>
                <span class="glyphicon-class">glyphicon-shopping-cart</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-folder-close"></span>
                <span class="glyphicon-class">glyphicon-folder-close</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-folder-open"></span>
                <span class="glyphicon-class">glyphicon-folder-open</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-resize-vertical"></span>
                <span class="glyphicon-class">glyphicon-resize-vertical</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-resize-horizontal"></span>
                <span class="glyphicon-class">glyphicon-resize-horizontal</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-hdd"></span>
                <span class="glyphicon-class">glyphicon-hdd</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-bullhorn"></span>
                <span class="glyphicon-class">glyphicon-bullhorn</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-bell"></span>
                <span class="glyphicon-class">glyphicon-bell</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-certificate"></span>
                <span class="glyphicon-class">glyphicon-certificate</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-thumbs-up"></span>
                <span class="glyphicon-class">glyphicon-thumbs-up</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-thumbs-down"></span>
                <span class="glyphicon-class">glyphicon-thumbs-down</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-hand-right"></span>
                <span class="glyphicon-class">glyphicon-hand-right</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-hand-left"></span>
                <span class="glyphicon-class">glyphicon-hand-left</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-hand-up"></span>
                <span class="glyphicon-class">glyphicon-hand-up</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-hand-down"></span>
                <span class="glyphicon-class">glyphicon-hand-down</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-circle-arrow-right"></span>
                <span class="glyphicon-class">glyphicon-circle-arrow-right</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-circle-arrow-left"></span>
                <span class="glyphicon-class">glyphicon-circle-arrow-left</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-circle-arrow-up"></span>
                <span class="glyphicon-class">glyphicon-circle-arrow-up</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-circle-arrow-down"></span>
                <span class="glyphicon-class">glyphicon-circle-arrow-down</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-globe"></span>
                <span class="glyphicon-class">glyphicon-globe</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-wrench"></span>
                <span class="glyphicon-class">glyphicon-wrench</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-tasks"></span>
                <span class="glyphicon-class">glyphicon-tasks</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-filter"></span>
                <span class="glyphicon-class">glyphicon-filter</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-briefcase"></span>
                <span class="glyphicon-class">glyphicon-briefcase</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-fullscreen"></span>
                <span class="glyphicon-class">glyphicon-fullscreen</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-dashboard"></span>
                <span class="glyphicon-class">glyphicon-dashboard</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-paperclip"></span>
                <span class="glyphicon-class">glyphicon-paperclip</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-heart-empty"></span>
                <span class="glyphicon-class">glyphicon-heart-empty</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-link"></span>
                <span class="glyphicon-class">glyphicon-link</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-phone"></span>
                <span class="glyphicon-class">glyphicon-phone</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-pushpin"></span>
                <span class="glyphicon-class">glyphicon-pushpin</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-usd"></span>
                <span class="glyphicon-class">glyphicon-usd</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-gbp"></span>
                <span class="glyphicon-class">glyphicon-gbp</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sort"></span>
                <span class="glyphicon-class">glyphicon-sort</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sort-by-alphabet"></span>
                <span class="glyphicon-class">glyphicon-sort-by-alphabet</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
                <span class="glyphicon-class">glyphicon-sort-by-alphabet-alt</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sort-by-order"></span>
                <span class="glyphicon-class">glyphicon-sort-by-order</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sort-by-order-alt"></span>
                <span class="glyphicon-class">glyphicon-sort-by-order-alt</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sort-by-attributes"></span>
                <span class="glyphicon-class">glyphicon-sort-by-attributes</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                <span class="glyphicon-class">glyphicon-sort-by-attributes-alt</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-unchecked"></span>
                <span class="glyphicon-class">glyphicon-unchecked</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-expand"></span>
                <span class="glyphicon-class">glyphicon-expand</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-collapse-down"></span>
                <span class="glyphicon-class">glyphicon-collapse-down</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-collapse-up"></span>
                <span class="glyphicon-class">glyphicon-collapse-up</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-log-in"></span>
                <span class="glyphicon-class">glyphicon-log-in</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-flash"></span>
                <span class="glyphicon-class">glyphicon-flash</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-log-out"></span>
                <span class="glyphicon-class">glyphicon-log-out</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-new-window"></span>
                <span class="glyphicon-class">glyphicon-new-window</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-record"></span>
                <span class="glyphicon-class">glyphicon-record</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-save"></span>
                <span class="glyphicon-class">glyphicon-save</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-open"></span>
                <span class="glyphicon-class">glyphicon-open</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-saved"></span>
                <span class="glyphicon-class">glyphicon-saved</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-import"></span>
                <span class="glyphicon-class">glyphicon-import</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-export"></span>
                <span class="glyphicon-class">glyphicon-export</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-send"></span>
                <span class="glyphicon-class">glyphicon-send</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-floppy-disk"></span>
                <span class="glyphicon-class">glyphicon-floppy-disk</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-floppy-saved"></span>
                <span class="glyphicon-class">glyphicon-floppy-saved</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-floppy-remove"></span>
                <span class="glyphicon-class">glyphicon-floppy-remove</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-floppy-save"></span>
                <span class="glyphicon-class">glyphicon-floppy-save</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-floppy-open"></span>
                <span class="glyphicon-class">glyphicon-floppy-open</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-credit-card"></span>
                <span class="glyphicon-class">glyphicon-credit-card</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-transfer"></span>
                <span class="glyphicon-class">glyphicon-transfer</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-cutlery"></span>
                <span class="glyphicon-class">glyphicon-cutlery</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-header"></span>
                <span class="glyphicon-class">glyphicon-header</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-compressed"></span>
                <span class="glyphicon-class">glyphicon-compressed</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-earphone"></span>
                <span class="glyphicon-class">glyphicon-earphone</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-phone-alt"></span>
                <span class="glyphicon-class">glyphicon-phone-alt</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-tower"></span>
                <span class="glyphicon-class">glyphicon-tower</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-stats"></span>
                <span class="glyphicon-class">glyphicon-stats</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sd-video"></span>
                <span class="glyphicon-class">glyphicon-sd-video</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-hd-video"></span>
                <span class="glyphicon-class">glyphicon-hd-video</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-subtitles"></span>
                <span class="glyphicon-class">glyphicon-subtitles</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sound-stereo"></span>
                <span class="glyphicon-class">glyphicon-sound-stereo</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sound-dolby"></span>
                <span class="glyphicon-class">glyphicon-sound-dolby</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sound-5-1"></span>
                <span class="glyphicon-class">glyphicon-sound-5-1</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sound-6-1"></span>
                <span class="glyphicon-class">glyphicon-sound-6-1</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sound-7-1"></span>
                <span class="glyphicon-class">glyphicon-sound-7-1</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-copyright-mark"></span>
                <span class="glyphicon-class">glyphicon-copyright-mark</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-registration-mark"></span>
                <span class="glyphicon-class">glyphicon-registration-mark</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-cloud-download"></span>
                <span class="glyphicon-class">glyphicon-cloud-download</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-cloud-upload"></span>
                <span class="glyphicon-class">glyphicon-cloud-upload</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-tree-conifer"></span>
                <span class="glyphicon-class">glyphicon-tree-conifer</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-tree-deciduous"></span>
                <span class="glyphicon-class">glyphicon-tree-deciduous</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-cd"></span>
                <span class="glyphicon-class">glyphicon-cd</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-save-file"></span>
                <span class="glyphicon-class">glyphicon-save-file</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-open-file"></span>
                <span class="glyphicon-class">glyphicon-open-file</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-level-up"></span>
                <span class="glyphicon-class">glyphicon-level-up</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-copy"></span>
                <span class="glyphicon-class">glyphicon-copy</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-paste"></span>
                <span class="glyphicon-class">glyphicon-paste</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-alert"></span>
                <span class="glyphicon-class">glyphicon-alert</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-equalizer"></span>
                <span class="glyphicon-class">glyphicon-equalizer</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-king"></span>
                <span class="glyphicon-class">glyphicon-king</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-queen"></span>
                <span class="glyphicon-class">glyphicon-queen</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-pawn"></span>
                <span class="glyphicon-class">glyphicon-pawn</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-bishop"></span>
                <span class="glyphicon-class">glyphicon-bishop</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-knight"></span>
                <span class="glyphicon-class">glyphicon-knight</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-baby-formula"></span>
                <span class="glyphicon-class">glyphicon-baby-formula</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-tent"></span>
                <span class="glyphicon-class">glyphicon-tent</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-blackboard"></span>
                <span class="glyphicon-class">glyphicon-blackboard</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-bed"></span>
                <span class="glyphicon-class">glyphicon-bed</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-apple"></span>
                <span class="glyphicon-class">glyphicon-apple</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-erase"></span>
                <span class="glyphicon-class">glyphicon-erase</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-hourglass"></span>
                <span class="glyphicon-class">glyphicon-hourglass</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-lamp"></span>
                <span class="glyphicon-class">glyphicon-lamp</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-duplicate"></span>
                <span class="glyphicon-class">glyphicon-duplicate</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-piggy-bank"></span>
                <span class="glyphicon-class">glyphicon-piggy-bank</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-scissors"></span>
                <span class="glyphicon-class">glyphicon-scissors</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-bitcoin"></span>
                <span class="glyphicon-class">glyphicon-bitcoin</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-btc"></span>
                <span class="glyphicon-class">glyphicon-btc</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-xbt"></span>
                <span class="glyphicon-class">glyphicon-xbt</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-yen"></span>
                <span class="glyphicon-class">glyphicon-yen</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-jpy"></span>
                <span class="glyphicon-class">glyphicon-jpy</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-ruble"></span>
                <span class="glyphicon-class">glyphicon-ruble</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-rub"></span>
                <span class="glyphicon-class">glyphicon-rub</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-scale"></span>
                <span class="glyphicon-class">glyphicon-scale</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-ice-lolly"></span>
                <span class="glyphicon-class">glyphicon-ice-lolly</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-ice-lolly-tasted"></span>
                <span class="glyphicon-class">glyphicon-ice-lolly-tasted</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-education"></span>
                <span class="glyphicon-class">glyphicon-education</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-option-horizontal"></span>
                <span class="glyphicon-class">glyphicon-option-horizontal</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-option-vertical"></span>
                <span class="glyphicon-class">glyphicon-option-vertical</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-menu-hamburger"></span>
                <span class="glyphicon-class">glyphicon-menu-hamburger</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-modal-window"></span>
                <span class="glyphicon-class">glyphicon-modal-window</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-oil"></span>
                <span class="glyphicon-class">glyphicon-oil</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-grain"></span>
                <span class="glyphicon-class">glyphicon-grain</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-sunglasses"></span>
                <span class="glyphicon-class">glyphicon-sunglasses</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-text-size"></span>
                <span class="glyphicon-class">glyphicon-text-size</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-text-color"></span>
                <span class="glyphicon-class">glyphicon-text-color</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-text-background"></span>
                <span class="glyphicon-class">glyphicon-text-background</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-object-align-top"></span>
                <span class="glyphicon-class">glyphicon-object-align-top</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-object-align-bottom"></span>
                <span class="glyphicon-class">glyphicon-object-align-bottom</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-object-align-horizontal"></span>
                <span class="glyphicon-class">glyphicon-object-align-horizontal</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-object-align-left"></span>
                <span class="glyphicon-class">glyphicon-object-align-left</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-object-align-vertical"></span>
                <span class="glyphicon-class">glyphicon-object-align-vertical</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-object-align-right"></span>
                <span class="glyphicon-class">glyphicon-object-align-right</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-triangle-right"></span>
                <span class="glyphicon-class">glyphicon-triangle-right</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-triangle-left"></span>
                <span class="glyphicon-class">glyphicon-triangle-left</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-triangle-bottom"></span>
                <span class="glyphicon-class">glyphicon-triangle-bottom</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-triangle-top"></span>
                <span class="glyphicon-class">glyphicon-triangle-top</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-console"></span>
                <span class="glyphicon-class">glyphicon-console</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-superscript"></span>
                <span class="glyphicon-class">glyphicon-superscript</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-subscript"></span>
                <span class="glyphicon-class">glyphicon-subscript</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-menu-left"></span>
                <span class="glyphicon-class">glyphicon-menu-left</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-menu-right"></span>
                <span class="glyphicon-class">glyphicon-menu-right</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-menu-down"></span>
                <span class="glyphicon-class">glyphicon-menu-down</span>
              </li>
              <li>
                <span class="glyphicon glyphicon-menu-up"></span>
                <span class="glyphicon-class">glyphicon-menu-up</span>
              </li>
            </ul>

      </div>
    </div>
  </div>

  <?php echo form_close(); ?>
</div>

<script type="text/javascript">
  function save_form()
  {
    // $("#main_status").val($("#main_status_select").val());
    $("form#optionform").submit();  
  }
</script