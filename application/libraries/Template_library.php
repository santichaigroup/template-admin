<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Template_library {

	private $ci;
	private $_data = array();

	public function __construct()
	{
		@ob_start();
		$this->ci = & get_instance();
		$this->ci->load->library('parser');
		$this->ci->load->library('admin_library');
		$this->ci->load->library('migration');
		$this->ci->load->library('template_migration');
		$this->ci->load->helper('file');
	}

	public function create_menu($data)
	{
		$this->_data['class_name']	= ucfirst(strtolower($data['menu_link']));
		$this->_data['language']	= $data['menu_language'];

		$get_template_controller  	= $this->ci->admin_library->view('template/controller', $this->_data, TRUE);
		$get_template_model  		= $this->ci->admin_library->view('template/model', $this->_data, TRUE);
		$get_template_listview		= read_file('./application/views/views/template/listview.php');
		$get_template_add			= read_file('./application/views/views/template/add.php');
		$get_template_edit			= read_file('./application/views/views/template/edit.php');

		// Create File menu controller.
			$controller_path = './application/controllers/'.$this->_data['class_name'].'.php';
			write_file($controller_path, "<?php \n".$get_template_controller." \n?>", 'w+');
			chmod($controller_path, 0777);

		// Create Database tables.
			$this->ci->template_migration->create_table_id(strtolower($this->_data['class_name']));
			$this->ci->template_migration->create_table_content(strtolower($this->_data['class_name']));
			$this->ci->template_migration->create_table_attachment(strtolower($this->_data['class_name']));
			$this->ci->template_migration->create_table_logs(strtolower($this->_data['class_name']));

		// Create File menu model.
			$model_path = './application/models/'.$this->_data['class_name'].'_model.php';
			write_file($model_path, "<?php \n".$get_template_model." \n?>", 'w+');
			chmod($model_path, 0777);

		// Create File view add, edit, listview.
			$view_path = './application/views/views/'.strtolower($this->_data['class_name']);
			mkdir($view_path, 0777, true);
			write_file('./application/views/views/'.strtolower($this->_data['class_name']).'/listview.php', $get_template_listview, 'w+');
			write_file('./application/views/views/'.strtolower($this->_data['class_name']).'/add.php', $get_template_add, 'w+');
			write_file('./application/views/views/'.strtolower($this->_data['class_name']).'/edit.php', $get_template_edit, 'w+');
			chmod($view_path, 0777);
			chmod($view_path.'/listview.php', 0777);
			chmod($view_path.'/add.php', 0777);
			chmod($view_path.'/edit.php', 0777);

		// Create upload files path.
			$images_path = './public/uploads/'.strtolower($this->_data['class_name']).'/images';
			mkdir($images_path, 0777, true);
			chmod($images_path, 0777);
			$files_path = './public/uploads/'.strtolower($this->_data['class_name']).'/files';
			mkdir($files_path, 0777, true);
			chmod($files_path, 0777);

		return TRUE;
	}

	public function create_sub_menu($data)
	{
		// delete_files('./path/to/directory/', TRUE);
	}

}
?>