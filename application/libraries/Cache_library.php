<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Cache_library
{
    var $ci;
    var $path;
    
    public function __construct()
    {
        $this->ci =& get_instance();
        
        $this->path = APPPATH.'cache/';
        if ($this->path == '') return false;
    }
    
    function get($filename, $use_expires = true)
    {
        $cache_path = $this->path;
            
        if ( ! is_dir($cache_path) OR ! is_really_writable($cache_path))
        {
            return FALSE;
        }
        
        // Build the file path.
        $filepath = $cache_path.$filename.'.cache';
        
        if ( ! @file_exists($filepath))
        {
            return FALSE;
        }
    
        if ( ! $fp = @fopen($filepath, FOPEN_READ))
        {
            return FALSE;
        }
            
        flock($fp, LOCK_SH);
        
        $cache = '';
        if (filesize($filepath) > 0)
        {
            $cache = unserialize(fread($fp, filesize($filepath)));
        }
        else
        {
            $cache = NULL;
        }
        
        flock($fp, LOCK_UN);
        fclose($fp);
        
        if (isset($cache['mp_cache_expires_time']) && $cache['mp_cache_expires_time'] < time() && $use_expires)
        {
            $this->delete($filename);
            return false;
        }
        
        // Return the cache
        log_message('debug', "MP_Cache retrieved: ".$filename);
        return $cache['html'];
    }
    
    function write(array $values = array(), $filename, $expires = 0)
    {
        $cache_path = $this->path;
        
        if ( ! is_dir($cache_path) OR ! is_really_writable($cache_path))
        {
            return;
        }
        
        // check if filename contains dirs
        $subdirs = explode('/', $filename);
        if (count($subdirs) > 1)
        {
            array_pop($subdirs);
            $test_path = $cache_path.implode('/', $subdirs);
            
            // check if specified subdir exists
            if ( ! @file_exists($test_path))
            {
                // create non existing dirs, asumes PHP5
                if ( ! @mkdir($test_path)) return false;
            }
        }
        
        $cache_path .= $filename.'.cache';

        if ( ! $fp = @fopen($cache_path, FOPEN_WRITE_CREATE_DESTRUCTIVE))
        {
            log_message('error', "Unable to write MP_cache file: ".$cache_path);
            return;
        }
        
        // Add expires variable if its set
        if ($expires > 0)
            $values['mp_cache_expires_time'] = time() + $expires;
        
        if (flock($fp, LOCK_EX))
        {
            fwrite($fp, serialize($values));
            flock($fp, LOCK_UN);
        }
        else
        {
            log_message('error', "MP_Cache was unable to secure a file lock for file at: ".$cache_path);
            return;
        }
        fclose($fp);
        @chmod($cache_path, DIR_WRITE_MODE);

        log_message('debug', "MP_Cache file written: ".$cache_path);
    }
    
    function delete($filename)
    {
        $file_path = $this->path.$filename.'.cache';
        
        if (file_exists($file_path)) unlink($file_path);
    }
    
    function delete_all($dirname)
    {
        if (empty($this->path)) return false;

        $this->ci->load->helper('file');
        if (file_exists($this->path.$dirname)) delete_files($this->path.$dirname, TRUE);
    }
}