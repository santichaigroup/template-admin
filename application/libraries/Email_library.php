<?php
class Email_library {
	private $ci;
	private $setting = array();
	private $config_mail = array();
	var $_data = array();
	public function __construct()
	{
		$this->ci = & get_instance();
		$this->ci->load->library('parser');

		$this->setting['body_entry']="";
		$this->setting['get']=array();
		$this->setting['title'] = "Administrator";
		$this->setting['asset_url'] = base_url("public/panel") . "/";
		$this->setting['site_url'] = site_url();
		$this->setting['base_url'] = base_url();
		$this->setting['admin_url'] = admin_url();	
		$this->setting['current_url'] = site_url($this->ci->uri->uri_string());
		$this->setting['navi_title'] = NULL;
		$this->setting['navi_icon'] = NULL;
		$this->setting['toolbar'] = array();
		$this->setting['company_name']=NULL;
		$this->setting['member_username']=NULL;

		$this->ci->load->library('email');
		$this->ci->load->model('lang_model');
	}

	public function confirm_register($result,$password=NULL)
	{
		$lang = "TH";
		($lang=="TH")?$this->_data['text_lang'] = $this->ci->lang_model->langTH():$this->_data['text_lang'] = $this->ci->lang_model->langEN();

		// Mail Detail
		$result_email 	= $this->load_setemail_content('3');
		$email 			= $result_email['content_email'];
		$email_name 	= $result_email['content_detail'];
		$email_cc 		= $result_email['content_cc'];

		// Member Data
		$fullname 		= $result['firstname']." ".$result['lastname'];
		$username 		= $result['username'];
		$link_confirm 	= "member/verify_email/EN?email=".$result['email']."&code=".md5($result['email']."-active-".$result['member_id'])."";
		$member_email 	= $result['email'];
		// var_dump($result['email']."---".$result['member_id']."---".$link_confirm);exit();

		// Messages Mail
		if($result['status']=="guest") {

			$this->_data['title'] = $this->_data['text_lang']['email_title'];
			$this->_data['image_logo'] = $this->_data['text_lang']['email_image_logo'];
			$this->_data['hello_user'] = $this->_data['text_lang']['email_hello_user'].$fullname;
			$this->_data['welcome'] = $this->_data['text_lang']['email_welcome'];
			$this->_data['welcome_desc'] = $this->_data['text_lang']['email_welcome_desc'];
			// $this->_data['messages'] = $this->_data['text_lang']['email_messages'];
			$this->_data['messages_username'] = $this->_data['text_lang']['email_messages_username'].$member_email;
			$this->_data['messages_password'] = $this->_data['text_lang']['email_messages_password'].$password;
			$this->_data['messages_login'] = $this->_data['text_lang']['email_messages_login_guest'];
			$this->_data['link_confirm'] = $this->_data['text_lang']['email_link_confirm'].$link_confirm;
			// $this->_data['text_confirm'] = $this->_data['text_lang']['email_text_confirm'];
			$this->_data['or_text_confirm'] = $this->_data['text_lang']['email_or_text_confirm'];
			$this->_data['thank_you'] = $this->_data['text_lang']['email_thank_you'];
			$this->_data['company'] = $this->_data['text_lang']['email_company'];
			$this->_data['address'] = $this->_data['text_lang']['email_address'];

		} else {

			$this->_data['title'] = $this->_data['text_lang']['email_title'];
			$this->_data['image_logo'] = $this->_data['text_lang']['email_image_logo'];
			$this->_data['hello_user'] = $this->_data['text_lang']['email_hello_user'].$fullname;
			$this->_data['welcome'] = $this->_data['text_lang']['email_welcome'];
			$this->_data['welcome_desc'] = $this->_data['text_lang']['email_welcome_desc'];
			// $this->_data['messages'] = $this->_data['text_lang']['email_messages'];
			$this->_data['messages_username'] = $this->_data['text_lang']['email_messages_username'].$username;
			// $this->_data['messages_password'] = $this->_data['text_lang']['email_messages_password'].$password_resutl;
			$this->_data['messages_login'] = $this->_data['text_lang']['email_messages_login'];
			$this->_data['link_confirm'] = $this->_data['text_lang']['email_link_confirm'].$link_confirm;
			// $this->_data['text_confirm'] = $this->_data['text_lang']['email_text_confirm'];
			$this->_data['or_text_confirm'] = $this->_data['text_lang']['email_or_text_confirm'];
			$this->_data['thank_you'] = $this->_data['text_lang']['email_thank_you'];
			$this->_data['company'] = $this->_data['text_lang']['email_company'];
			$this->_data['address'] = $this->_data['text_lang']['email_address'];

		}

		$mail_body = $this->view("template_email/confirm_register", $this->_data, TRUE);

		// Config Mail
        $this->ci->email->from($email, $email_name);
        $this->ci->email->to($member_email);
        #$this->ci->email->cc();
        $this->ci->email->subject($this->_data['title']);
        $this->ci->email->message($mail_body);

		if( !$this->ci->email->send() ) {

			echo $this->ci->email->print_debugger();
			exit();
			$send_mail = false;

		} else {

			$send_mail = true;

		}     

		return $send_mail;
	}

	public function forget_password($result, $newPassword)
	{
		$lang = "TH";
		($lang=="TH")?$this->_data['text_lang'] = $this->ci->lang_model->langTH():$this->_data['text_lang'] = $this->ci->lang_model->langEN();

		// Mail Detail
		$result_email 	= $this->load_setemail_content('4');
		$email = $result_email['content_email'];
		$email_name = $result_email['content_detail'];
		$email_cc = $result_email['content_cc'];

		// Member Data
		$fullname = $result['firstname']." ".$result['lastname'];
		$username = $result['username'];
		$password_resutl = $newPassword;
		$link_confirm = "member/signin";
		$member_email = $result['email'];

		// Messages Mail
		$this->_data['title_reset_password'] = $this->_data['text_lang']['email_title_reset_password'];
		$this->_data['image_logo'] = $this->_data['text_lang']['email_image_logo'];
		$this->_data['hello_user'] = $this->_data['text_lang']['email_hello_user'].$fullname;
		// $this->_data['welcome'] = $this->_data['text_lang']['email_welcome'];
		// $this->_data['messages_forget_'] = $this->_data['text_lang']['email_messages'];
		$this->_data['messages_username'] = $this->_data['text_lang']['email_messages_username'].$username;
		$this->_data['messages_password'] = $this->_data['text_lang']['email_messages_password'].$password_resutl;
		$this->_data['messages_forget_password'] = $this->_data['text_lang']['email_messages_forget_password'];
		$this->_data['messages_click_reset'] = $this->_data['text_lang']['email_messages_click_reset'];
		$this->_data['link_confirm'] = $this->_data['text_lang']['email_link_confirm'].$link_confirm;
		// $this->_data['text_reset_password'] = $this->_data['text_lang']['email_text_reset_password'];
		// $this->_data['or_text_confirm'] = $this->_data['text_lang']['email_or_text_confirm'];
		$this->_data['thank_you'] = $this->_data['text_lang']['email_thank_you'];
		$this->_data['company'] = $this->_data['text_lang']['email_company'];
		$this->_data['address'] = $this->_data['text_lang']['email_address'];
		$mail_body = $this->view("template_email/change_password", $this->_data, TRUE);

		// Config Mail
        $this->ci->email->initialize($this->config_mail);
        $this->ci->email->from($email, $email_name);
        $this->ci->email->to($member_email);
        #$this->ci->email->cc();
        $this->ci->email->subject($this->_data['title_reset_password']);
        $this->ci->email->message($mail_body);
        // $this->ci->email->send();

        if( !$this->ci->email->send() ) {

			// echo $this->ci->email->print_debugger();
			$send_mail = false;

		} else {

			$send_mail = true;

		}

		return $send_mail;
	}

	public function order_confirmation($result, $lang = "TH")
	{
		($lang=="TH")?$this->_data['text_lang'] = $this->ci->lang_model->langTH():$this->_data['text_lang'] = $this->ci->lang_model->langEN();

		$email_warehouse = 'sarut.chinprapinporn@thaiunion.com';

		// Mail Detail
		$result_email 	= $this->load_setemail_content('6');
		$email = $result_email['content_email'];
		$email_name = $result_email['content_detail'];
		$email_cc = $result_email['content_cc'];

		// Member Profile
		$load_order 	= $this->load_orders($result['refno'], $result['customeremail']);
		$order_detail	= $load_order['order_detail'];

		$this->_data['customer_name']		= $order_detail['customer_name'];
		$this->_data['customer_email']		= $order_detail['customer_email'];
		$this->_data['shipping_address']	= $order_detail['shipping_address'];
		$this->_data['billing_name']		= $order_detail['billing_name'];
		$this->_data['billing_address']		= $order_detail['billing_address'];
		//$this->_data['customer_telephone']	= $order_detail['customer_telephone'];
		$this->_data['order_date']			= $order_detail['order_date'];
		$this->_data['order_ref_id']		= $order_detail['order_ref_id'];
		$this->_data['thai_epay_refno']		= $order_detail['thai_epay_refno'];
		$this->_data['order_subtotal']		= $order_detail['order_subtotal'] - $order_detail['order_discount'];
		$this->_data['order_tax_cost']		= $order_detail['order_tax_cost'];
		$this->_data['shipping_cost']		= $order_detail['shipping_cost'];
		$this->_data['order_grand_total']	= $order_detail['order_grand_total'];
		$this->_data['order_grand_total_before_tax'] = round($this->_data['order_grand_total'] - $this->_data['order_tax_cost']);

		$this->_data['order_discount'] 		= $order_detail['order_discount'];
		$this->_data['order_status']		= $order_detail['order_status'];

		// Product detail
		$this->_data['result_order_item']	= $load_order['order_item'];
		foreach ($this->_data['result_order_item'] as $key => $value) {
			$_product_sku = $this->ci->product_library->get_product_sku(null, $value['item_id'], $lang);
			$_product_detail = $this->ci->product_library->get_product_detail($_product_sku[0]['product_id'], null, $lang);
			$img            = ( empty($_product_sku[0]['attachment'][0]) ? ( empty($_product_detail['content_thumbnail']) ? $_product_detail['attachment'][0]['attachment_name'] : $_product_detail['content_thumbnail'] ) : $_product_sku[0]['attachment'][0]['attachment_name'] );
			$this->_data['result_order_item'][$key]['product_image'] = site_url('public/uploads/product/images/' . $img);
		}

		// Messages Mail
		$this->_data['email_title_order_confirmation'] = $this->_data['text_lang']['email_title_order_confirmation'];
		//$this->_data['image_logo'] = $this->_data['text_lang']['email_image_logo'];

		$this->_data['address'] = $this->_data['text_lang']['email_address'];
		$mail_body = $this->view("template_email/order_confirmation", $this->_data, TRUE);

		// Config Mail
        $this->ci->email->initialize($this->config_mail);
        $this->ci->email->from($email, $email_name);
        $this->ci->email->to($this->_data['customer_email']);
        // $this->ci->email->cc($email_cc);
        $this->ci->email->bcc($email_warehouse);
        $this->ci->email->subject($this->_data['email_title_order_confirmation']);
        $this->ci->email->message($mail_body);
        // $this->ci->email->send();

        if( !$this->ci->email->send() ) {

			// echo $this->ci->email->print_debugger();
			$send_mail = false;

		} else {

			$send_mail = true;

		}

		return $send_mail;
	}

	public function confirm_order_creditcard($result)
	{
		$lang = "TH";
		($lang=="TH")?$this->_data['text_lang'] = $this->ci->lang_model->langTH():$this->_data['text_lang'] = $this->ci->lang_model->langEN();

		// Mail Detail
		$result_email 	= $this->load_setemail_content('7');
		$email = $result_email['content_email'];
		$email_name = $result_email['content_detail'];
		$email_cc = $result_email['content_cc'];

		// Member Profile
		$load_order 	= $this->load_orders($result['refno'], $result['customeremail']);
		$order_detail	= $load_order['order_detail'];

		$this->_data['customer_name']		= $order_detail['customer_name'];
		$this->_data['customer_email']		= $order_detail['customer_email'];
		$this->_data['shipping_address']	= $order_detail['shipping_address'];
		$this->_data['customer_telephone']	= $order_detail['customer_telephone'];
		$this->_data['order_date']			= $order_detail['order_date'];
		$this->_data['order_ref_id']		= $order_detail['order_ref_id'];
		$this->_data['order_subtotal']		= $order_detail['order_subtotal']-$order_detail['order_discount'];
		$this->_data['shipping_cost']		= $order_detail['shipping_cost'];
		$this->_data['order_grand_total']	= $order_detail['order_grand_total'];

		switch ($order_detail['payment_method']) {
			case 'V':
				$payment_method = "VISA";
				break;
			case 'M':
				$payment_method = "MASTER CARD";
				break;
			case 'A':
				$payment_method = "AMEX";
				break;
			case 'J':
				$payment_method = "JCB";
				break;
			case 'C':
				$payment_method = "UnionPay";
				break;
			case 'B':
				$payment_method = "Counter Service";
				break;
			default:
				$payment_method = "Counter Service";
				break;
		}

		// ช่องทางชำระ
		$this->_data['payment_method']		= $payment_method;
		// Product detail
		$this->_data['result_order_item']	= $load_order['order_item'];


		// Messages Mail
		$this->_data['email_title_confirm_order_creditcard'] = $this->_data['text_lang']['email_title_confirm_order_creditcard'];
		$this->_data['image_logo'] = $this->_data['text_lang']['email_image_logo'];
		$this->_data['address'] = $this->_data['text_lang']['email_address'];



		$mail_body = $this->view("template_email/confirm_order_counterservice", $this->_data, TRUE);

		// Config Mail
        $this->ci->email->initialize($this->config_mail);
        $this->ci->email->from($email, $email_name);
        $this->ci->email->to($this->_data['customer_email']);
        // $this->ci->email->cc($email_cc);
        $this->ci->email->subject($this->_data['email_title_confirm_order_creditcard']);
        $this->ci->email->message($mail_body);
        // $this->ci->email->send();

        if( !$this->ci->email->send() ) {

			// echo $this->ci->email->print_debugger();
			$send_mail = false;

		} else {

			$send_mail = true;

		}

		return $send_mail;
	}

	public function confirm_order_moneytransfer()
	{
	}

	public function shipping_confirm_order($result)
	{
		$lang = "TH";
		($lang=="TH")?$this->_data['text_lang'] = $this->ci->lang_model->langTH():$this->_data['text_lang'] = $this->ci->lang_model->langEN();

		// Mail Detail
		$result_email 	= $this->load_setemail_content('6');
		$email = $result_email['content_email'];
		$email_name = $result_email['content_detail'];
		$email_cc = $result_email['content_cc'];

		// Member Profile
		$load_order 	= $this->load_orders($result['refno'], $result['customeremail']);
		$order_detail	= $load_order['order_detail'];

		$this->_data['customer_name']		= $order_detail['customer_name'];
		$this->_data['customer_email']		= $order_detail['customer_email'];
		$this->_data['shipping_address']	= $order_detail['shipping_address'];
		$this->_data['customer_telephone']	= $order_detail['customer_telephone'];
		$this->_data['order_date']			= $order_detail['order_date'];
		$this->_data['order_ref_id']		= $order_detail['order_ref_id'];
		$this->_data['order_subtotal']		= $order_detail['order_subtotal']-$order_detail['order_discount'];
		$this->_data['shipping_cost']		= $order_detail['shipping_cost'];
		$this->_data['order_grand_total']	= $order_detail['order_grand_total'];
		$this->_data['shipping_method']		= $order_detail['shipping_method'];
		$this->_data['shipping_tracking']	= $order_detail['shipping_tracking'];
		$this->_data['shipping_method_select']		= $order_detail['shipping_method_select'];
		$this->_data['shipping_tracking_select']	= $order_detail['shipping_tracking_select'];
		$this->_data['shipping_tracking_date']		= $order_detail['shipping_tracking_date'];

		// switch ($order_detail['payment_method']) {
		// 	case 'V':
		// 		$payment_method = "VISA";
		// 		break;
		// 	case 'M':
		// 		$payment_method = "MASTER CARD";
		// 		break;
		// 	case 'A':
		// 		$payment_method = "AMEX";
		// 		break;
		// 	case 'J':
		// 		$payment_method = "JCB";
		// 		break;
		// 	case 'C':
		// 		$payment_method = "UnionPay";
		// 		break;
		// 	case 'B':
		// 		$payment_method = "Counter Service";
		// 		break;
		// 	default:
		// 		$payment_method = "Counter Service";
		// 		break;
		// }

		// ช่องทางชำระ
		// $this->_data['payment_method']		= $payment_method;
		// Product detail
		$this->_data['result_order_item']	= $load_order['order_item'];


		// Messages Mail
		$this->_data['email_title_shipping_confirm_order'] = $this->_data['text_lang']['email_title_shipping_confirm_order'];
		$this->_data['image_logo'] = $this->_data['text_lang']['email_image_logo'];

		$this->_data['address'] = $this->_data['text_lang']['email_address'];
		$mail_body = $this->view("template_email/shipping_confirm_order", $this->_data, TRUE);

		// Config Mail
        $this->ci->email->initialize($this->config_mail);
        $this->ci->email->from($email, $email_name);
        $this->ci->email->to($this->_data['customer_email']);
        // $this->ci->email->cc($email_cc);
        $this->ci->email->subject($this->_data['email_title_shipping_confirm_order']);
        $this->ci->email->message($mail_body);
        // $this->ci->email->send();

        if( !$this->ci->email->send() ) {

			// echo $this->ci->email->print_debugger();
			$send_mail = false;

		} else {

			$send_mail = true;

		}

		return $send_mail;
	}

	public function payment_counterservice($result)
	{
		$lang = "TH";
		($lang=="TH")?$this->_data['text_lang'] = $this->ci->lang_model->langTH():$this->_data['text_lang'] = $this->ci->lang_model->langEN();

		// Mail Detail
		$result_email 	= $this->load_setemail_content('8');
		$email 			= $result_email['content_email'];
		$email_name 	= $result_email['content_detail'];
		$email_cc 		= $result_email['content_cc'];

		// Member Profile
		$load_order 	= $this->load_orders($result['refno'], $result['customeremail']);
		$order_detail	= $load_order['order_detail'];

		$this->_data['customer_name']		= $order_detail['customer_name'];
		$this->_data['customer_email']		= $order_detail['customer_email'];
		$this->_data['shipping_address']	= $order_detail['shipping_address'];
		$this->_data['customer_telephone']	= $order_detail['customer_telephone'];
		$this->_data['order_date']			= $order_detail['order_date'];
		$this->_data['order_ref_id']		= $order_detail['order_ref_id'];
		$this->_data['order_subtotal']		= $order_detail['order_subtotal']-$order_detail['order_discount'];
		$this->_data['shipping_cost']		= $order_detail['shipping_cost'];
		$this->_data['order_grand_total']	= $order_detail['order_grand_total'];

		switch ($order_detail['payment_method']) {
			case 'V':
				$payment_method = "VISA";
				break;
			case 'M':
				$payment_method = "MASTER CARD";
				break;
			case 'A':
				$payment_method = "AMEX";
				break;
			case 'J':
				$payment_method = "JCB";
				break;
			case 'C':
				$payment_method = "UnionPay";
				break;
			case 'B':
				$payment_method = "Counter Service";
				break;
			default:
				$payment_method = "Counter Service";
				break;
		}

		// ช่องทางชำระ
		$this->_data['payment_method']		= $payment_method;
		// Product detail
		$this->_data['result_order_item']	= $load_order['order_item'];

		// Messages Mail
		$this->_data['email_title_payment_counterservice'] = $this->_data['text_lang']['email_title_payment_counterservice'];
		$this->_data['image_logo'] = $this->_data['text_lang']['email_image_logo'];
		$this->_data['address'] = $this->_data['text_lang']['email_address'];



		$mail_body = $this->view("template_email/payment_counterservice", $this->_data, TRUE);

		// Config Mail
        $this->ci->email->initialize($this->config_mail);
        $this->ci->email->from($email, $email_name);
        $this->ci->email->to($this->_data['customer_email']);
        // $this->ci->email->cc($email_cc);
        $this->ci->email->subject($this->_data['email_title_payment_counterservice']);
        $this->ci->email->message($mail_body);
        // $this->ci->email->send();

        if( !$this->ci->email->send() ) {

			// echo $this->ci->email->print_debugger();
			$send_mail = false;

		} else {

			$send_mail = true;

		}

		return $send_mail;
	}

	public function payment_creditcard($result)
	{
		$lang = "TH";
		($lang=="TH")?$this->_data['text_lang'] = $this->ci->lang_model->langTH():$this->_data['text_lang'] = $this->ci->lang_model->langEN();

		// Mail Detail
		$result_email 	= $this->load_setemail_content('9');
		$email 			= $result_email['content_email'];
		$email_name 	= $result_email['content_detail'];
		$email_cc 		= $result_email['content_cc'];

		// Member Profile
		$load_order 	= $this->load_orders($result['refno'], $result['customeremail']);
		$order_detail	= $load_order['order_detail'];

		$this->_data['customer_name']		= $order_detail['customer_name'];
		$this->_data['customer_email']		= $order_detail['customer_email'];
		$this->_data['shipping_address']	= $order_detail['shipping_address'];
		$this->_data['customer_telephone']	= $order_detail['customer_telephone'];
		$this->_data['order_date']			= $order_detail['order_date'];
		$this->_data['order_ref_id']		= $order_detail['order_ref_id'];
		$this->_data['order_subtotal']		= $order_detail['order_subtotal']-$order_detail['order_discount'];
		$this->_data['shipping_cost']		= $order_detail['shipping_cost'];
		$this->_data['order_grand_total']	= $order_detail['order_grand_total'];

		switch ($order_detail['payment_method']) {
			case 'V':
				$payment_method = "VISA";
				break;
			case 'M':
				$payment_method = "MASTER CARD";
				break;
			case 'A':
				$payment_method = "AMEX";
				break;
			case 'J':
				$payment_method = "JCB";
				break;
			case 'C':
				$payment_method = "UnionPay";
				break;
			case 'B':
				$payment_method = "Counter Service";
				break;
			default:
				$payment_method = "Counter Service";
				break;
		}

		// ช่องทางชำระ
		$this->_data['payment_method']		= $payment_method;
		// Product detail
		$this->_data['result_order_item']	= $load_order['order_item'];

		// Messages Mail
		$this->_data['email_title_payment_creditcard'] = $this->_data['text_lang']['email_title_payment_creditcard'];
		$this->_data['image_logo'] = $this->_data['text_lang']['email_image_logo'];
		$this->_data['address'] = $this->_data['text_lang']['email_address'];



		$mail_body = $this->view("template_email/payment_creditcard", $this->_data, TRUE);

		// Config Mail
        $this->ci->email->initialize($this->config_mail);
        $this->ci->email->from($email, $email_name);
        $this->ci->email->to($this->_data['customer_email']);
        $this->ci->email->cc($email_cc);
        $this->ci->email->subject($this->_data['email_title_payment_creditcard']);
        $this->ci->email->message($mail_body);
        // $this->ci->email->send();

        if( !$this->ci->email->send() ) {

			// echo $this->ci->email->print_debugger();
			$send_mail = false;

		} else {

			$send_mail = true;

		}

		return $send_mail;
	}

	public function payment_moneytransfer()
	{
		
	}

	public function order_remind_counterservice()
	{

	}

	public function order_remind_creditCard()
	{

	}

	public function order_remind_moneytransfer()
	{

	}

	public function email_voucher($result, $voucher_detail = array())
	{
		$lang = "TH";
		($lang=="TH")?$this->_data['text_lang'] = $this->ci->lang_model->langTH():$this->_data['text_lang'] = $this->ci->lang_model->langEN();

		// Mail Detail
		$result_email 	= $this->load_setemail_content('10');
		$email 			= $result_email['content_email'];
		$email_name 	= $result_email['content_detail'];
		$email_cc 		= $result_email['content_cc'];

		// Member Data
		$fullname 		= $result['firstname']." ".$result['lastname'];
		
		/* VOUCHER */
		$promo_code	= $voucher_detail['coupon_code'];

		$date = date_create($voucher_detail['promotion_end']);
		$_voucher_expire = date_format($date,"d M Y");

		$expire_date = strtoupper($_voucher_expire);

		$promo_price = $voucher_detail['promotion_action_value'];
		/* VOUCHER */
		
		$member_email 	= $result['email'];

		// Messages Mail
		if($result['status']=="guest") {

			$this->_data['title'] = $this->_data['text_lang']['email_title_voucher'];
			$this->_data['image_logo'] = $this->_data['text_lang']['email_image_logo'];
			$this->_data['hello_user'] = $this->_data['text_lang']['email_hello_user'].$fullname;

			$this->_data['promo_code']  = $promo_code;
	        $this->_data['promo_price'] = $promo_price;
	        $this->_data['expire_date'] = $expire_date;

	        $this->_data['promo_code_instruction'] = $this->_data['text_lang']['email_promo_code_instruction'];
          	$this->_data['thanks_for_confirm']     = $this->_data['text_lang']['email_thanks_for_confirm'];
          	$this->_data['back_to']                = $this->_data['text_lang']['email_back_to'];

			$this->_data['thank_you'] = $this->_data['text_lang']['email_thank_you'];
			$this->_data['company'] = $this->_data['text_lang']['email_company'];
			$this->_data['address'] = $this->_data['text_lang']['email_address'];

		} else {

			$this->_data['title'] = $this->_data['text_lang']['email_title_voucher'];
			$this->_data['image_logo'] = $this->_data['text_lang']['email_image_logo'];
			$this->_data['hello_user'] = $this->_data['text_lang']['email_hello_user'].$fullname;
			$this->_data['welcome'] = $this->_data['text_lang']['email_welcome'];
			
			$this->_data['promo_code']  = $promo_code;
	        $this->_data['promo_price'] = $promo_price;
	        $this->_data['expire_date'] = $expire_date;

	        $this->_data['promo_code_instruction'] = $this->_data['text_lang']['email_promo_code_instruction'];
          	$this->_data['thanks_for_confirm']     = $this->_data['text_lang']['email_thanks_for_confirm'];
          	$this->_data['back_to']                = $this->_data['text_lang']['email_back_to'];

			$this->_data['thank_you'] = $this->_data['text_lang']['email_thank_you'];
			$this->_data['company'] = $this->_data['text_lang']['email_company'];
			$this->_data['address'] = $this->_data['text_lang']['email_address'];

		}

		$mail_body = $this->view("template_email/email_voucher", $this->_data, TRUE);

		// Config Mail
        $this->ci->email->initialize($this->config_mail);
        $this->ci->email->from($email, $email_name);
        $this->ci->email->to($member_email);
        #$this->ci->email->cc();
        $this->ci->email->subject($this->_data['title']);
        $this->ci->email->message($mail_body);
        // $this->ci->email->send();

        if( !$this->ci->email->send() ) {

			// echo $this->ci->email->print_debugger();
			$send_mail = false;

		} else {

			$send_mail = true;

		}

		return $send_mail;
	}

	public function email_contact($result, $email_data)
	{
		$lang = "TH";
		($lang=="TH")?$this->_data['text_lang'] = $this->ci->lang_model->langTH():$this->_data['text_lang'] = $this->ci->lang_model->langEN();

		// Mail Detail
		$result_email 	= $this->load_setemail_content('5');
		$member_email = $email_data['content_email'];
		$email_name = $result['fullname'];
		$email_cc = $email_data['content_email_cc'];

		// Member Data
		$date_time		= "เมื่อ ".date("Y-m-d H:i:s");
		$topic			= $email_data['content_keyword'];
		$fullname 		= $result['fullname'];
		// $telephone 		= $result['telephone'];
		$email 			= $result['email'];
		$detail 		= $result['detail'];


		// Messages Mail
		$this->_data['title_contact'] = $this->_data['text_lang']['email_title_contact'];
		$this->_data['image_logo'] = $this->_data['text_lang']['email_image_logo'];
		$this->_data['hello_admin'] = $this->_data['text_lang']['email_hello_admin'];
		$this->_data['welcome'] = $this->_data['text_lang']['email_welcome'];

		$this->_data['date_time'] 	= $date_time;
		$this->_data['topic'] 		= $topic;
		$this->_data['fullname'] 	= $fullname;
		$this->_data['telephone'] 	= "";
		$this->_data['email'] 		= $email;
		$this->_data['detail'] 		= $detail;

		$this->_data['thank_you'] = $this->_data['text_lang']['email_thank_you'];
		$this->_data['company'] = $this->_data['text_lang']['email_company'];
		$this->_data['address'] = $this->_data['text_lang']['email_address'];
		$mail_body = $this->view("template_email/email_contact", $this->_data, TRUE);

		// Config Mail
        $this->ci->email->initialize($this->config_mail);
        $this->ci->email->from($email, $email_name);
        $this->ci->email->to($member_email);
        $this->ci->email->cc($email_cc);
        $this->ci->email->subject($this->_data['title_contact']);
        $this->ci->email->message($mail_body);
        // $this->ci->email->send();

        if( !$this->ci->email->send() ) {

			// echo $this->ci->email->print_debugger();
			$send_mail = false;

		} else {

			$send_mail = true;

		}

		return $send_mail;
	}

	public function notify_inventory_unit($result)
	{
		$lang = "TH";
		($lang=="TH")?$this->_data['text_lang'] = $this->ci->lang_model->langTH():$this->_data['text_lang'] = $this->ci->lang_model->langEN();

		// Mail Detail
		$member_email 	= $result['email'];
		$email_name 	= "ระบบตอบรับอัตโนมัติ";
		// $email_cc 		= $email_data['content_email_cc'];

		// Member Data
		$date_time		= "เมื่อ ".date("Y-m-d H:i:s");
		$email 			= "";


		// Messages Mail
		$this->_data['subject']		= $result['subject'];
		$this->_data['sku_code'] 	= $result['sku_code'];
		$this->_data['sku_name'] 	= $result['sku_name'];
		$this->_data['date'] 		= date("Y-m-d H:i:s");
		$this->_data['stock'] 		= $result['stock'];
		$this->_data['notify_set'] 	= $result['notify_set'];
		$this->_data['notify_type'] = $result['notify_type'];
		$this->_data['date_time'] 	= $date_time;

		$mail_body = $this->view("template_email/notify_product", $this->_data, TRUE);

		// Config Mail
        $this->ci->email->initialize($this->config_mail);
        $this->ci->email->from($email, $email_name);
        $this->ci->email->to($member_email);
        // $this->ci->email->cc($email_cc);
        $this->ci->email->subject($result['subject']);
        $this->ci->email->message($mail_body);
        $this->ci->email->send();

  //       if( !$this->ci->email->send() ) {

		// 	// echo $this->ci->email->print_debugger();
		// 	$send_mail = false;

		// } else {

		// 	$send_mail = true;

		// }

		// return $send_mail;
	}

	public function load_setemail_content($content_id)
	{
		$this->ci->db->select("*");
		$this->ci->db->where("content_id", $content_id);	

		return $this->ci->db->get("setemail_content")->row_array();
	}

	function randPassword($length)
	{
		$chars = "1234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i = 0;
		$password = "";
		while ($i <= $length) {
			$password .= $chars{mt_rand(0,(strlen($chars)-1))};
			$i++;
		}
		return $password;
	}

	public function load_orders($order_ref_id, $customer_email)
	{
		$detail = array();

		$this->ci->db->select("*");
		$this->ci->db->where("order_ref_id", $order_ref_id);
		$this->ci->db->where("customer_email", $customer_email);

		$detail['order_detail'] = $this->ci->db->get('orders')->row_array();

		$this->ci->db->select("*");
		$this->ci->db->where("order_id", $detail['order_detail']['id']);

		$detail['order_item']	= $this->ci->db->get('order_items')->result_array();

		return $detail;
	}

	// !!! Don't Delete Important !!!!
	public function view($file,$data=array(),$output=false)
	{
		if($data) {
			$data = array_merge($this->setting,$data);
		} else {
			$data = $this->setting;
		}
		foreach($_GET as $key=>$val) {
			$this->setting['get'][$key]=$val;
		}
		$res= $this->ci->load->view("views/".$file,$data,true);
		if($output==false) {
			$this->setting['body_entry'] = $res;
		}
		return $res;
	}

}
?>