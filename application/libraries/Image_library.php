<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Image_library {

	private $ci;
	private $_data = array();

	public function __construct()
	{
		$this->ci = & get_instance();
		$this->ci->load->library('parser');
		// $this->ci->load->library('admin_library');
	}

	public function checkUploadImage($menu_link,$image_cate=null)
	{
		$this->ci->db->where('menu_link', $menu_link);

		if($image_cate) {

			$this->ci->db->where('image_cate', $image_cate);
		}

		$this->ci->db->where('image_status', 'active');

		return $this->ci->db->get("system_image");
	}
	public function checkAttachments($menu_link,$image_cate,$main_id,$lang_id=NULL)
	{
		$type_file = array('gif','jpg','png','jpeg');

		$this->ci->db->select('*');
		$this->ci->db->where($menu_link."_attachment.default_main_id",$main_id);
		// $this->ci->db->where_in($menu_link."_attachment.attachment_type", $type_file);
		$this->ci->db->where($menu_link."_attachment.image_cate", $image_cate);

		if($lang_id) {
			$this->ci->db->where($menu_link."_attachment.lang_id",$lang_id);
		}

		return  $this->ci->db->count_all_results($menu_link.'_attachment');
	}
	public function getAttachmentsList($menu_link,$image_cate,$main_id,$lang_id=NULL)
	{
		$query_start = $this->ci->db->select('*');

		$query_result = $query_start->join($menu_link."_content",$menu_link."_content.main_id = ".$menu_link."_attachment.default_main_id")
					->where($menu_link."_content.content_thumbnail IS NULL", null, false)
					->where($menu_link."_attachment.default_main_id",$main_id)
					->where($menu_link."_attachment.image_cate",$image_cate);

		if($lang_id) {
			$query_result->where($menu_link."_attachment.lang_id",$lang_id);
		}
			$query_result->order_by($menu_link.'_attachment.sequence', 'asc');

		$query_check = $query_result->get($menu_link.'_attachment');
		
		if(!$query_check->num_rows()) {

			$query_start->join($menu_link."_content",$menu_link."_content.main_id = ".$menu_link."_attachment.default_main_id")
				->where($menu_link."_content.content_thumbnail IS NULL", null, false)
				->where($menu_link."_attachment.default_main_id",$main_id)
				->where($menu_link."_attachment.image_cate",$image_cate);

			if($lang_id) {
				$query_start->where($menu_link."_attachment.lang_id",$lang_id);
			}

			$query_start->order_by($menu_link.'_attachment.sequence', 'asc');

			return $query_start->get($menu_link.'_attachment');
			// return $query_check;
		} else {

			return $query_check;
		}
	}
	public function getAttachmentsMainId($menu_link,$main_id,$lang_id=NULL)
	{
		$type_file = array('gif','jpg','png','jpeg');

		$this->ci->db->select('*');
		$this->ci->db->join($menu_link."_content",$menu_link."_content.main_id = ".$menu_link."_id.main_id AND ".$menu_link."_content.content_id = ".$menu_link."_id.default_main_id");
		$this->ci->db->join($menu_link."_attachment",$menu_link."_attachment.default_main_id = ".$menu_link."_id.main_id ","right");

		$this->ci->db->where($menu_link."_id.main_id",$main_id);
		$this->ci->db->where_in($menu_link."_attachment.attachment_type", $type_file);
		$this->ci->db->where($menu_link."_id.main_status <>",'deleted');

		if($lang_id) {
			$this->ci->db->where($menu_link."_attachment.lang_id",$lang_id);
		}

		$this->ci->db->order_by($menu_link."_attachment.sequence","ASC");
		$this->ci->db->limit(1000);

		return  $this->ci->db->get($menu_link.'_id');
	}
	public function insertContent($menu_link,$default_main_id,$attachment_name,$image_cate,$lang_id=NULL)
	{
		$checkAttachments = $this->checkAttachments($menu_link,$image_cate,$default_main_id,$lang_id);

		if($attachment_name) {
			$attachment_type = str_replace('.','',strstr($attachment_name,'.'));

			$this->ci->db->set("attachment_name",$attachment_name);
			$this->ci->db->set("attachment_name_main",$attachment_name);
			$this->ci->db->set("attachment_type",$attachment_type);
		}

		if($default_main_id) {
			$this->ci->db->set("default_main_id",$default_main_id);
		}

		$this->ci->db->set("image_cate",$image_cate);
		$this->ci->db->set("lang_id",$lang_id);
		$this->ci->db->set("update_by",$this->ci->admin_library->userdata('user_id'));
		$this->ci->db->set("update_date","NOW()",false);
		$this->ci->db->set("update_ip",$this->ci->input->ip_address());
		$this->ci->db->set("sequence", ($checkAttachments+1));
		$this->ci->db->set("attachment_status", 'active');

		$this->ci->db->insert($menu_link."_attachment");
		return $this->ci->db->insert_id();
	}
	public function updateContent(array $data = array())
	{
		if($data['main_id'] != $data['default_main_id']) {
			$this->ci->db->set('default_main_id', $data['main_id']);

			$this->ci->db->where('default_main_id', $data['default_main_id']);
			$this->ci->db->update($data['menu_link'].'_attachment');

			$data['default_main_id'] = $data['main_id'];
		}

		$img_id 				= $data['attachment_id'];
		$attachment_name 		= $data['attachment_name'];
		$attachment_name_old 	= $data['attachment_name_old'];
		$attachment_name_main 	= $data['attachment_name_main'];
		$attachment_detail 		= $data['attachment_detail'];

		if(is_array($img_id) && count($img_id)>0) {

			$i=0;
			foreach($img_id as $id) {

				if(!empty($attachment_name[$id]) || !empty($attachment_name_old[$id])) {

					$data_change_path_file = [
						'attachment_id'			=> $id,
						'attachment_name' 		=> (!empty($attachment_name[$id]) ?
													($attachment_name_old[$id] ?
														$attachment_name[$id] :
													 		$attachment_name[$id]."_".$attachment_name_main[$id]) :
																$attachment_name_main[$id]
												),
						'attachment_name_main'	=> $attachment_name_main[$id],
						'attachment_detail'		=> $attachment_detail[$id]
					];

					if( $this->changePathFile($data['menu_link'],$data_change_path_file,$attachment_name_old[$id],'images') ) {

						$this->updateAttachmentContent($data['menu_link'],$data_change_path_file);
					}
				} else {

					$data_change_path_file = [
						'attachment_id'			=> $id,
						'attachment_detail'		=> $attachment_detail[$id]
					];

					$this->updateAttachmentContent($data['menu_link'],$data_change_path_file);
				}

				$i++;
				$this->setSequenceAttachment($data['menu_link'],$id,$i);
			}
		}
	}
	public function updateAttachmentContent($menu_link,array $data = array())
	{
		$this->ci->db->set('update_by',$this->ci->admin_library->userdata('user_id'));
		$this->ci->db->set('update_date','NOW()',false);
		$this->ci->db->set('update_ip',$this->ci->input->ip_address());

		$this->ci->db->where('attachment_id',$data['attachment_id']);
		$this->ci->db->update($menu_link.'_attachment', $data);
	}
	public function setSequenceAttachment($menu_link,$id,$i)
	{
		$this->ci->db->set("sequence",$i);
		$this->ci->db->set("update_by",$this->ci->admin_library->userdata('user_id'));
		$this->ci->db->set("update_date","NOW()",false);
		$this->ci->db->set("update_ip",$this->ci->input->ip_address());

		$this->ci->db->where("attachment_id",$id);
		return $this->ci->db->update($menu_link."_attachment");
	}
	public function getThumbnailImage($menu_link,$content_thumbnail)
	{
		$this->ci->db->where($menu_link.'_attachment.attachment_id', $content_thumbnail);
		$this->ci->db->limit(1);

		return $this->ci->db->get($menu_link.'_attachment');
	}
	public function setDefaultImage($menu_link,$default_main_id,$attachment_id,$lang_id)
	{
		$this->ci->db->set("content_thumbnail",$attachment_id);

		$this->ci->db->where("main_id",$default_main_id);
		$this->ci->db->where("lang_id",$lang_id);
		return $this->ci->db->update($menu_link."_content");
	}
	public function deleteImage($menu_link,$attachment_id)
	{
		$this->ci->db->where("attachment_id",$attachment_id);
		return $this->ci->db->delete($menu_link."_attachment");
	}
	public function refreshHash($menu_link,$default_main_id)
	{
		$this->ci->db->where("default_main_id",$default_main_id);
		$this->ci->db->delete($menu_link."_attachment");
	}
	private function changePathFile($menu_link,array $data = array(),$attachment_name_old,$type="images")
	{
		$attachment_new 	= ($data['attachment_name'] ? 
										$data['attachment_name'] : 
											$attachment_name_old);
		$attachment_old 	= ($attachment_name_old ? 
										$attachment_name_old : 
											$data['attachment_name_main']);

		$upload_path_new 	= './public/uploads/'.$menu_link.'/'.$type.'/'.$attachment_new;
		$upload_path_main 	= './public/uploads/'.$menu_link.'/'.$type.'/'.$attachment_old;

		if(file_exists($upload_path_main)) {

			return rename($upload_path_main, $upload_path_new);
		} else {

			return false;
		}
	}
}