<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Template_migration extends CI_Migration {

        var $_data = array();

        public function create_table_id($class_name)
        {
                $this->dbforge->add_field(array(
                        'main_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )
                ));

                $fields = array(
                                "default_main_id int(11) DEFAULT NULL,
                                post_by int(11) DEFAULT NULL,
                                post_date datetime DEFAULT NULL,
                                post_ip char(15) DEFAULT '000.000.000.000',
                                update_by int(11) DEFAULT NULL,
                                update_date datetime DEFAULT NULL,
                                update_ip char(15) DEFAULT '000.000.000.000',
                                main_status enum('deleted','pending','active') DEFAULT 'active',
                                sequence int(11) DEFAULT NULL"
                                );

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('main_id', TRUE);
                $this->dbforge->create_table($class_name.'_id');
        }

        public function create_table_content($class_name)
        {
                $this->dbforge->add_field(array(
                        'content_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )
                ));

                $fields = array(
                                "main_id int(11) DEFAULT NULL,
                                lang_id char(2) DEFAULT NULL,
                                content_subject varchar(255) DEFAULT NULL,
                                content_detail text,
                                content_thumbnail int(11) DEFAULT NULL,
                                content_title varchar(255) DEFAULT NULL,
                                content_keyword text,
                                content_description text,
                                content_seo varchar(255) DEFAULT NULL,
                                content_rewrite_id varchar(32) DEFAULT NULL,
                                update_by int(11) DEFAULT NULL,
                                update_date datetime DEFAULT NULL,
                                update_ip char(15) DEFAULT '000.000.000.000',
                                content_status enum('deleted','pending','active') DEFAULT 'active'"
                                );

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('content_id', TRUE);
                $this->dbforge->create_table($class_name.'_content');
        }

        public function create_table_attachment($class_name)
        {
                $this->dbforge->add_field(array(
                        'attachment_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )
                ));

                $fields = array(
                                "default_main_id varchar(100) DEFAULT NULL,
                                image_cate char(10) DEFAULT 'pc',
                                lang_id char(2) DEFAULT NULL,
                                attachment_name varchar(100) DEFAULT NULL,
                                attachment_name_main varchar(100) DEFAULT NULL,
                                attachment_detail text,
                                attachment_type varchar(50) DEFAULT NULL,
                                attachment_status enum('pending','active') DEFAULT 'pending',
                                update_by int(11) DEFAULT NULL,
                                update_date datetime DEFAULT NULL,
                                update_ip char(15) DEFAULT '000.000.000.000',
                                sequence int(11) DEFAULT NULL"
                                );

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('attachment_id', TRUE);
                $this->dbforge->create_table($class_name.'_attachment');
        }

        public function create_table_logs($class_name)
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )
                ));

                $fields = array(
                                "menu_id int(11) DEFAULT NULL,
                                submenu_id varchar(255) DEFAULT NULL,
                                action enum('add','update','delete') DEFAULT NULL,
                                username varchar(255) DEFAULT NULL,
                                session varchar(40) DEFAULT NULL,
                                messages text,
                                data text,
                                post_by int(11) DEFAULT NULL,
                                post_date int(10) DEFAULT NULL,
                                post_ip char(15) DEFAULT '000.000.000.000',
                                status enum('success', 'failure', 'others') DEFAULT NULL"
                                );

                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($class_name.'_log');
        }
}