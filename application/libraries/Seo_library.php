<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Seo_library {

	private $ci;
	private $_data = array();

	public function __construct()
	{
		$this->ci = & get_instance();
	}

	public function rewrite_update(array $data = array())
	{
		switch ($data['lang_id']) {

            case 'th':
                    $check_lang = "/[^a-zA-Z0-9\wก-๙เ]+/u";
                    break;
            case 'cn':
                    $check_lang = "/[^a-zA-Z0-9\p{Han}]+/u";
                    break;
            default:
                    $check_lang = "/[^a-zA-Z0-9]+/";
                    break;
        }

        if($data['language']=="single") {

	        $content_subject 	= $data['content_subject'];

			$data_rewrite 		= array(
								'url_rewrite_id'		=> $data['content_rewrite_id'],
								'target_path'			=> $data['target_path'],
								'target_detail_path'	=> $data['target_detail_path'],
								'target_main_path'		=> strtolower(rtrim(preg_replace($check_lang, "-", $content_subject), '-')),
								'update_by' 			=> $this->ci->admin_library->userdata('user_id'),
								'update_date' 			=> time(),
								'update_ip' 			=> $this->ci->input->ip_address()
							);
		} else {

	        $content_subject 	= str_replace("/".$data['lang_id'],"",$data['content_subject']);

			$data_rewrite 		= array(
								'url_rewrite_id'		=> $data['content_rewrite_id'],
								'target_path'			=> $data['target_path'],
								'target_detail_path'	=> $data['target_detail_path'],
								'target_main_path'		=> strtolower(rtrim(preg_replace($check_lang, "-", $content_subject), '-')).( $data['lang_id'] ? "/".$data['lang_id'] : '' ),
								'update_by' 			=> $this->ci->admin_library->userdata('user_id'),
								'update_date' 			=> time(),
								'update_ip' 			=> $this->ci->input->ip_address()
							);
		}

		// Check data New to insert, Change language to update or Update content
			if(!$this->ci->db->where('url_rewrite_id', $data_rewrite['url_rewrite_id'])->insert('system_url_rewrite', $data_rewrite)) {

				$this->ci->db->where('url_rewrite_id', $data_rewrite['url_rewrite_id'])->update('system_url_rewrite', $data_rewrite);
			}

		// Update content_seo
			if($data_rewrite['url_rewrite_id']) {

				$data_seo = array(
							'content_rewrite_id' 		=> $data_rewrite['url_rewrite_id'],
							'content_seo'				=> $data_rewrite['target_main_path']
						);

				$this->ci->db->where('content_rewrite_id',$data_seo['content_rewrite_id'])->update($data['menu_link'].'_content', $data_seo);
			}

		// Update Url from system_url_rewrite to routes.php
			$this->rewrite_all_url();
	}

	public function rewrite_delete(array $data = array())
	{
		$this->ci->db->where('target_detail_path', $data['target_detail_path']);
		$this->ci->db->where('url_rewrite_id', $data['content_rewrite_id']);
		$this->ci->db->delete('system_url_rewrite');

		// Update Url from system_url_rewrite to routes.php
			$this->rewrite_all_url();		
	}


	private function rewrite_all_url()
	{
        $routes = $this->ci->db->get('system_url_rewrite');

        // write out the PHP array to the file with help from the file helper
        if (!empty($routes)) {
            // for every page in the database, get the route using the recursive
            foreach ($routes->result_array() as $route) {

                $data[] = '$route["' . $route['target_main_path'] . '"] = "' . $route['target_path'].'/'.$route['target_detail_path'] . '";';
            }

            $output = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');\n";

            $output .= implode("\n", $data);

            $this->ci->load->helper('file');
            write_file(APPPATH . "cache/routes.php", $output);
        }
	}
}